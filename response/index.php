<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Отзывы");
?>
<?
if($_REQUEST['send'] == 1)
{
    CModule::IncludeModule("iblock");

    if ($_REQUEST['name'] == "") { $_REQUEST['name'] = "не указано"; }

    $el = new CIBlockElement;
    $PROP = array();
    $PROP[8] = $_REQUEST['name'];
    $PROP[9] = $_REQUEST['contract_number'];

    $arLoadProductArray = Array(
        "IBLOCK_ID" => 5,
        "ACTIVE" => "N",
        "NAME" => $_REQUEST['contract_number']." ".$_REQUEST['name'],
        "PREVIEW_TEXT" => $_REQUEST['text_item'],
        "PROPERTY_VALUES"=> $PROP,
    );
    $el->Add($arLoadProductArray);

    $from = "oknakomforta.com <noreply@oknakomforta.com>";
    $to = "dennis@hacktor.ru, dina@oknakomforta.com, marketing@oknakomforta.com";
    $subject = "Окна комфорта - отзыв";
    $message = "Имя: ".$_REQUEST['name']."<br />";
    $message .= "№ договора: ".$_REQUEST['contract_number']."<br />";
    $message .= "Сообщение: <br />".$_REQUEST['text_item'];

    $subject = "=?utf-8?B?" . base64_encode($subject) . "?=";
    $separator = md5(time());
    $eol = PHP_EOL;

    // main header (multipart mandatory)
    $headers = "From: ".$from.$eol;
    $headers .= "MIME-Version: 1.0".$eol;
    $headers .= "Content-Type: multipart/mixed; boundary=\"".$separator."\"".$eol.$eol;
    $headers .= "Content-Transfer-Encoding: 7bit".$eol;
    $headers .= "This is a MIME encoded message.".$eol.$eol;

    // message
    $headers .= "--".$separator.$eol;
    $headers .= "Content-Type: text/html; charset=\"utf-8\"".$eol;
    $headers .= "Content-Transfer-Encoding: 8bit".$eol.$eol;
    $headers .= $message.$eol.$eol;

    // send message
    mail($to, $subject, "", $headers);
}
?>
<style>
fieldset {
	padding:0px;
	margin:0px;
    border: medium none;
    font-size: 12px;
}
fieldset .row {
    margin-bottom: 8px;
    overflow: hidden;
}
fieldset .column {
    float: left;
    width: 200px;
}
.highlight {
    color: #C01811;
}
fieldset .textinput {
    border: 1px solid #D9CDCA;
    border-radius: 5px 5px 5px 5px;
    color: #564B40;
    font-size: 12px;
    height: 22px;
    width: 190px;
}
#commentForm textarea {
    border: 1px solid #D9CDCA;
    border-radius: 5px 5px 5px 5px;
    color: #564B40;
    font-size: 12px;
    height: 70px;
    width: 390px;
}
.input_h {
    display: none;
}
.btn-send {
    background: url("/images/btn-2.png") no-repeat scroll 0 0 transparent;
    border: 0 none;
    cursor: pointer;
    display: inline-block;
    font-size: 0;
    height: 26px;
    line-height: 0;
    margin: 0;
    padding: 0;
    text-indent: -9999px;
    width: 97px;
}
.warning {
    background: none repeat scroll 0 0 #FFEEEE;
    border: 1px dashed #884444;
    border-radius: 5px 5px 5px 5px;
    color: #440000;
    font-size: 13px;
    margin-bottom: 1em;
    padding: 7px 10px;
}

.end_res{
	color:#CF3A14;
	font-size:18px;
	font-weight:600;
}

.listhead {
    border-bottom: 1px dashed #9F0511;
    color: #9F0511;
    cursor: pointer;
    display: inline;
    font-family: Arial,Helvetica,sans-serif;
    font-size: 16px !important;
    width: auto;
}
.blockabout {
    display: none;
    font-family: Arial,Helvetica,sans-serif;
    font-size: 12px;
    margin: 15px 0 0 0;
    padding: 0;
    width: 308px;
    z-index: 1;
}
</style>
<script type="text/javascript">
$(document).ready(function () {
    $('.listhead').click(function () {
        $(this).next("div").slideToggle("medium");
        $(this).toggleClass("activep");
        $(this).siblings("p").removeClass("activep");
        return false;
    });
});
</script>
<?$APPLICATION->IncludeComponent("bitrix:news", ".default", array(
	"IBLOCK_TYPE" => "content",
	"IBLOCK_ID" => "5",
	"NEWS_COUNT" => "10",
	"USE_SEARCH" => "N",
	"USE_RSS" => "N",
	"USE_RATING" => "N",
	"USE_CATEGORIES" => "N",
	"USE_FILTER" => "N",
	"SORT_BY1" => "ACTIVE_FROM",
	"SORT_ORDER1" => "DESC",
	"SORT_BY2" => "SORT",
	"SORT_ORDER2" => "ASC",
	"CHECK_DATES" => "Y",
	"SEF_MODE" => "Y",
	"SEF_FOLDER" => "/response/",
	"AJAX_MODE" => "N",
	"AJAX_OPTION_JUMP" => "N",
	"AJAX_OPTION_STYLE" => "Y",
	"AJAX_OPTION_HISTORY" => "N",
	"CACHE_TYPE" => "A",
	"CACHE_TIME" => "36000000",
	"CACHE_FILTER" => "N",
	"CACHE_GROUPS" => "Y",
	"SET_TITLE" => "Y",
	"SET_STATUS_404" => "Y",
	"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
	"ADD_SECTIONS_CHAIN" => "N",
	"USE_PERMISSIONS" => "N",
	"PREVIEW_TRUNCATE_LEN" => "",
	"LIST_ACTIVE_DATE_FORMAT" => "d/m/Y",
	"LIST_FIELD_CODE" => array(
		0 => "",
		1 => "",
	),
	"LIST_PROPERTY_CODE" => array(
		0 => "",
		1 => "",
	),
	"HIDE_LINK_WHEN_NO_DETAIL" => "N",
	"DISPLAY_NAME" => "Y",
	"META_KEYWORDS" => "-",
	"META_DESCRIPTION" => "-",
	"BROWSER_TITLE" => "-",
	"DETAIL_ACTIVE_DATE_FORMAT" => "d.m.Y",
	"DETAIL_FIELD_CODE" => array(
		0 => "",
		1 => "",
	),
	"DETAIL_PROPERTY_CODE" => array(
		0 => "",
		1 => "",
	),
	"DETAIL_DISPLAY_TOP_PAGER" => "N",
	"DETAIL_DISPLAY_BOTTOM_PAGER" => "Y",
	"DETAIL_PAGER_TITLE" => "Страница",
	"DETAIL_PAGER_TEMPLATE" => "",
	"DETAIL_PAGER_SHOW_ALL" => "Y",
	"DISPLAY_TOP_PAGER" => "N",
	"DISPLAY_BOTTOM_PAGER" => "Y",
	"PAGER_TITLE" => "Новости",
	"PAGER_SHOW_ALWAYS" => "Y",
	"PAGER_TEMPLATE" => "",
	"PAGER_DESC_NUMBERING" => "N",
	"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
	"PAGER_SHOW_ALL" => "Y",
	"DISPLAY_DATE" => "Y",
	"DISPLAY_PICTURE" => "Y",
	"DISPLAY_PREVIEW_TEXT" => "Y",
	"USE_SHARE" => "N",
	"AJAX_OPTION_ADDITIONAL" => "",
	"SEF_URL_TEMPLATES" => array(
		"news" => "",
		"section" => "",
		"detail" => "#ELEMENT_ID#/",
	)
	),
	false
);?>
<?if($_REQUEST['send'] == 1 && ($_REQUEST['name'] == "" || $_REQUEST['text_item'] == "")):?>
        <div class="warning">Заполните поля отмеченные <span class="highlight">*</span></div>
<?endif;?>
<?if($_REQUEST['send'] == 1 && $_REQUEST['name'] != "" && $_REQUEST['text_item'] != ""):?>
        <div class="end_res">Спасибо! Ваш отзыв принят.</div><br />
<?endif;?>
        <p class="listhead">Оставить отзыв</p>
        <div class="blockabout">
        <form id="commentForm" method="post" action="">
          <input name="send" type="hidden" value="1">
          <fieldset>
            <div class="row">
              <div class="column column1"><label>Ваше имя:<span class="highlight">*</span></label></div>
              <div class="column column2"><input type="text" class="textinput" value="" name="name"></div>
            </div>
            <div class="row">
              <div class="column column1"><label>№ договора:</label></div>
              <div class="column column2"><input type="text" class="textinput" value="" name="contract_number"></div>
            </div>
            <div class="row">
              <label>Текст сообщения:<span class="highlight">*</span></label><br>
              <textarea name="text_item" cols="" rows=""></textarea>
            </div>
            <div class="row">
              <p><i><span class="highlight">*</span> - поля, обязательные для заполнения</i></p>
            </div>
            <div class="row">
              <input type="text" autocomplete="off" class="textinput input_h" value="" name="mail">
              <input type="submit" value="Отправить" class="btn-send">
            </div>
          </fieldset>
        </form>
        </div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>