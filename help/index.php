<?
define("NOT_SHOW_BREADCRUMB", 1);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Помощь в выборе пластиковых окон");
?>
<style>
#help_form {
    background: url("/images/helpback.png") no-repeat scroll 0 0 transparent;
    height: 259px;
    padding: 1px;
    width: 684px;
}

#help_form p {
    color: #e93e00;
    font-family: Arial;
    font-size: 16px;
    font-weight: 600;
    margin: 11px 0 0 15px;
}
#help_form div {
    margin-left: 15px;
}
#help_form div span {
    display: block;
    font-family: Arial;
    font-size: 13px;
    margin-top: 3px;
}
#helper_form_act ul li {
    border-bottom: 1px solid #D29A20;
    color: #330000;
    font-family: Arial;
    font-size: 12px;
    font-style: italic;
    list-style-image: url("/images/help_point.png");
    list-style-position: inside;
}
#helper_form_act span {
    background: url("/images/inp_h.png") no-repeat scroll 0 0 transparent;
    display: block;
    height: 32px;
    margin-left: 10px;
    width: 199px;
}
#helper_form_act input {
    border: medium none;
    color: #996666;
    font-family: Arial;
    font-size: 12px;
    font-style: italic;
}
#rec {
    float: left;
    margin-top: -10px;
    overflow: hidden;
    width: 380px;
	display:none;
}

.podbor {
	width:700px;
}

.podbor .item{
	width:600px;
	overflow:hidden;
	margin-bottom:30px;
}

.podbor .item img{
	float:left;
	padding-right:10px;
}

.listhead {
    border-bottom: 1px dashed #9F0511;
    color: #9F0511;
    cursor: pointer;
    display: inline;
    font-family: Arial,Helvetica,sans-serif;
    font-size: 16px !important;
    margin-left: -10px;
    padding: 2px 10px;
    width: auto;
}
.blockabout {
    display: none;
    font-family: Arial,Helvetica,sans-serif;
    font-size: 12px;
    margin: 15px 0 0 40px;
    padding: 0;
    width: 308px;
    z-index: 1;
}
</style>
<?$APPLICATION->IncludeComponent("sfera:helper", ".default", array());?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>