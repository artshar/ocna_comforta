<?
class calculator 
{
    const RATE_USD = 40;
    
    function getLeafNames()
    {
		return array(
            1 => 'Не открывается',
            2 => 'Поворотное',
            3 => 'Поворотно-откидное',
            4 => 'Фрамуга',
            5 => 'Не сдвигается',
            6 => 'Сдвигается',
        );
    }

    function getBaseFactor($base_id)
    {
		switch ($base_id) {
            case 'elit':
                return BASE_FACTOR_ELIT;
            case 'ideal':
                return BASE_FACTOR_IDEAL;
            case 'classic':
                return BASE_FACTOR_CLASSIC;
            case 'econom':
                return BASE_FACTOR_ECONOM;
        }
    }

    function getCalculatorItemId($leaf_name)
    {
		return array_search($leaf_name, $this->getLeafNames());
    }

    function getItem($id)
    {
        $id = (int)$id;
        if($id > 0)
        {
            $DataBase = & singleton('DataBase');
        	$query = "SELECT * FROM `calculator_items` WHERE `id` = $id";
        	$result = $DataBase->select($query);
        	if($DataBase->get_count_row())
            {
        		return mysql_fetch_assoc($result);
        	}
        }
		return false;
    }

    function insertItem($param)
    {
		$sql_param = array(
			'id' => 0,
			'name' => '',
			'size_from_w' => 0,
			'size_from_h' => 0,
			'size_to_w' => 0,
			'size_to_h' => 0,
			'size_step' => 0,
		);

		if(!isset($param['id'])){
			$param['id'] = 0;
		}else{
			$param['id'] = (int)$param['id'];
		}
        $DataBase = & singleton('DataBase');
		// вставка
		if($param['id'] == 0){
			foreach($sql_param as $param_name => $param_value){
				if(isset($param[$param_name])){
					$sql_param[$param_name] = $param[$param_name];
				}
			}
			unset($sql_param['id']);
			$DataBase->Insert('calculator_items', $sql_param);
			return $DataBase->GetLastInsertId();
		}else{
			foreach($sql_param as $param_name => $param_value){
				if(!isset($param[$param_name])){
					unset($sql_param[$param_name]);
				}else{
					$sql_param[$param_name] = $param[$param_name];
				}
			}
			unset($sql_param['id']);
			$DataBase->Update('calculator_items', $sql_param, 'id = '. $param['id']);
			return $param['id'];
		}
    }

    function deleteItem($id)
    {
    	$id = (int)$id;
        if($id > 0)
        {
            $DataBase = & singleton('DataBase');
    		$query = "DELETE FROM `calculator_items` WHERE id = '$id'";
			$DataBase->query($query);
    		$query = "DELETE FROM `calculator_item_sizes` WHERE calculator_item_id = '$id'";
			$DataBase->query($query);
			return true;
    	}
    	return false;
    }

    public function hasSize($calculator_item_id, $w, $h)
    {
        $DataBase = & singleton('DataBase');
        $query = "SELECT * 
            FROM `calculator_item_sizes` 
            WHERE `calculator_item_id` = ".to_int($calculator_item_id)." 
                AND size_w = ".to_int($w)."
                AND size_h = ".to_int($h)."
            LIMIT 1";
        $DataBase->select($query);
		return $DataBase->get_count_row() ? true : false;
    }

    public function findSize($calculator_item_id, $w, $h)
    {
        $DataBase = & singleton('DataBase');
        $query = "SELECT * 
            FROM `calculator_item_sizes` 
            WHERE `calculator_item_id` = ".to_int($calculator_item_id)." 
                AND size_w = ".to_int($w)."
                AND size_h = ".to_int($h)."
            LIMIT 1";
        $result = $DataBase->select($query);
        if($DataBase->get_count_row())
        {
            return mysql_fetch_assoc($result);
        }
		return NULL;
    }

    public function findPrice($calculator_item_id, $w, $h)
    {
        $DataBase = & singleton('DataBase');
        $query = "SELECT * 
            FROM `calculator_item_sizes` 
            WHERE `calculator_item_id` = ".to_int($calculator_item_id)." 
                AND size_w >= ".to_int($w)."
                AND size_h >= ".to_int($h)."
            ORDER BY size_w, size_h
            LIMIT 1";
        $result = $DataBase->select($query);
        if($DataBase->get_count_row())
        {
            $row = mysql_fetch_assoc($result);
            return $row['price'];
        }
		return 0;
    }

    public function addSize($calculator_item_id, $w, $h)
    {
        $DataBase = & singleton('DataBase');
    	$calculator_item_id = (int)$calculator_item_id;
    	$w = (int)$w;
    	$h = (int)$h;
        if(!$this->hasSize($calculator_item_id, $w, $h))
        {
            $DataBase->Insert('calculator_item_sizes', array('calculator_item_id' => $calculator_item_id, 'size_w' => $w, 'size_h' => $h, 'saved' => 1));
            return true;
        }
        else
        {
            $DataBase->Update('calculator_item_sizes', array('size_w' => $w, 'size_h' => $h, 'saved' => 1), "calculator_item_id = {$calculator_item_id}");
            return true;
        }
		return false;
    }

    public function removeSize($calculator_item_id, $w, $h)
    {
        $DataBase = & singleton('DataBase');
        $query = "DELETE 
            FROM `calculator_item_sizes`  
            WHERE `calculator_item_id` = ".to_int($calculator_item_id)." 
                AND size_w = ".to_int($w)."
                AND size_h = ".to_int($h)."";
		$DataBase->query($query);
		return true;
    }

    public function getConfigurationPrice($node)
    {
        $price = 0;
        
        list( , $configuration) = each($node->xpath("./@id"));
        $configuration = (string)$configuration;
        
        list( , $frame_type_title) = each($node->xpath("./frame_type/title"));
        $frame_type_title = (string)$frame_type_title;
        if($frame_type_title == 'балконный блок')
        {
            
            $leafs = $node->xpath("./frame_type/leafs/leaf");
            foreach ($leafs as $leaf)
            {
                list( , $width) = each($leaf->xpath("./width"));
                $width = (string)$width;
                list( , $height) = each($leaf->xpath("./height"));
                $height = (string)$height;
            
                list( , $leaf_name) = each($leaf->xpath("./title"));
                $leaf_name = (string)$leaf_name;
                $calculator_item_id = $this->getCalculatorItemId($leaf_name);
                $calculator_item_row = $this->getItem($calculator_item_id);
                
                if($calculator_item_row)
                {
                    // проверяем граничные значения
                    if($width < $calculator_item_row['size_from_w'])
                    {
                        $width = $calculator_item_row['size_from_w'];
                    }
                    elseif($width > $calculator_item_row['size_to_w'])
                    {
                        $width = $calculator_item_row['size_to_w'];
                    }
                    if($height < $calculator_item_row['size_from_h'])
                    {
                        $height = $calculator_item_row['size_from_h'];
                    }
                    elseif($height > $calculator_item_row['size_to_h'])
                    {
                        $height = $calculator_item_row['size_to_h'];
                    }
                    
                    $price += $this->findPrice($calculator_item_id, $width, $height);
                }
            }
            
            // дверь
            $height = 600;
            list( , $width) = each($node->xpath("./frame_type/width"));
            $width = (string)$width;
            
            $doors = $node->xpath("./frame_type/door_position");
            foreach ($doors as $door)
            {
                list( , $width) = each($door->xpath("./width"));
                $width = (string)$width;
                list( , $height) = each($door->xpath("./height"));
                $height = (string)$height;
            
                list( , $leaf_name) = each($leaf->xpath("./title"));
                $leaf_name = (string)$leaf_name;
                
                $calculator_item_id = strpos($leaf_name, 'откидное') === false ? 2 : 3;
                $calculator_item_row = $this->getItem($calculator_item_id);
                if($calculator_item_row)
                {
                    // проверяем граничные значения
                    if($width < $calculator_item_row['size_from_w'])
                    {
                        $width = $calculator_item_row['size_from_w'];
                    }
                    elseif($width > $calculator_item_row['size_to_w'])
                    {
                        $width = $calculator_item_row['size_to_w'];
                    }
                    if($height < $calculator_item_row['size_from_h'])
                    {
                        $height = $calculator_item_row['size_from_h'];
                    }
                    elseif($height > $calculator_item_row['size_to_h'])
                    {
                        $height = $calculator_item_row['size_to_h'];
                    }
                    
                    $price += $this->findPrice($calculator_item_id, $width, $height);
                }
            }
        }
        else
        {
            list( , $width) = each($node->xpath("./frame_type/width"));
            $width = (string)$width;
            list( , $height) = each($node->xpath("./frame_type/height"));
            $height = (string)$height;
            
            $count_leaf = count($node->xpath("./frame_type/leafs/leaf"));
            if($count_leaf)
            {
                $width = round($width / $count_leaf, 0);
            }
            
            
            $leafs = $node->xpath("./frame_type/leafs/leaf");
            foreach ($leafs as $leaf)
            {
                list( , $leaf_name) = each($leaf->xpath("./title"));
                $leaf_name = (string)$leaf_name;
                $calculator_item_id = $this->getCalculatorItemId($leaf_name);
                $calculator_item_row = $this->getItem($calculator_item_id);
                
                if($calculator_item_row)
                {
                    // проверяем граничные значения
                    if($width < $calculator_item_row['size_from_w'])
                    {
                        $width = $calculator_item_row['size_from_w'];
                    }
                    elseif($width > $calculator_item_row['size_to_w'])
                    {
                        $width = $calculator_item_row['size_to_w'];
                    }
                    if($height < $calculator_item_row['size_from_h'])
                    {
                        $height = $calculator_item_row['size_from_h'];
                    }
                    elseif($height > $calculator_item_row['size_to_h'])
                    {
                        $height = $calculator_item_row['size_to_h'];
                    }
                    
                    $price += $this->findPrice($calculator_item_id, $width, $height);
                }
            }
            
            // фрамуга
            $height = 600;
            list( , $width) = each($node->xpath("./frame_type/width"));
            $width = (string)$width;
            
            $framugas = $node->xpath("./frame_type/framuga");
            foreach ($framugas as $framuga)
            {
                list( , $framuga_name) = each($framuga->xpath("./title"));
                $framuga_name = (string)$framuga_name;
                if($framuga_name == 'Нет')
                {
                    continue;
                }
                $calculator_item_id = 4;
                $calculator_item_row = $this->getItem($calculator_item_id);
                if($calculator_item_row)
                {
                    // проверяем граничные значения
                    if($width < $calculator_item_row['size_from_w'])
                    {
                        $width = $calculator_item_row['size_from_w'];
                    }
                    elseif($width > $calculator_item_row['size_to_w'])
                    {
                        $width = $calculator_item_row['size_to_w'];
                    }
                    if($height < $calculator_item_row['size_from_h'])
                    {
                        $height = $calculator_item_row['size_from_h'];
                    }
                    elseif($height > $calculator_item_row['size_to_h'])
                    {
                        $height = $calculator_item_row['size_to_h'];
                    }
                    
                    $price += $this->findPrice($calculator_item_id, $width, $height);
                }
            }
        }
        list( , $base_id) = each($node->xpath("./base/id"));
        $base_id = (string)$base_id;
        $base_factor = $this->getBaseFactor($base_id);
        $price = round($price + $price * $base_factor, 0);
        
		return $price;
    }

    public function getConfigurationImage($node)
    {
        $images = '';
        
        list( , $configuration) = each($node->xpath("./@id"));
        $configuration = (string)$configuration;
        
        list( , $frame_type_title) = each($node->xpath("./frame_type/title"));
        $frame_type_title = (string)$frame_type_title;
        if($frame_type_title == 'балконный блок')
        {
            $leafs = $node->xpath("./frame_type/leafs/leaf");
            foreach ($leafs as $leaf)
            {
                list( , $width) = each($leaf->xpath("./width"));
                $width = (string)$width;
                list( , $height) = each($leaf->xpath("./height"));
                $height = (string)$height;
            
                list( , $leaf_name) = each($leaf->xpath("./title"));
                $leaf_name = (string)$leaf_name;
                $calculator_item_id = $this->getCalculatorItemId($leaf_name);
                $calculator_item_row = $this->getItem($calculator_item_id);
                
                if($calculator_item_row)
                {
                    // проверяем граничные значения
                    if($width < $calculator_item_row['size_from_w'])
                    {
                        $width = $calculator_item_row['size_from_w'];
                    }
                    elseif($width > $calculator_item_row['size_to_w'])
                    {
                        $width = $calculator_item_row['size_to_w'];
                    }
                    if($height < $calculator_item_row['size_from_h'])
                    {
                        $height = $calculator_item_row['size_from_h'];
                    }
                    elseif($height > $calculator_item_row['size_to_h'])
                    {
                        $height = $calculator_item_row['size_to_h'];
                    }
                    
                    $price += $this->findPrice($calculator_item_id, $width, $height);
                }
            }
            
            // дверь
            $height = 600;
            list( , $width) = each($node->xpath("./frame_type/width"));
            $width = (string)$width;
            
            $doors = $node->xpath("./frame_type/door_position");
            foreach ($doors as $door)
            {
                list( , $width) = each($door->xpath("./width"));
                $width = (string)$width;
                list( , $height) = each($door->xpath("./height"));
                $height = (string)$height;
            
                list( , $leaf_name) = each($leaf->xpath("./title"));
                $leaf_name = (string)$leaf_name;
                
                $calculator_item_id = strpos($leaf_name, 'откидное') === false ? 2 : 3;
                $calculator_item_row = $this->getItem($calculator_item_id);
                if($calculator_item_row)
                {
                    // проверяем граничные значения
                    if($width < $calculator_item_row['size_from_w'])
                    {
                        $width = $calculator_item_row['size_from_w'];
                    }
                    elseif($width > $calculator_item_row['size_to_w'])
                    {
                        $width = $calculator_item_row['size_to_w'];
                    }
                    if($height < $calculator_item_row['size_from_h'])
                    {
                        $height = $calculator_item_row['size_from_h'];
                    }
                    elseif($height > $calculator_item_row['size_to_h'])
                    {
                        $height = $calculator_item_row['size_to_h'];
                    }
                    
                    $price += $this->findPrice($calculator_item_id, $width, $height);
                }
            }
        }
        else
        {
            $count_leaf = count($node->xpath("./frame_type/leafs/leaf"));
            
            $leafs = $node->xpath("./frame_type/leafs/leaf");
            foreach ($leafs as $leaf)
            {
                list( , $leaf_name) = each($leaf->xpath("./title"));
                $leaf_name = (string)$leaf_name;
                switch ($leaf_name) {
        			case 'Не открывается':
        				$images .= '<img src="/img/leaf_type_1.png" />';
        				break;
        			case 'Не открывается':
        				$images .= '<img src="/img/leaf_type_1.png" />';
        				break;
        			case 'Не открывается':
        				$images .= '<img src="/img/leaf_type_1.png" />';
        				break;
        			default:
        				# code...
        				break;
        		}
                if($calculator_item_row)
                {
                    // проверяем граничные значения
                    if($width < $calculator_item_row['size_from_w'])
                    {
                        $width = $calculator_item_row['size_from_w'];
                    }
                    elseif($width > $calculator_item_row['size_to_w'])
                    {
                        $width = $calculator_item_row['size_to_w'];
                    }
                    if($height < $calculator_item_row['size_from_h'])
                    {
                        $height = $calculator_item_row['size_from_h'];
                    }
                    elseif($height > $calculator_item_row['size_to_h'])
                    {
                        $height = $calculator_item_row['size_to_h'];
                    }
                    
                    $price += $this->findPrice($calculator_item_id, $width, $height);
                }
            }
            
            // фрамуга
            $height = 600;
            list( , $width) = each($node->xpath("./frame_type/width"));
            $width = (string)$width;
            
            $framugas = $node->xpath("./frame_type/framuga");
            foreach ($framugas as $framuga)
            {
                $calculator_item_id = 4;
                $calculator_item_row = $this->getItem($calculator_item_id);
                if($calculator_item_row)
                {
                    // проверяем граничные значения
                    if($width < $calculator_item_row['size_from_w'])
                    {
                        $width = $calculator_item_row['size_from_w'];
                    }
                    elseif($width > $calculator_item_row['size_to_w'])
                    {
                        $width = $calculator_item_row['size_to_w'];
                    }
                    if($height < $calculator_item_row['size_from_h'])
                    {
                        $height = $calculator_item_row['size_from_h'];
                    }
                    elseif($height > $calculator_item_row['size_to_h'])
                    {
                        $height = $calculator_item_row['size_to_h'];
                    }
                    
                    $price += $this->findPrice($calculator_item_id, $width, $height);
                }
            }
        }
        list( , $base_id) = each($node->xpath("./base/id"));
        $base_id = (string)$base_id;
        $base_factor = $this->getBaseFactor($base_id);
        $price = round($price + $price * $base_factor, 0);
        
        
        
		return $price;
    }

    
    
}
?>