<?php
/**
 * Система управления сайтом HostCMS v. 5.xx
 * Copyright © http://hostcms-cms.ru/
 *
 * Модуль order_calculation.
 *
 * Файл: /modules/order_calculation/order_calculation.php
 *
 * @author Сергей Дылов
 * @version 1.х
 */

/* Путь к модулю */
$module_path_name = 'order_calculation';

/* Имя модуля */
$module_name = 'Расчет заказа';

// Указание соответствия имени класса и модуля
$GLOBALS['HOSTCMS_CLASS'][$module_path_name] = $module_path_name;

$kernel = & singleton('kernel');

/* Список файлов для загрузки */
$kernel->AddModuleFile($module_path_name, CMS_FOLDER . "modules/{$module_path_name}/{$module_path_name}.class.php");

// Добавляем версию модуля
$kernel->add_modules_version($module_path_name, '1.x', '05.08.2011');

// Если раздел администрирования
if (defined('IS_ADMIN_PART'))
{
	// Помещаем модуль в меню
	$AdminMenu = new AdminMenu();
	$AdminMenu->AddAdminMenuItem(492, $module_name,
	"DoLoadAjax('/admin/{$module_path_name}/{$module_path_name}.php', '', 0, 'load_data', 0, 0, 0, 0);
	return false;", "/admin/{$module_path_name}/{$module_path_name}.php", $module_path_name, 1);
}
?>