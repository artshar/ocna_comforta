<?php

// Подключаем основные классы.
require_once('../../main_classes.php');

$admin = new Admin();

// Проверка авторизации пользователя.
$admin->admin_session_valid('calculator');

$kernel = & singleton('kernel');

// Инсталляция всех модулей ядра.
$kernel->LoadModules(!isset($_REQUEST['JsHttpRequest']));

// Инициализация CURRENT_SITE и констант.
$admin->admin_init();

if (!isset($_REQUEST['JsHttpRequest']))
{
	do_html_header_admin('Калькулятор');
}
$admin_forms = new admin_forms();


// Обработчик кнопок "Сохранить" и "Применить" формы "Редактирования/Добавления сущности"
if (isset($_REQUEST['apply_item']) || isset($_REQUEST['save_item']))
{
	ob_start();

	include(CMS_FOLDER . 'admin/calculator/action/add_item.php');

	$message = ob_get_clean();
	$JsHttpRequest = new JsHttpRequest(SITE_CODING);

	// При добавлени (не сохранении) - отправляем команду на загрузку
	if (isset($_REQUEST['apply_item']))
	{
		// Отображаем форму
		include(CMS_FOLDER . '/admin/calculator/action/load_items.php');
		$GLOBALS['_RESULT'] = $admin_forms->ShowForm(100002, 'load_data');

		// Добавляем определенное выше сообщение
		$GLOBALS['_RESULT']['error'] = $message;
	}
	else
	{
		// Отправляем только сообщение о произведенных действиях
		$GLOBALS['_RESULT'] = array(
			'error' => $message,
			'form_html' => '',
			'title' => '');
	}
	echo $JsHttpRequest->LOADER;
	exit();
}

// Обработчик кнопок "Сохранить" и "Применить" формы "Редактирования/Добавления сущности"
if (isset($_REQUEST['apply_sizes']) || isset($_REQUEST['save_sizes']))
{
	ob_start();

	include(CMS_FOLDER . 'admin/calculator/action/save_sizes.php');

	$message = ob_get_clean();
	$JsHttpRequest = new JsHttpRequest(SITE_CODING);

	// При добавлени (не сохранении) - отправляем команду на загрузку
	if (isset($_REQUEST['apply_sizes']))
	{
		// Отображаем форму
		include(CMS_FOLDER . '/admin/calculator/action/load_items.php');
		$GLOBALS['_RESULT'] = $admin_forms->ShowForm(100002, 'load_data');

		// Добавляем определенное выше сообщение
		$GLOBALS['_RESULT']['error'] = $message;
	}
	else
	{
		// Отправляем только сообщение о произведенных действиях
		$GLOBALS['_RESULT'] = array(
			'error' => $message,
			'form_html' => '',
			'title' => '');
	}
	echo $JsHttpRequest->LOADER;
	exit();
}

// Отображение форм
switch (to_int($_REQUEST['admin_forms_id']))
{
	case 100002:
	default:
		{
			include(CMS_FOLDER . '/admin/calculator/action/load_items.php');
			$admin_forms->ProcessAjax(100002);
			break;
		}
}

// Отображение "Подвала" сайта
if (!isset($_REQUEST['JsHttpRequest']))
{
	do_html_footer_admin();
}
?>