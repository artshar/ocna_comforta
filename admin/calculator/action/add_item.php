<?php
/**
 *
 * Обработчик добавления/редактирования 
 *
 *
 * @author 
 * @version 1.x
 */
$calculator = & singleton('calculator');
$DataBase = & singleton('DataBase');

// Обновление информационной группы
$calculator_item_id = to_int($_POST['calculator_item_id']);


$param = array();
$param['id'] = $calculator_item_id;
$param['name'] = to_str($_POST['calculator_item_name']);
$param['size_from_w'] = to_int($_POST['size_from_w']);
$param['size_from_h'] = to_int($_POST['size_from_h']);
$param['size_to_w'] = to_int($_POST['size_to_w']);
$param['size_to_h'] = to_int($_POST['size_to_h']);
$param['size_step'] = to_int($_POST['size_step']);

$new_id = $calculator->insertItem($param);

$DataBase->Update('calculator_item_sizes', array('saved' => 0), "calculator_item_id = {$new_id}");
// перекраиваем сетку
for($w=$param['size_from_w']; $w<=$param['size_to_w']; $w+=$param['size_step'])
{
    for($h=$param['size_from_h']; $h<=$param['size_to_h']; $h+=$param['size_step'])
    {
        $calculator->addSize($new_id, $w, $h);
    }    
}
$query = "DELETE FROM `calculator_item_sizes` WHERE saved = 0";
$DataBase->query($query);

// Информация о группе отредактирована
if ($calculator_item_id)
{
	show_message('Элемент сохранен');
}
else // Информационная группа добавлена
{
	show_message('Элемент добавлен');
}

$admin_forms = new admin_forms();
$window_id = $admin_forms->GetWindowId();
?><script type="text/javascript">
$.appendInput('<?php echo $window_id?>', 'EditAddItem', 'calculator_item_id', '<?php echo $new_id?>');
</script><?php

?>