<?php
/**
 *
 * Форма добаления/редактирования.
 * 
 * 
 * @author 
 * @version 1.x
 */
$DataBase = & singleton('DataBase');
$calculator = & singleton('calculator');

$calculator_item_row = $calculator->getItem($calculator_item_id);
    

$form_params = array();

$form_params['title'] = 'Сетка для элемента '. $calculator_item_row['name'];

$form_params['form_attribs']['method'] = 'post';
$form_params['form_attribs']['action'] = '/admin/calculator/calculator.php';
$form_params['form_attribs']['name'] = 'EditAddItem';

$admin_forms_fields = new admin_forms_fields();

// Формируем "хлебные крошки"
$new_path_array = array();

$new_path_array[] = array(
'link' => $admin_forms_fields->GetHtmlCallDoLoadAjax('/admin/calculator/calculator.php', '', 100002, 'load_data'),
'onclick' => $admin_forms_fields->GetOnClickCallDoLoadAjax('/admin/calculator/calculator.php', '', 100002, 'load_data'),
'name' => 'Калькулятор');


$form_params['path_array'] = $new_path_array;

$admin_forms_fields->admin_forms_fields($form_params);

$tab_id = $admin_forms_fields->AddTab('Основные параметры');

ob_start();
?>
<table id="sizesTable">
    <tr class="sizes_table_title">
        <th>ММ</th>
        <?
        for($w=$calculator_item_row['size_from_w']; $w<=$calculator_item_row['size_to_w']; $w+=$calculator_item_row['size_step'])
        {
            ?><th><?=$w?></th><?
        }
        ?>
    </tr>
    <?
    for($h=$calculator_item_row['size_from_h']; $h<=$calculator_item_row['size_to_h']; $h+=$calculator_item_row['size_step'])
    {
        ?><tr><th><?=$h?></th><?
        for($w=$calculator_item_row['size_from_w']; $w<=$calculator_item_row['size_to_w']; $w+=$calculator_item_row['size_step'])
        {
            $size_row = $calculator->findSize($calculator_item_id, $w, $h);
            ?><td><input type="text" name="size_<?=to_int($size_row['id'])?>" value="<?=to_int($size_row['price'])?>" /></td><?
        }
        ?></tr><?
    }
    ?>
</table>
<style>
#sizesTable {
    
}
#sizesTable th {
    background-color: #EAEAEA;
    padding: 5px 5px;
}
#sizesTable td {
    padding: 0 4px;
}
#sizesTable td input {
    width: 40px;
    border: 1px solid #999999;
    text-align: center;
    height: 19px;
    font-size: 11px;
    line-height: 14px;
    padding: 0;
}
#sizesTable .row_table_over
{
	background-color: #aaa;
}
#sizesTable .row_table_over_odd
{
	background-color: #aaa;
}
#sizesTable .row_table
{
	background-color: #FFFFFF;
}
#sizesTable .row_table_odd
{
	background-color: #ddd;
}

</style>
<script type="text/javascript">
$(function() {

	$("#sizesTable tr:not(.sizes_table_title):nth-child(odd)")
	.attr('class', 'row_table')
	.mouseover(function(){
		$(this).attr('class', 'row_table_over');
	})
	.mouseout(function(){
		$(this).attr('class', 'row_table');
	});
	
    $("#sizesTable tr:not(.sizes_table_title):nth-child(even)")
    .attr('class', 'row_table_odd')
	.mouseover(function(){
		$(this).attr('class', 'row_table_over_odd');
	})
	.mouseout(function(){
		$(this).attr('class', 'row_table_odd');
	});
});

</script>
<?




$sizes_html = ob_get_clean();
$param = array();
$param['tab_id'] = $tab_id;
$param['name'] = 'sizes'; // Как есть
$param['type'] = 15; // Как есть
$param['value'] = $sizes_html;
$param['div_attributes']['style'] = '';
$admin_forms_fields->AddField($param);



$param = array();
$param['tab_id'] = $tab_id;
$param['name'] = 'calculator_item_id';
$param['type'] = 4; // Скрытое поле.
$param['value'] = $calculator_item_id;
$admin_forms_fields->AddField($param);

// Кнопка "Сохранить"
$param = array();
$param['name'] = 'save_sizes';
$param['caption'] = 'Сохранить';
$param['image'] = '/admin/images/add.gif';
$param['attributes']['onclick'] = "return SendForm('{$admin_forms_fields->GetWindowId()}', '{$admin_forms_fields->AAction}', '{$admin_forms_fields->AAdditionalParams}', this, ".to_int($_REQUEST['admin_forms_id']).", 'load_data', " . to_int($admin_forms_fields->form_params['current_page']) . ", " . to_int($admin_forms_fields->form_params['on_page']) . " )";
$param['attributes']['type'] = 'button';
$admin_forms_fields->AddButton($param);

// Кнопка "Применить".
$param = array();
$param['name'] = 'apply_sizes';
$param['caption'] = 'Применить';
$param['image'] = '/admin/images/add.gif';
$param['attributes']['onclick'] = "return SendForm('{$admin_forms_fields->GetWindowId()}', '{$admin_forms_fields->AAction}', '{$admin_forms_fields->AAdditionalParams}', this, ".to_int($_REQUEST['admin_forms_id']).", 'load_data', " . to_int($admin_forms_fields->form_params['current_page']) . ", " . to_int($admin_forms_fields->form_params['on_page']) . " )";
$param['attributes']['type'] = 'submit';
$admin_forms_fields->AddButton($param);

// Отображение формы.
$admin_forms_fields->ShowForm();
