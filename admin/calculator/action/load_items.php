<?php
function LoadAjaxData(&$admin_forms)
{
	$on_page = $admin_forms->GetOnPageCount();

	$page = to_int($_REQUEST['limit']);
	$begin = ($page == 0) ? 0 : ($page - 1) * $on_page;

	$param = array();

	$param['title'] = 'Элементы окна';

	$param['limit'][0] = array('begin' => $begin, 'count' => $on_page);

	$DataBase = & singleton('DataBase');

	// Преобразуем идентификаторы групп в ссылки на группы.
	$new_path_array = array();
	$new_path_array[0]['name'] = 'Калькулятор';
	$new_path_array[0]['link'] = $admin_forms->GetHtmlCallDoLoadAjax('/admin/calculator/calculator.php', '', 100002, 'load_data');
	$new_path_array[0]['onclick'] = $admin_forms->GetOnClickCallDoLoadAjax('/admin/calculator/calculator.php', '', 100002, 'load_data');

	$param['path_array'] = $new_path_array;


	// Запрос для выборки элементов магазина
	$param['data'][] = "SELECT * FROM `calculator_items`
		WHERE 1
		{FILTER_STRING} {ORDER_STRING} {LIMIT}";

	// Запрос на количество элементов
	$count = "SELECT count(*) as count FROM `calculator_items`
		WHERE 1 {FILTER_STRING}";

	// Формируем верхнее меню.
	$k = 0;
	$menu = array();
	$menu[$k]['name'] = 'Добавить элемент';
	$menu[$k]['link'] = $admin_forms->GetHtmlCallTrigerSingleAction('edit', 'check_0_0', 100002, 0, 0);
	$menu[$k]['onclick'] = $admin_forms->GetOnClickCallTrigerSingleAction('edit', 'check_0_0', 100002, 0, 0);
	$menu[$k]['icon'] = '/admin/images/add.gif';

	// Формируем массив меню.
	$menu_array = array();
	$menu_array[] = $menu;
    

	$param['field_params'][0]['min_size']['callback_function'] = 'callback_function_min_size';
	// Функция обратного вызова для названия группы
	function callback_function_min_size($field_value, $value, $row)
	{
        echo $row['size_from_w'] .' x '.$row['size_from_h'] ;
	}

	$param['field_params'][0]['max_size']['callback_function'] = 'callback_function_max_size';
	// Функция обратного вызова для названия группы
	function callback_function_max_size($field_value, $value, $row)
	{
        echo $row['size_to_w'] .' x '.$row['size_to_h'] ;
	}

	$param['total_count'][] = $count;
	$param['current_page'] = $page;
	$param['on_page'] = $on_page;
	$param['menus'] = $menu_array;

	return $param;
}

// Удаление
function delete(&$param)
{
	ob_start();
    $calculator = & singleton('calculator');
    $count = 0;
	if (isset($param['data']))
	{
		foreach ($param['data'] as $dataset)
		{
			foreach ($dataset as $key => $value)
			{
				$calculator->deleteItem($key);
                ++$count;
			}
			if ($count == 1)
			{
				show_message('Элемент удален');
			}
			else
			{
				show_message('Элементы удалены');
			}
		}
	}
	$param['error'] = ob_get_clean();
}

// редактирование
function edit(&$param)
{
	ob_start();
	if (isset($param['data']))
	{
		foreach ($param['data'] as $dataset)
		{
			foreach ($dataset as $key => $value)
			{
				$calculator_item_id = intval($key);
				include(CMS_FOLDER . 'admin/calculator/action/form_edit_add_calculator_item.php');
				$param['title'] = $calculator_item_id == 0 ? 'Добавление элемента' : 'Редактирование элемента';
				break;
			}
		}
	}

	$param['result'] = ob_get_clean();
	return true;
}

// редактирование
function grid(&$param)
{
	ob_start();
	if (isset($param['data']))
	{
		foreach ($param['data'] as $dataset)
		{
			foreach ($dataset as $key => $value)
			{
				$calculator_item_id = intval($key);
				include(CMS_FOLDER . 'admin/calculator/action/show_sizes.php');
				$param['title'] = 'Сетка для элемента';
				break;
			}
		}
	}

	$param['result'] = ob_get_clean();
	return true;
}

?>