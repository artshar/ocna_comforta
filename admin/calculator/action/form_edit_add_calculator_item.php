<?php
/**
 *
 * Форма добаления/редактирования.
 * 
 * 
 * @author 
 * @version 1.x
 */
$calculator = & singleton('calculator');

if ($calculator_item_id)
{
	// Получаем данные о сообщении
	$calculator_item_row = $calculator->getItem($calculator_item_id);
    
	$calculator_item_name = $calculator_item_row['name'];
	$size_from_w = $calculator_item_row['size_from_w'];
    $size_from_h = $calculator_item_row['size_from_h'];
    $size_to_w = $calculator_item_row['size_to_w'];
    $size_to_h = $calculator_item_row['size_to_h'];
    $size_step = $calculator_item_row['size_step'];
}
else // Добавление сообщения
{
	$calculator_item_name = '';
	$size_from_w = '';
    $size_from_h = '';
    $size_to_w = '';
    $size_to_h = '';
    $size_step = '';
}

$form_params = array();

$form_params['title'] = $calculator_item_id == 0 ? 'Добавление элемента' : 'Редактирование элемента';

$form_params['form_attribs']['method'] = 'post';
$form_params['form_attribs']['action'] = '/admin/calculator/calculator.php';
$form_params['form_attribs']['name'] = 'EditAddItem';

$admin_forms_fields = new admin_forms_fields();

// Формируем "хлебные крошки"
$new_path_array = array();

$new_path_array[] = array(
'link' => $admin_forms_fields->GetHtmlCallDoLoadAjax('/admin/calculator/calculator.php', '', 100002, 'load_data'),
'onclick' => $admin_forms_fields->GetOnClickCallDoLoadAjax('/admin/calculator/calculator.php', '', 100002, 'load_data'),
'name' => 'Калькулятор');


$form_params['path_array'] = $new_path_array;

$admin_forms_fields->admin_forms_fields($form_params);

$tab_id = $admin_forms_fields->AddTab('Основные параметры');

// Название 
$param = array();
$param['tab_id'] = $tab_id;
$param['name'] = 'calculator_item_name';
$param['caption'] = 'Название';
$param['type'] = 0; // Поле ввода.
$param['value'] = $calculator_item_name;
$param['attributes']['class'] = 'large';
$param['format']['maxlen']['value'] = 255;
$admin_forms_fields->AddField($param);

$admin_forms_fields->AddSeparator($tab_id);

// Min ширина 
$param = array();
$param['tab_id'] = $tab_id;
$param['name'] = 'size_from_w';
$param['caption'] = 'Min ширина';
$param['type'] = 0; // Поле ввода.
$param['value'] = $size_from_w;
$param['div_attributes']['style'] = 'float: left;';
$param['attributes']['style'] = 'width: 120px;';
$admin_forms_fields->AddField($param);

// Min высота 
$param = array();
$param['tab_id'] = $tab_id;
$param['name'] = 'size_from_h';
$param['caption'] = 'Min высота';
$param['type'] = 0; // Поле ввода.
$param['value'] = $size_from_h;
$param['div_attributes']['style'] = 'float: left;';
$param['attributes']['style'] = 'width: 120px;';
$admin_forms_fields->AddField($param);

$admin_forms_fields->AddSeparator($tab_id);

// Max ширина 
$param = array();
$param['tab_id'] = $tab_id;
$param['name'] = 'size_to_w';
$param['caption'] = 'Max ширина';
$param['type'] = 0; // Поле ввода.
$param['value'] = $size_to_w;
$param['div_attributes']['style'] = 'float: left;';
$param['attributes']['style'] = 'width: 120px;';
$admin_forms_fields->AddField($param);

// Max высота 
$param = array();
$param['tab_id'] = $tab_id;
$param['name'] = 'size_to_h';
$param['caption'] = 'Max высота';
$param['type'] = 0; // Поле ввода.
$param['value'] = $size_to_h;
$param['div_attributes']['style'] = 'float: left;';
$param['attributes']['style'] = 'width: 120px;';
$admin_forms_fields->AddField($param);

$admin_forms_fields->AddSeparator($tab_id);

// Шаг сетки
$param = array();
$param['tab_id'] = $tab_id;
$param['name'] = 'size_step';
$param['caption'] = 'Шаг сетки';
$param['type'] = 0; // Поле ввода.
$param['value'] = $size_step;
$param['div_attributes']['style'] = 'float: left;';
$param['attributes']['style'] = 'width: 120px;';
$admin_forms_fields->AddField($param);



if($calculator_item_id)
{
	$param = array();
	$param['tab_id'] = $tab_id;
	$param['name'] = 'calculator_item_id';
	$param['type'] = 4; // Скрытое поле.
	$param['value'] = $calculator_item_id;
	$admin_forms_fields->AddField($param);
}

// Кнопка "Сохранить"
$param = array();
$param['name'] = 'save_item';
$param['caption'] = 'Сохранить';
$param['image'] = '/admin/images/add.gif';
$param['attributes']['onclick'] = "return SendForm('{$admin_forms_fields->GetWindowId()}', '{$admin_forms_fields->AAction}', '{$admin_forms_fields->AAdditionalParams}', this, ".to_int($_REQUEST['admin_forms_id']).", 'load_data', " . to_int($admin_forms_fields->form_params['current_page']) . ", " . to_int($admin_forms_fields->form_params['on_page']) . " )";
$param['attributes']['type'] = 'button';
$admin_forms_fields->AddButton($param);

// Кнопка "Применить".
$param = array();
$param['name'] = 'apply_item';
$param['caption'] = 'Применить';
$param['image'] = '/admin/images/add.gif';
$param['attributes']['onclick'] = "return SendForm('{$admin_forms_fields->GetWindowId()}', '{$admin_forms_fields->AAction}', '{$admin_forms_fields->AAdditionalParams}', this, ".to_int($_REQUEST['admin_forms_id']).", 'load_data', " . to_int($admin_forms_fields->form_params['current_page']) . ", " . to_int($admin_forms_fields->form_params['on_page']) . " )";
$param['attributes']['type'] = 'submit';
$admin_forms_fields->AddButton($param);

// Отображение формы.
$admin_forms_fields->ShowForm();
