<style>

@media print {
    #header {display: none;}
    #footer {display: none;}
    .right_box {display: none;}
    .no_print {display: none;}
    #body {padding: 0;}
}
</style>
<script type="text/javascript">

var leaf_w = 50;
var leaf_h = 100;
var door_w = 50;
var door_h = 150;
var framuga_h = 36;
var conf_offset = 3;
var conf_border = 2;

function okDrawConfiguration(width, height)
{
    ctx.strokeRect(0,0,width,height);
}
function okDrawLeaf(type, offset, framuga)
{
    var offset_y = framuga ? framuga_h + 4 : 0;
    var offset_x = conf_offset + offset;
    
    ctx.fillRect(offset_x, offset_y, leaf_w, leaf_h - offset_y);
    ctx.clearRect(offset_x + conf_border, offset_y + conf_border, leaf_w - 2 * conf_border, (leaf_h - 2 * conf_border) - offset_y);
    
    switch (type) {
      case 1: // Поворотно-откидное
        ctx.beginPath();
            
        ctx.moveTo(offset_x + conf_border, offset_y);
        ctx.lineTo(offset_x + leaf_w / 2, leaf_h - conf_border);
        ctx.moveTo(offset_x + leaf_w / 2, leaf_h - conf_border);
        ctx.lineTo(offset_x + leaf_w - conf_border, offset_y);
            
        ctx.moveTo(offset_x + conf_border, offset_y + conf_border);
        ctx.lineTo(offset_x + leaf_w - conf_border, (leaf_h + offset_y)/2);
        ctx.moveTo(offset_x + leaf_w - conf_border, (leaf_h + offset_y)/2);
        ctx.lineTo(offset_x + conf_border, leaf_h - conf_border);
            
        ctx.stroke();        
        break;
      case 2: // Поворотное
        ctx.beginPath();
        ctx.moveTo(offset_x, offset_y + conf_border);
        ctx.lineTo(offset_x + leaf_w - conf_border, (leaf_h + offset_y)/2);
        ctx.moveTo(offset_x + leaf_w - conf_border, (leaf_h + offset_y)/2);
        ctx.lineTo(offset_x, leaf_h - conf_border);
        ctx.stroke();
        break
      case 3: // Сдвигается
        ctx.beginPath();
        ctx.moveTo(offset_x + 10, (leaf_h + offset_y)/2);
        ctx.lineTo(offset_x + leaf_w - 10, (leaf_h + offset_y)/2);
           
        ctx.moveTo(offset_x + 10, (leaf_h + offset_y)/2);
        ctx.lineTo(offset_x + 20, (leaf_h + offset_y)/2 - 5);
           
        ctx.moveTo(offset_x + 10, (leaf_h + offset_y)/2);
        ctx.lineTo(offset_x + 20, (leaf_h + offset_y)/2 + 5);
            
        ctx.moveTo(offset_x + leaf_w - 10, (leaf_h + offset_y)/2);
        ctx.lineTo(offset_x + leaf_w - 20, (leaf_h + offset_y)/2 - 5);
            
        ctx.moveTo(offset_x + leaf_w - 10, (leaf_h + offset_y)/2);
        ctx.lineTo(offset_x + leaf_w - 20, (leaf_h + offset_y)/2 + 5);
        ctx.stroke();
        break
      case 0: // Не открывается
      default:
        break;
    }

    return offset_x + leaf_w;
}

function okDrawDoor(opening, offset, tilt)
{
    var offset_x = conf_offset + offset;
    ctx.fillRect(offset_x, 0, door_w, door_h);
    ctx.clearRect(offset_x + conf_border, conf_border, door_w - 2 * conf_border, door_h - 2 * conf_border);
    
    ctx.beginPath();
    ctx.moveTo(offset_x, leaf_h);
    ctx.lineTo(offset_x + door_w, leaf_h);
    ctx.stroke();
    
    switch (opening) {
      case 'left':
        ctx.beginPath();
        ctx.moveTo(offset_x + door_w, 0);
        ctx.lineTo(offset_x + conf_border, door_w);
        ctx.moveTo(offset_x + conf_border, door_w);
        ctx.lineTo(offset_x + door_w, leaf_h);
        ctx.stroke();        
        break;
      case 'right': 
        ctx.beginPath();
        ctx.moveTo(offset_x, 0);
        ctx.lineTo(offset_x + door_w - conf_border, door_w);
        ctx.moveTo(offset_x + door_w - conf_border, door_w);
        ctx.lineTo(offset_x, leaf_h);
        ctx.stroke();
        break;
      default:
        break;
    }
    if(tilt){
        ctx.beginPath();
        ctx.moveTo(offset_x + conf_border, 0);
        ctx.lineTo(offset_x + door_w / 2, leaf_h);
        ctx.moveTo(offset_x + door_w / 2, leaf_h);
        ctx.lineTo(offset_x + door_w - conf_border, 0);
        ctx.stroke();
    }
    return offset_x + door_w;
}

function okDrawFramuga(leafs, tilt) {
    var width = leaf_w * leafs + conf_offset * (leafs - 1);
    ctx.fillRect(conf_offset, 0, width, framuga_h);
    ctx.clearRect(conf_offset + conf_border, conf_border, width - 2 * conf_border, framuga_h - 2 * conf_border);
    
    if(tilt){
        ctx.beginPath();
        ctx.moveTo(conf_offset + conf_border, conf_border);
        ctx.lineTo(width / 2 + 2 * conf_border, framuga_h - conf_border);
        ctx.moveTo(width / 2 + 2 * conf_border, framuga_h - conf_border);
        ctx.lineTo(width + 2 * conf_border, conf_border);
        ctx.stroke();
    }
}




</script>
<h1>Заявка № <?=$calculation_items_id?> от <?=$Date->datetime_format($calculation_row['calculation_items_date'])?></h1>
<p class="no_print">
	<a onclick="DoLoadAjax('/admin/order_calculation/order_calculation.php', '', '100000', 'load_data', 0, 0, '0', '1'); return false;" href="/admin/order_calculation/order_calculation.php?admin_forms_id=100000&amp;operation=load_data&amp;limit=0">Список заявок</a>				</p>
<?
if($accepted) {
    show_message('Заявка расчитана пользователем '. $calculation_row['calculation_items_accepted_user_login'] .'. <br />Дата и время расчета '. $Date->datetime_format($calculation_row['calculation_items_accepted_date']));
}
$calculator = & singleton('calculator');                     
$js_code = '';
function obj_to_str($val){
    return (string)$val;
}

    $text_item = '';
    
    $config_xml = $calculation_row['calculation_items_xml'];
    $xml = simplexml_load_string($config_xml);
    $nodes = $xml->xpath("//configuration");
	foreach ($nodes as $configuration_pos => $node) {
	   list( , $configuration) = each($node->xpath("./@id"));
       $configuration = (string)$configuration;
       
       $text_item .= '<p><strong><i style="font-size: 14px;color: #ff0000;">Конструкция №'. ($configuration_pos + 1) .'</i></strong><br />';
       
        ob_start();
        ?>
        var offset  = 0;
        configurationShema = document.getElementById("configurationShema_<?=($configuration_pos + 1)?>");
        ctx = configurationShema.getContext('2d');
        configurationShema.width = 650;
        configurationShema.height = door_h;  
        <?
        $js_code .= ob_get_clean();
            
       if($configuration == 'window') {
        
            $prm = array();
            $prm[] = 'окно';
            //Типы домов, серии, материалы - ШАГ 1
    	    list( , $build_type) = each($node->xpath("./build_type/title"));
            $prm[] = obj_to_str($build_type);
       
    	    list( , $build_sub_type) = each($node->xpath("./build_sub_type/title"));
            $build_sub_type = obj_to_str($build_sub_type);
            if(!empty($build_sub_type)) {
                $prm[] = 'постройка - '. $build_sub_type;
            }else {
                //$prm[] = 'постройка - не указана';
            }
            
            $text_item .= ''. join(', ', $prm) .';<br />';
            
                $prm = array();
            // Конфигурация
    	    list( , $door_position) = each($node->xpath("./frame_type/door_position/title"));
            $door_position = obj_to_str($door_position);
            if($door_position) {
        	    list( , $frame_type) = each($node->xpath("./frame_type/title"));
                $prm[] = obj_to_str($frame_type);
                $prm[] = 'дверь - '. $door_position;
    	        list( , $door_opening) = each($node->xpath("./frame_type/door_opening/title"));
                $door_opening = obj_to_str($door_opening);
                $prm[] = $door_opening;
    	        list( , $door_type) = each($node->xpath("./frame_type/door_type/title"));
                $door_type = obj_to_str($door_type);
                $prm[] = $door_type;
                
                $ctx_door_position = stristr($door_opening, 'Правое') === false ? 'left' : 'right';
                $ctx_door_opening = stristr($door_opening, 'откидное') === false ? 0 : 1;
                
    	        list( , $leaf_title) = each($node->xpath("./frame_type/leafs/leaf[1]/title"));
                $leaf_title = obj_to_str($leaf_title);
                $ctx_leaf_type_1 = 0;
                $leaf_title == 'Поворотно-откидное' && $ctx_leaf_type_1 = 1;
                $leaf_title == 'Поворотное' && $ctx_leaf_type_1 = 2;
                
                if($node->xpath("./frame_type/leafs/leaf[2]/title"))
                {
                    list( , $leaf_title) = each($node->xpath("./frame_type/leafs/leaf[2]/title"));
                    $leaf_title = obj_to_str($leaf_title);
                    $ctx_leaf_type_2 = 0;
                    $leaf_title == 'Поворотно-откидное' && $ctx_leaf_type_2 = 1;
                    $leaf_title == 'Поворотное' && $ctx_leaf_type_2 = 2;
                }
    	        
                
                ob_start();
                ?>
                    <?if($door_type == 'одно окно'):?>
                        <?if($door_position == 'Слева'):?>
                        offset = okDrawDoor('<?=$ctx_door_position?>', offset, <?=$ctx_door_opening?>);
                        offset = okDrawLeaf(<?=$ctx_leaf_type_1?>, offset , 0);
                        <?else:?>
                        offset = okDrawLeaf(<?=$ctx_leaf_type_1?>, offset , 0);
                        offset = okDrawDoor('<?=$ctx_door_position?>', offset, <?=$ctx_door_opening?>);
                        <?endif;?>
                    <?else:?>
                        <?if($door_position == 'Слева'):?>
                        offset = okDrawDoor('<?=$ctx_door_position?>', offset, <?=$ctx_door_opening?>);
                        offset = okDrawLeaf(<?=$ctx_leaf_type_1?>, offset , 0);
                        offset = okDrawLeaf(<?=$ctx_leaf_type_2?>, offset , 0);
                        <?elseif($door_position == 'Справа'):?>
                        offset = okDrawLeaf(<?=$ctx_leaf_type_1?>, offset , 0);
                        offset = okDrawLeaf(<?=$ctx_leaf_type_2?>, offset , 0);
                        offset = okDrawDoor('<?=$ctx_door_position?>', offset, <?=$ctx_door_opening?>);
                        <?else:?>
                        offset = okDrawLeaf(<?=$ctx_leaf_type_1?>, offset , 0);
                        offset = okDrawDoor('<?=$ctx_door_position?>', offset, <?=$ctx_door_opening?>);
                        offset = okDrawLeaf(<?=$ctx_leaf_type_2?>, offset , 0);
                        <?endif;?>
                    <?endif;?>
                <?
                $js_code .= ob_get_clean();
            
            }else {
                
        	    list( , $framuga_type) = each($node->xpath("./frame_type/framuga/title"));
                $framuga_type = obj_to_str($framuga_type);
                
                $is_framuga = $framuga_type == 'Нет' ? 0 : 1;
                
                ob_start();
                $leaf_prm = array();   
                $leafs = $node->xpath("./frame_type/leafs/leaf");
                foreach ($leafs as $i => $leaf) {
                    list( , $leaf_title) = each($leaf->xpath("./title"));
                    $leaf_title = obj_to_str($leaf_title);
                    $leaf_prm[] = $leaf_title;
                    
                    $ctx_leaf_type = 0;
                    $leaf_title == 'Поворотно-откидное' && $ctx_leaf_type = 1;
                    $leaf_title == 'Поворотное' && $ctx_leaf_type = 2;
                    ?>
                    offset = okDrawLeaf(<?=$ctx_leaf_type?>, offset , <?=$is_framuga?>);
                    <?
            	}       
                if($is_framuga)
                {
                    $ctx_framuga_type = stristr($framuga_type, 'глухое') === false ? 0 : 1;
                    ?>
                    okDrawFramuga(<?=count($leafs)?>, <?=$ctx_framuga_type?>);
                    <?
                }
                $js_code .= ob_get_clean();
                $leaf_frame = '';  
                if(count($leaf_prm)) {
                    $leaf_frame = ' ('. join(', ', $leaf_prm) .')';
                }
        	    list( , $frame_type) = each($node->xpath("./frame_type/title"));
                $prm[] = obj_to_str($frame_type) . $leaf_frame;
                
                if($is_framuga) {
                    $prm[] = 'с фрамугой, тип открывания - '. $framuga_type;
                }
                
                
            }
            $text_item .= '<strong>Конфигурация:</strong> '. join(', ', $prm) .';<br />';
            
            // Размеры
            $prm = array();
    	    list( , $size) = each($node->xpath("./size/title"));
            $prm[] = obj_to_str($size);
    	    list( , $oriel) = each($node->xpath("./oriel/title"));
            $prm[] = 'изгибы - '. obj_to_str($oriel);
            $text_item .= '<strong>Размеры:</strong> '. join(', ', $prm) .';<br />';
            
            // Решение
            $prm = array();
    	    list( , $base) = each($node->xpath("./base/title"));
            $base = obj_to_str($base);
            if(!empty($base)) {
                $prm[] = $base;
                    
                if($base != 'Эконом' && $base != 'Классик') {
            	    list( , $base_laminated) = each($node->xpath("./base/laminated/title"));
                    $base_laminated = obj_to_str($base_laminated);
                    if($base_laminated) {
                        $prm[] = 'ламинированные '. $base_laminated;
                    }
            	    list( , $base_color) = each($node->xpath("./base/color/title"));
                    $base_color = obj_to_str($base_color);
                    if($base_color) {
                        $prm[] = 'цвет - '. $base_color;
                    }
                }
                $text_item .= '<strong>Решение:</strong> '. join(', ', $prm) .';<br />';
            }else {
                $text_item .= '<strong>Решение:</strong> не указано;<br />';
            }
            
            // Подоконник
            $prm = array();
    	    list( , $sill) = each($node->xpath("./sill/title"));
            $sill = obj_to_str($sill);
            if($sill) {
                $prm[] = $sill;
        	    list( , $sill_size) = each($node->xpath("./sill/size"));
                $sill_size = obj_to_str($sill_size);
                if($sill_size) {
                    $prm[] = $sill_size;
                }
        	    list( , $sill_color) = each($node->xpath("./sill/color"));
                $sill_color = obj_to_str($sill_color);
                if($sill_color) {
                    $prm[] = $sill_color;
                }       
            }
            if(count($prm)) {
                $text_item .= '<strong>Подоконник:</strong> '. join(', ', $prm) .';<br />';
            }
            
            // Откосы
            $prm = array();
    	    list( , $slopes) = each($node->xpath("./slopes/title"));
            $slopes = obj_to_str($slopes);
            if($slopes) {
                $prm[] = $slopes;
        	    list( , $slopes_size) = each($node->xpath("./slopes/size"));
                $slopes_size = obj_to_str($slopes_size);
                if($slopes_size) {
                    $prm[] = $slopes_size;
                }
        	    list( , $slopes_color) = each($node->xpath("./slopes/color"));
                $slopes_color = obj_to_str($slopes_color);
                if($slopes_color) {
                    $prm[] = $slopes_color;
                }
            }   
            if(count($prm)) {
                $text_item .= '<strong>Откосы:</strong> '. join(', ', $prm) .';<br />';
            }
            
            // Отлив
            $prm = array();
    	    list( , $ebb) = each($node->xpath("./ebb/title"));
                $ebb = obj_to_str($ebb);
                if($ebb) {
                    $prm[] = $ebb;
                }
    	    list( , $ebb_size) = each($node->xpath("./ebb/size"));
                $ebb_size = obj_to_str($ebb_size);
                if($ebb_size) {
                    $prm[] = $ebb_size;
                }
    	    list( , $ebb_color) = each($node->xpath("./ebb/color"));
                $ebb_color = obj_to_str($ebb_color);
                if($ebb_color) {
                    $prm[] = $ebb_color;
                }             
            if(count($prm)) {
                $text_item .= '<strong>Отлив:</strong> '. join(', ', $prm) .';<br />';
            }     
            
            // Опции
            $prm = array();
            $options = $node->xpath("./options/option");
            foreach ($options as $i => $option) {
                list( , $option_title) = each($option->xpath("./title"));
                $prm[] = obj_to_str($option_title);
        	}    
            if(count($prm)) {
                $text_item .= '<strong>Опции:</strong> '. join(', ', $prm) .';<br />';
            }else {
                $text_item .= '<strong>Опции:</strong> нет;<br />';
            }
            
            
            // Доставка и монтаж
            $prm = array();
    	    list( , $delivery) = each($node->xpath("./delivery/title"));
            $delivery = obj_to_str($delivery);
            if($delivery) {
                $prm[] = $delivery;
        	    list( , $rise) = each($node->xpath("./rise/title"));
                $rise = obj_to_str($rise);
                if(!empty($rise)) {
                    $prm[] = $rise;
                }
            }else {
                $prm[] = 'самовывоз';
            }
        	list( , $assembly) = each($node->xpath("./assembly/title"));
            $assembly = obj_to_str($assembly);
            if(!empty($assembly)) {
                $prm[] = $assembly;
            }else {
                $prm[] = 'монтаж не требуется';
            }
            
            $text_item .= '<strong>Доставка и монтаж:</strong> '. join(', ', $prm) .';<br />';
            
       }else {
            $prm = array();
            $prm[] = 'балкон или лоджия';
    	    list( , $material) = each($node->xpath("./material/title"));
            $prm[] = obj_to_str($material);
            
            //Типы домов, серии, материалы - ШАГ 1
    	    list( , $build_type) = each($node->xpath("./build_type/title"));
            $prm[] = obj_to_str($build_type);
       
    	    list( , $build_sub_type) = each($node->xpath("./build_sub_type/title"));
            $build_sub_type = obj_to_str($build_sub_type);
            if(!empty($build_sub_type)) {
                $prm[] = 'постройка - '. $build_sub_type;
            }else {
                //$prm[] = 'постройка - не указана';
            }
            
            $text_item .= ''. join(', ', $prm) .';<br />';
            
            // Размеры и парапет
            $prm = array();
    	    list( , $size) = each($node->xpath("./size/title"));
            $prm[] = obj_to_str($size);
    	    list( , $oriel) = each($node->xpath("./oriel/title"));
            $prm[] = 'изгибы - '. obj_to_str($oriel);
    	    list( , $parapet) = each($node->xpath("./parapet/title"));
            $prm[] = 'материал парапета - '. obj_to_str($parapet);
                   
    	    list( , $about) = each($node->xpath("./about/title"));
            $about = obj_to_str($about);
            if($about) {
                $prm[] = $about;
            }
            $text_item .= '<strong>Размеры и тип парапета:</strong> '. join(', ', $prm) .';<br />';
            
            // Тип створки
            $prm = array();
                $leaf_prm = array();  
                
                ob_start();
                 
                $leafs = $node->xpath("./frame_type/leafs/leaf");
                foreach ($leafs as $i => $leaf) {
                    list( , $leaf_title) = each($leaf->xpath("./title"));
                    $leaf_prm[] = obj_to_str($leaf_title);
                    
                    $ctx_leaf_type = 0;
                    $leaf_title == 'Поворотно-откидное' && $ctx_leaf_type = 1;
                    $leaf_title == 'Поворотное' && $ctx_leaf_type = 2;
                    $leaf_title == 'Сдвигается' && $ctx_leaf_type = 3;
                    ?>
                    offset = okDrawLeaf(<?=$ctx_leaf_type?>, offset , 0);
                    <?
            	}       
                
                $js_code .= ob_get_clean();
                
                $leaf_frame = '';  
                if(count($leaf_prm)) {
                    $leaf_frame = ' ('. join(', ', $leaf_prm) .')';
                }
        	    list( , $frame_type) = each($node->xpath("./frame_type/title"));
                $prm[] = obj_to_str($frame_type) . $leaf_frame;
                   
            $text_item .= '<strong>Конфигурация:</strong> '. join(', ', $prm) .';<br />';
            
            // Решение
            $prm = array();
    	    list( , $base) = each($node->xpath("./base/title"));
            $base = obj_to_str($base);
            if(!empty($base)) {
                $prm[] = $base;
                    
                if($base != 'Эконом' && $base != 'Классик') {
            	    list( , $base_laminated) = each($node->xpath("./base/laminated/title"));
                    $base_laminated = obj_to_str($base_laminated);
                    if($base_laminated) {
                        $prm[] = 'ламинированные '. $base_laminated;
                    }
            	    list( , $base_color) = each($node->xpath("./base/color/title"));
                    $base_color = obj_to_str($base_color);
                    if($base_color) {
                        $prm[] = 'цвет - '. $base_color;
                    }
                }
                $text_item .= '<strong>Решение:</strong> '. join(', ', $prm) .';<br />';
            }else {
                $text_item .= '<strong>Решение:</strong> не указано;<br />';
            }
            // Подоконник
            $prm = array();
    	    list( , $sill) = each($node->xpath("./sill/title"));
            $sill = obj_to_str($sill);
            if($sill) {
                $prm[] = $sill;
        	    list( , $sill_size) = each($node->xpath("./sill/size"));
                $sill_size = obj_to_str($sill_size);
                if($sill_size) {
                    $prm[] = $sill_size;
                }
        	    list( , $sill_color) = each($node->xpath("./sill/color"));
                $sill_color = obj_to_str($sill_color);
                if($sill_color) {
                    $prm[] = $sill_color;
                }       
            }
            if(count($prm)) {
                $text_item .= '<strong>Подоконник:</strong> '. join(', ', $prm) .';<br />';
            }
            
            // Опции
            $prm = array();
            $options = $node->xpath("./options/option");
            foreach ($options as $i => $option) {
                list( , $option_title) = each($option->xpath("./title"));
                $prm[] = obj_to_str($option_title);
        	}    
            if(count($prm)) {
                $text_item .= '<strong>Опции:</strong> '. join(', ', $prm) .';<br />';
            }else {
                $text_item .= '<strong>Опции:</strong> нет;<br />';
            }
            
            // Дополнительно
            $prm = array();
            // Москитная сетка
    	    list( , $mosquito_net) = each($node->xpath("./mosquito_net/title"));
            $mosquito_net = obj_to_str($mosquito_net);
            if($mosquito_net) {
                $prm[] = $mosquito_net;
            }
            
                   
            if(count($prm)) {
                $text_item .= '<strong>Дополнительно:</strong> '. join(', ', $prm) .';<br />';
            }
            
            
            // Доставка и монтаж
            $prm = array();
    	    list( , $delivery) = each($node->xpath("./delivery/title"));
            $delivery = obj_to_str($delivery);
            if($delivery) {
                $prm[] = $delivery;
        	    list( , $rise) = each($node->xpath("./rise/title"));
                $rise = obj_to_str($rise);
                if(!empty($rise)) {
                    $prm[] = $rise;
                }
            }else {
                $prm[] = 'самовывоз';
            }
        	list( , $assembly) = each($node->xpath("./assembly/title"));
            $assembly = obj_to_str($assembly);
            if(!empty($assembly)) {
                $prm[] = $assembly;
            }else {
                $prm[] = 'монтаж не требуется';
            }
            $text_item .= '<strong>Доставка и монтаж:</strong> '. join(', ', $prm) .';<br />';
            
       }
       
       $price = $calculator->getConfigurationPrice($node) * calculator::RATE_USD;
       $text_item .= '<strong>Примерная стоимость:</strong> '.$price.' руб.;<br />';
       
        list( , $amount) = each($node->xpath("./@amount"));
        $amount = (string)$amount;
        if($amount)
        {
            $text_item .= '<strong>Количество:</strong> '.$amount.';<br />';
        }
        else
        {
            $text_item .= '<strong>Количество:</strong> 1;<br />';
        }
       
       
       
       $text_item .= '</p>';
       $text_item .= '<canvas id="configurationShema_'.($configuration_pos + 1).'"></canvas>';
       
    }




?>

<table style="width:650px;">
<tr>
    <td>
        <p>
        <strong>Номер заявки:</strong> <?=$calculation_items_id?><br />
        <strong>Дата поступления:</strong> <?=$Date->datetime_format($calculation_row['calculation_items_date'])?><br />
        </p>
        <p>
        <strong>Имя:</strong> <?=$calculation_row['calculation_items_user_name']?><br />
        <strong>Телефон:</strong> <?=$calculation_row['calculation_items_user_phone']?><br />
        <strong>E-mail:</strong> <?=(empty($calculation_row['calculation_items_user_email']) ? 'не указан' : '<a href="mailto:'.$calculation_row['calculation_items_user_email'].'">'.$calculation_row['calculation_items_user_email'].'</a>')?><br />
        </p>
       
        <p><strong>Состав заказа:</strong></p>

        <?=$text_item?>

        <div style="margin: 10px 0 20px 0;text-align: center;" class="no_print">
            <a href="#" onclick="window.print(); return false;"><img src="http://oknakomforta.com/img/print_order_button.png" alt="" border="0"/></a>
        </div>
        <div id="configurationShema"></div>
    </td>
</tr>
</table>
<script type="text/javascript">
$(document).ready(function(){
    <?=$js_code?>
});
</script>
