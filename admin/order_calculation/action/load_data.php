<?php 
/**
 * Система управления сайтом HostCMS v. 5.xx
 * Copyright © 2005-2008 ООО "Хостмэйк" (Hostmake LLC), http://www.hostcms.ru
 *
 * Ajax загрузчик списка отправленных форм.
 * 
 * Файл: /admin/Forms/action/load_form_fills.php
 * 
 * @author Hostmake LLC
 * @version 5.2
 */
function LoadAjaxData(&$admin_forms)
{
	$on_page = $admin_forms->GetOnPageCount();
	$page = to_int($_REQUEST['limit']);
	$begin = ($page == 0) ? 0 : ($page - 1) * $on_page;

	$param = array();

	$param['limit'][0] = array('begin' => $begin, 'count' => $on_page);

	// Выборка нужных записей.
	$param['data'][] = "SELECT calculation_items.*, users_table.users_name as calculation_items_accepted_user_login
        FROM `calculation_items` 
        LEFT JOIN users_table ON calculation_items.calculation_items_accepted_user = users_table.users_id
        WHERE 1 
        {FILTER_STRING} {ORDER_STRING} {LIMIT}";

	$count = "SELECT count(calculation_items_id) AS count FROM `calculation_items` WHERE 1 {FILTER_STRING}";

	// Строим хлебные крошки
	$new_path_array[] = array (
	'link' => $admin_forms->GetHtmlCallDoLoadAjax('/admin/order_calculation/order_calculation.php', '', 100000, 'load_data'),
	'onclick' => $admin_forms->GetOnClickCallDoLoadAjax('/admin/order_calculation/order_calculation.php', '', 100000, 'load_data'),
	'name' => 'Список заявок');

	$param['path_array'] = $new_path_array;

	$param['total_count'][] = $count;
	$param['current_page'] = $page;
	$param['on_page'] = $on_page;
	$param['title'] = 'Список заявок';

	
	$param['field_params'][0]['calculation_items_accepted_date']['admin_forms_field_type'] = 10;// вычисляемое поле
	$param['field_params'][0]['calculation_items_accepted_date']['callback_function'] = 'callback_function_calculation_items_accepted_date';
	
	// Функция обратного вызова для названия элемента
	function callback_function_calculation_items_accepted_date($field_value, $value, $row)
	{		
		if ($value != '0000-00-00 00:00:00')
		{
		      $Date = & singleton('DateClass');
		
			echo $Date->datetime_format($value);
		}
		else
		{
			echo '-';
		}		
	}

	$param['field_params'][0]['calculation_items_accepted_user_login']['admin_forms_field_type'] = 10;// вычисляемое поле
	$param['field_params'][0]['calculation_items_accepted_user_login']['callback_function'] = 'callback_function_calculation_items_accepted_user_login';
	
	// Функция обратного вызова для названия элемента
	function callback_function_calculation_items_accepted_user_login($field_value, $value, $row)
	{		
		if ($value)
		{
			?><strong><?php echo $value;?></strong>
			<?php 
		}
		else
		{
			echo '-';
		}		
	}

	$param['field_params'][0]['calculation_view']['admin_forms_field_type'] = 10;// вычисляемое поле
	$param['field_params'][0]['calculation_view']['callback_function'] = 'callback_function_calculation_view';
	
	// Функция обратного вызова для названия элемента
	function callback_function_calculation_view($field_value, $value, $row)
	{		
	   $id = $row['calculation_items_id'];
		if ($row['calculation_items_accepted_user'] > 0)
		{
			?>
            <a href="/admin/order_calculation/order_calculation.php?admin_forms_id=100000&operation=show_calculation&check_0_<?=$id?>=1" 
            onclick="TrigerSingleAction('/admin/order_calculation/order_calculation.php', '', 'show_calculation', 'check_0_<?=$id?>', 100000, '', '', '', ''); return false;">Посмотреть</a>
			<?php 
		}
		else
		{
			?>
            <strong><a href="/admin/order_calculation/order_calculation.php?admin_forms_id=100000&operation=show_calculation&check_0_<?=$id?>=1" 
            onclick="TrigerSingleAction('/admin/order_calculation/order_calculation.php', '', 'show_calculation', 'check_0_<?=$id?>', 100000, '', '', '', ''); return false;">Расчитать</a></strong>
            <?
		}		
	}
    
	$param['field_params'][0]['calculation_items_sum']['callback_function'] = 'callback_function_calculation_items_sum';	
	// Функция обратного вызова для названия элемента
	function callback_function_calculation_items_sum($field_value, $value, $row)
	{		
        $calculator = & singleton('calculator');
		$config_xml = $row['calculation_items_xml'];
        $xml = simplexml_load_string($config_xml);
        $nodes = $xml->xpath("//configuration");
        
        $price = 0;
        foreach ($nodes as $i => $node) 
        {
            $configuration_price = $calculator->getConfigurationPrice($node);
            
            list( , $amount) = each($node->xpath("./@amount"));
            $amount = (string)$amount;
            $amount = $amount ? $amount : 1;
            $price += $configuration_price * $amount;
        }
        echo $price * calculator::RATE_USD;
	}

	$param['field_params'][0]['calculation_items_count']['callback_function'] = 'callback_function_calculation_items_count';	
	// Функция обратного вызова для названия элемента
	function callback_function_calculation_items_count($field_value, $value, $row)
	{		
        $calculator = & singleton('calculator');
		$config_xml = $row['calculation_items_xml'];
        $xml = simplexml_load_string($config_xml);
        $nodes = $xml->xpath("//configuration");
        
        $count = 0;
        foreach ($nodes as $i => $node) 
        {
            list( , $amount) = each($node->xpath("./@amount"));
            $amount = (string)$amount;
            $count += $amount ? $amount : 1;
        }
        echo $count;
	}


	return $param;
}


/**
 * Обработчик события "Удалить" заполненной формы
 */
function delete(&$param)
{
	ob_start();
    $DataBase = & singleton('DataBase');
	if (defined('READ_ONLY') && READ_ONLY)
	{
		$kernel = & singleton('kernel');
						
		$kernel->ReadMode();
	}
	else
	{
		$forms = new Forms();

		$flag_success = true;

		// Счетчик удаленных заполненных форм
		$count_deleted_form_fills = 0;

		if (isset($param['data'][0]))
		{
			foreach ($param['data'][0] as $key => $value)
			{
			     $DataBase->Delete('calculation_items', "`calculation_items_id` = ". to_int($key));
                          			 
				++$count_deleted_form_fills;
			}
				// Удалена одна форма
				if ($count_deleted_form_fills == 1)
				{
					show_message('Заявка удалена');
				}
				// Удалена группа форм
				else
				{
					show_message('Заявки удалены');
				}
			
		}

		
	}
	
	$param['error'] = ob_get_clean();

}

/**
 * Обработчик события "Просмотр значений" заполненной формы
 */
function show_calculation(&$param)
{
	ob_start();


	if (isset($param['data']))
	{
		if (isset($param['data'][0]))
		{
			$DataBase = & singleton ('DataBase');
            $Date = & singleton('DateClass');
			
			foreach ($param['data'] as $dataset)
			{
				foreach ($dataset as $key => $value)
				{
					$calculation_items_id = intval($key);
					$query = "SELECT calculation_items.*, users_table.users_name as calculation_items_accepted_user_login
                        FROM `calculation_items` 
                        LEFT JOIN users_table ON calculation_items.calculation_items_accepted_user = users_table.users_id
                        WHERE calculation_items_id = ".$calculation_items_id;
                    $result = $DataBase->select($query);
                    if($result) {
                        $accepted = false;
                        $calculation_row = mysql_fetch_assoc($result);
                        if($calculation_row['calculation_items_accepted_user'] > 0) {
		                      $accepted = true;
                        }else {
                            $query = "UPDATE calculation_items 
                                SET calculation_items_accepted_date = NOW(), calculation_items_accepted_user = ".to_int($_SESSION['current_users_id'])."
                                WHERE calculation_items_id = $calculation_items_id";
					        $DataBase->query($query);                             
                        }
										
					   include(CMS_FOLDER . 'admin/order_calculation/action/show_calculation.php');

					   $param['title'] = 'Заявка № '. $calculation_items_id .' от '. $Date->datetime_format($calculation_row['calculation_items_date']);
					
                    }
					break;
				}
			}
		}
	}

	$param['result'] = ob_get_clean();
	return true;
}

?>