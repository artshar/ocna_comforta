<?php 
/**
 * Система управления сайтом HostCMS v. 5.xx
 * Copyright © 2005-2008 ООО "Хостмэйк" (Hostmake LLC), http://www.hostcms.ru
 *
 * Модуль order_calculation.
 * 
 * Файл: /admin/order_calculation/order_calculation.php
 * 
 * @author Hostmake LLC
 * @version 5.2
 */
// Подключаем основные классы.
require_once('../../main_classes.php');

$admin = new Admin();

// Проверка авторизации пользователя.
$admin->admin_session_valid('order_calculation');

$kernel = & singleton('kernel');

// Инсталляция всех модулей ядра.
$kernel->LoadModules(!isset($_REQUEST['JsHttpRequest']));

// Инициализация CURRENT_SITE и констант.
$admin->admin_init();

$forms = & singleton('Forms');

if (!isset($_REQUEST['JsHttpRequest']) && !isset($_REQUEST['print_form_fill_id']))
{
	do_html_header_admin('Список заявок');
}

// Распечатка заполненной формы
if (to_int($_REQUEST['print_form_fill_id']))
{
	include(CMS_FOLDER . '/admin/order_calculation/action/form_fill_blank.php');
}

$admin_forms = new admin_forms();

// Добавление/редактирование информации о форме
if (isset($_REQUEST['add_form']) || isset($_REQUEST['save_form']))
{
	ob_start();

	if (defined('READ_ONLY') && READ_ONLY)
	{
		$kernel->ReadMode();
	}
	else
	{
		// Добавляем информацию в БД.
		include(CMS_FOLDER . 'admin/order_calculation/action/add_form.php');
	}

	$message = ob_get_clean();

	$JsHttpRequest = new JsHttpRequest(SITE_CODING);

	// При добавлении (не сохранении) - отправляем команду на загрузку
	if (isset($_REQUEST['add_form']))
	{
		// Отображаем форму
		include(CMS_FOLDER . 'admin/order_calculation/action/load_forms.php');
		$GLOBALS['_RESULT'] = $admin_forms->ShowForm(100000, 'load_data');
		// Добавляем определенное выше сообщение
		$GLOBALS['_RESULT']['error'] = $message;
	}
	else
	{
		// Отправляем только сообщение о произведенных действиях
		$GLOBALS['_RESULT'] = array(
		'error' => $message,
		'form_html' => '',
		'title' => '');
	}

	echo $JsHttpRequest->LOADER;
	exit();
}



// Отображение форм
switch (to_int($_REQUEST['admin_forms_id']))
{
	case 100000:
	default:
		{
			// Ajax загрузчик для списка загруженных форм.
			include(CMS_FOLDER.'admin/order_calculation/action/load_data.php');
			$admin_forms->ProcessAjax(100000);
			break;
		}
}

if (!isset($_REQUEST['JsHttpRequest']))
{
	do_html_footer_admin();
}

?>