<?
// Подключаем основные классы
/*
require_once('main_classes.php');
$kernel = & singleton('kernel');

// Загрузка модулей
$kernel->LoadModules();
*/

$_SESSION['last_calculation_items_id'] = 0;
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>ON-Line калькулятор - Окна Комфорта</title>
    <meta name="description" content=""/>
    <meta name="keywords" content=""/>
    
    <meta http-equiv="Content-Language" content="ru"/>
    <meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
    
	<link rel="stylesheet" type="text/css" href="/css/order.css" />
    
	<script type="text/javascript" src="/js/swfobject.js"></script>
    
    <!-- Google Analytics -->
    <script type="text/javascript">
        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-17683530-3']);
        _gaq.push(['_trackPageview']);
        _gaq.push(['_setDomainName', 'oknakomforta.com']);
        (function() {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();
    </script>
    <!-- /Google Analytics -->


</head>
<body>
<!-- Yandex.Metrika counter -->
<script src="//mc.yandex.ru/metrika/watch.js" type="text/javascript"></script>
<div style="display:none;"><script type="text/javascript">
try { var yaCounter1390649 = new Ya.Metrika({id:1390649,
          clickmap:true,
          trackLinks:true});}
catch(e) { }
</script></div>
<noscript><div><img src="//mc.yandex.ru/watch/1390649" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
	<div id="flash-box">
			<div id="altContent">
				<p>
					<a href="http://www.adobe.com/go/getflashplayer">
						<img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" width="150" alt="Get Adobe Flash player" />
					</a>
				</p>
			</div>
		</div>
	<script type="text/javascript">
    	var so = new SWFObject("/swf/WinConfig.swf", "Configurator", "1060", "663", "10.2.0", "");
	    so.addParam("wmode", "transparent");
    	so.addParam("menu", "false");
    	so.addParam("scale", "noScale");
    	so.addParam("allowFullscreen", "true");
    	so.addParam("allowScriptAccess", "always");
    	so.addParam("bgcolor", "#FFFFFF");
    	so.write("flash-box");
	</script>

<!-- Google Code for &#1042;&#1089;&#1077; &#1087;&#1086;&#1089;&#1077;&#1090;&#1080;&#1090;&#1077;&#1083;&#1080; Remarketing List -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 1013687388;
var google_conversion_language = "en";
var google_conversion_format = "3";
var google_conversion_color = "ffffff";
var google_conversion_label = "Z7SUCPTRtQMQ3Miu4wM";
var google_conversion_value = 0;
/* ]]> */
</script>
<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="http://www.googleadservices.com/pagead/conversion/1013687388/?value=0&amp;label=Z7SUCPTRtQMQ3Miu4wM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>

<div style="position: absolute; visibility: hidden;">
<!--  AdRiver code START. Type:counter(zeropixel) Site: oknakomf PZ: 0 BN: 0 -->
<script language="javascript" type="text/javascript"><!--
var RndNum4NoCash = Math.round(Math.random() * 1000000000);
var ar_Tail='unknown'; if (document.referrer) ar_Tail = escape(document.referrer);
document.write('<img src="http://ad.adriver.ru/cgi-bin/rle.cgi?' + 'sid=185886&bt=21&pz=0&rnd=' + RndNum4NoCash + '&tail256=' + ar_Tail + '" border=0 width=1 height=1>')
//--></script>
<noscript><img src="http://ad.adriver.ru/cgi-bin/rle.cgi?sid=185886&bt=21&pz=0&rnd=1165410460" border=0 width=1 height=1></noscript>
<!--  AdRiver code END  -->
</div>
</body>
</html>