<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

//delayed function must return a string
if(empty($arResult))
	return "";

$strReturn = '          <div class="breadcrumbs">';

for($index = 0, $itemSize = count($arResult); $index < $itemSize; $index++)
{
	if($index > 0 && $title <> '')
		$strReturn .= ' \\ ';

	$title = htmlspecialcharsex($arResult[$index]["TITLE"]);
	if($itemSize == $index+1)
		$strReturn .= '<a href="'.$arResult[$index]["LINK"].'" title="'.$title.'" class="active">'.$title.'</a>';
	else
		$strReturn .= '<a href="'.$arResult[$index]["LINK"].'" title="'.$title.'">'.$title.'</a>';
}

$strReturn .= '</div>';
return $strReturn;
?>
