<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

?>
<!--<pre>
    <? /*var_dump($arResult);*/ ?>
</pre>-->
<div class="rslides_container">
    <ul class="rslides">
        <?
        if (is_array($arResult['ITEMS']) && count($arResult['ITEMS']) > 0):?>

            <? foreach ($arResult['ITEMS'] as $arItem): ?>
                <li>
                    <a src="<?= $arItem['PROPERTIES']['URL']['VALUE'] ?>" onclick="window.location='<?=$arItem['PROPERTIES']['URL']['VALUE'] ?>'">
                        <img src="<?= CFile::GetPath($arItem['PREVIEW_PICTURE']['SRC']) ?>"
                             alt="<?= $arItem['NAME'] ?>">
                    </a>
                </li>
            <? endforeach;?>

        <? endif; ?>
    </ul>
</div>