<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
        <h1 class="index_h1">Новости</h1>
        <div class="index_news">
<?foreach($arResult["ITEMS"] as $arItem):?>
<?
    $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
    $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
?>
          <div class="newsblock" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
            <div class="newsblock_left">
              <a href="<?echo $arItem["DETAIL_PAGE_URL"]?>"><img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" alt="<?echo $arItem["NAME"]?>" title="<?echo $arItem["NAME"]?>" /></a><br />
<?    if($arParams["DISPLAY_DATE"]!="N" && $arItem["DISPLAY_ACTIVE_FROM"]):?>
              <div class="newsblockdate"><?echo $arItem["DISPLAY_ACTIVE_FROM"]?></div>
<?    endif?>
            </div>
            <div class="newsblock_right">
              <div><a href="<?echo $arItem["DETAIL_PAGE_URL"]?>"><?echo $arItem["NAME"]?></a></div>
              <p><?=$arItem["PREVIEW_TEXT"]?></p>
            </div>
          </div>
<?endforeach;?>
          <div class="all_news"><a href="/news/">Все новости</a></div>
        </div>
