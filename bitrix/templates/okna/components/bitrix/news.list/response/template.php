<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
        <div class="otz">
          <div class="otz_top"></div>
          <div class="otz_center">
            <div class="otzc_q">
              <div class="otz_c1">Отзывы</div>
              <div class="otz_c2"><a href="/response/">Все отзывы</a></div>
            </div>
<?foreach($arResult["ITEMS"] as $arItem):?>
<?
    $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
    $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
?>
            <div class="otz_body" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
<?    if($arParams["DISPLAY_DATE"]!="N" && $arItem["DISPLAY_ACTIVE_FROM"]):?>
              <div class="otz_date"><?echo $arItem["DISPLAY_ACTIVE_FROM"]?></div>
<?    endif?>
              <div class="otz_name"><?=$arItem["DISPLAY_PROPERTIES"]["NAME"]["VALUE"]?></div>
              <div class="otz_text"><?=$arItem["PREVIEW_TEXT"]?></div>
            </div>
<?endforeach;?>

<div class="otz_link"><a href="/response/add/">Оставить отзыв</a></div>

          </div>

          <div class="otz_bottom"></div>

        </div>
