<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div id="dump" style="display:none">
<?
print_r($arResult);
?>
</div>
<?if (!empty($arResult)):?>
      <div class="otz">
        <div class="otz_top"></div>
        <div class="otz_center">
          <ul class="sidemenu">
<?$previousLevel = 0; foreach($arResult as $arItem):?>
<?if($arItem["DEPTH_LEVEL"]>1){?><?$space=str_repeat("  ",$arItem["DEPTH_LEVEL"]);?><?}?>
<?    if($previousLevel && $arItem["DEPTH_LEVEL"] < $previousLevel):?>
<?=str_repeat("</ul></li>", ($previousLevel - $arItem["DEPTH_LEVEL"]));?>
<?    endif?>
<?if ($arItem["IS_PARENT"]):?>
            <li<?if($arItem["CHILD_SELECTED"] !== true && $arItem["SELECTED"] !== true):?> class="close"<?else:?> class="selected active_x"<?endif?>>
              <div class="folder" onClick="OpenMenuNode(this)"></div>
              <div class="item-text"><a href="<?=$arItem["LINK"]?>"<?if($arItem["CHILD_SELECTED"] !== true && $arItem["SELECTED"] !== true):?><?else:?> class="selected active_xa"<?endif?>><?=$arItem["TEXT"]?></a></div>
              <ul>
<?else:?>
            <li<?if($arItem["SELECTED"]):?> class="selected active"<?endif;?>>
              <div class="page"></div>
              <div class="item-text"><a href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a></div>
            </li>
<?endif?>
<?$previousLevel = $arItem["DEPTH_LEVEL"];?>
<?endforeach?>
<?if ($previousLevel > 1):?>
<?=str_repeat("</ul></li>", ($previousLevel-1) );?>
<?endif?>
          </ul>
        </div>
        <div class="otz_bottom2"></div>
      </div>
<?endif?>