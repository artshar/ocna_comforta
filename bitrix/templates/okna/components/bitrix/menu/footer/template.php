<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if (!empty($arResult)):?>
    <div class="block">
      <div class="ftmenu">
        <div class="ftmenu_buttons">
<?
$count = count($arResult)-1;
foreach($arResult as $id => $arItem):
    if($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1)
        continue;
?>
<?    if($arItem["SELECTED"]):?>
          <a href="<?=$arItem["LINK"]?>" class="selected<?if($id == $count){?> nobrdr<?}?>"><?=$arItem["TEXT"]?></a>
<?    else:?>
          <a href="<?=$arItem["LINK"]?>"<?if($id == $count){?> class="nobrdr"<?}?>><?=$arItem["TEXT"]?></a>
<?    endif?>
<?endforeach?>
        </div>
      </div>
    </div>
<?endif?>
