<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if (!empty($arResult)):?>
    <div class="mmenu">
      <div class="block3">
        <ul class="mmenu_buttons">
<?
$i=1; $num = count($arResult);
foreach($arResult as $id => $arItem):
    if($arParams["MAX_LEVEL"] == 2 && $arItem["DEPTH_LEVEL"] > 2)
        continue;
?>
<?    if($arItem["DEPTH_LEVEL"] == 1):?>
<?        if($parentOpen):?><?$parentOpen = 0;?>
            </ul>
          </li>
<?        endif;?>
<?        if($arItem["SELECTED"]):?>
<?            if(!$arItem["IS_PARENT"]):?>
          <li<?if($i>=$num){?> class="nobrdr"<?}?>><a href="<?=$arItem["LINK"]?>" class="selected<?if($i>=$num){?> nobrdr<?}?>"><?=$arItem["TEXT"]?></a></li>
<?            else:?><?$parentOpen = 1;?>
          <li<?if($i>=$num){?> class="nobrdr"<?}?>><a href="<?=$arItem["LINK"]?>" class="selected<?if($i>=$num){?> nobrdr<?}?>"><?=$arItem["TEXT"]?></a>
            <ul class="dropdown">
<?            endif;?>
<?        else:?>
<?            if(!$arItem["IS_PARENT"]):?>
          <li<?if($i>=$num){?> class="nobrdr"<?}?>><a href="<?=$arItem["LINK"]?>"<?if($i>=$num){?> class="nobrdr"<?}?>><?=$arItem["TEXT"]?></a></li>
<?            else:?><?$parentOpen = 1;?>
          <li<?if($i>=$num){?> class="nobrdr"<?}?>><a href="<?=$arItem["LINK"]?>"<?if($i>=$num){?> class="nobrdr"<?}?>><?=$arItem["TEXT"]?></a>
            <ul class="dropdown">
<?            endif;?>
<?        endif?>
<?    endif?>
<?    if($arItem["DEPTH_LEVEL"] == 2):?>
<?        if($arItem["SELECTED"]):?>
              <li><a href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a></li>
<?        else:?>
              <li><a href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a></li>
<?        endif?>
<?    endif?>
<?$i++;endforeach;?>
<?if($parentOpen):?>
            </ul>
          </li>
<?endif?>
        </ul>
      </div>
    </div>
<?endif?>
