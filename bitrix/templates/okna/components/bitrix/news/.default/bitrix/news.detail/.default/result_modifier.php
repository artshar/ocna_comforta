<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
foreach ($arResult['ITEMS'] as $key => $arElement)
{
    if (is_array($arElement['DETAIL_PICTURE']))
    {
		$arFilter = '';
		if($arParams["SHARPEN"] != 0)
		{
			$arFilter = array(array("name" => "sharpen", "precision" => $arParams["SHARPEN"]));
		}
		$arFileTmp = CFile::ResizeImageGet(
			$arElement['DETAIL_PICTURE'],
			array("width" => 300, "height" => 300),
			array(),
			BX_RESIZE_IMAGE_PROPORTIONAL,
			true, $arFilter
		);

		$arFile['WIDTH'] = $arFileTmp["width"];
		$arFile['HEIGHT'] = $arFileTmp["height"];
		$arFile['SRC'] = $arFileTmp['src'];
		$arResult['ITEMS'][$key]['DETAIL_PICTURE'] = $arFile;
	}
}
?>