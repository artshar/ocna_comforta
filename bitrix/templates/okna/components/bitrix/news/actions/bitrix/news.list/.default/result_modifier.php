<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
foreach ($arResult['ITEMS'] as $key => $arElement)
{
    if (is_array($arElement['PREVIEW_PICTURE']))
    {
		$arFilter = '';
		if($arParams["SHARPEN"] != 0)
		{
			$arFilter = array(array("name" => "sharpen", "precision" => $arParams["SHARPEN"]));
		}
		$arFileTmp = CFile::ResizeImageGet(
			$arElement['PREVIEW_PICTURE'],
			array("width" => 100, "height" => 100),
			array(),
			BX_RESIZE_IMAGE_PROPORTIONAL,
			true, $arFilter
		);

		$arFile['WIDTH'] = $arFileTmp["width"];
		$arFile['HEIGHT'] = $arFileTmp["height"];
		$arFile['SRC'] = $arFileTmp['src'];
		$arResult['ITEMS'][$key]['PREVIEW_PICTURE'] = $arFile;
	}
}
?>