<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<script>
$(function(){
	$('.fancybox-gallery').fancybox({
        padding: 100,
        width: 492,
        height: 647,
		scrolling:'no',
		overlayOpacity: 0.5,
        helpers	: {
            thumbs	: {
                width	: 100,
                height	: 100
            }
        }
	}); 
});
</script>

<div class="gallery-list">
	<?
	$i=0;
	foreach($arResult['SECTIONS'] as $arSection):
		$i++;
		?>
		<div class="section<?if($i%3==0):?> last<?endif;?>">
			<a href="<?php echo $arSection['FIRSTPIC']['SRC']?$arSection['FIRSTPIC']['SRC']:$arSection['PICTURE']['SRC'];?>" class="fancybox-gallery" rel="gallery-<?=$arSection['ID']?>">
				<span class="pic"><img src="<?=$arSection['PICTURE']['SRC']?>" /></span>
				<span class="name"><?=$arSection['NAME']?> <span class="cnt">(<?=$arSection['ELEMENT_CNT']?>)</span></span>
			</a>
			<div class="section-items">
				<?foreach($arSection['ITEMS'] as $arPic):?>
					<a href="<?=$arPic['SRC']?>" rel="gallery-<?=$arSection['ID']?>" class="fancybox-gallery"></a>
				<?endforeach;?>
			</div>
		</div>
		<?if($i%3==0):?><div class="clear"></div><?endif;?>
	<?endforeach;?>
	<div class="clear"></div>
</div>
<?// print_r($arResult); ?>