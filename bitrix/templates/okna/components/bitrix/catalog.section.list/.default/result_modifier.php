<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?foreach($arResult["SECTIONS"] as $id=>$section):
	$arSelect=Array("DETAIL_PICTURE");
	$arFilter=Array("IBLOCK_ID"=>3,"ACTIVE"=>"Y","SECTION_ID"=>$section['ID']);
	$dbRes=CIBlockElement::GetList(Array("SORT"=>"ASC"),$arFilter,false,false,$arSelect);
	$i=0;
	while($arRes=$dbRes->GetNext()):
		$i++;
		$arPic=CFile::ResizeImageGet($arRes['DETAIL_PICTURE'],Array("width"=>1000,"height"=>1000),BX_RESIZE_IMAGE_PROPORTIONAL,false);
		if($i==1):
			$arResult["SECTIONS"][$id]['FIRSTPIC']['SRC']=$arPic['src'];
		else:
			$arResult["SECTIONS"][$id]['ITEMS'][]=Array("SRC"=>$arPic['src']);
		endif;
	endwhile;
endforeach;?>