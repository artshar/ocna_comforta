<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
IncludeTemplateLangFile(__FILE__);
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title><? $APPLICATION->ShowTitle() ?></title>
    <? $APPLICATION->ShowHead(); ?>
    <? $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/jquery.1.8.3.min.js'); ?>
    <script type="text/javascript" src="<?= SITE_TEMPLATE_PATH ?>/js/script.js"></script>
    <script type="text/javascript" src="/bitrix/js/fancybox/jquery.fancybox.pack.js"></script>
    <script type="text/javascript" src="/bitrix/js/fancybox/helpers/jquery.fancybox-thumbs.js"></script>
    <link rel="stylesheet" type="text/css" href="/bitrix/js/fancybox/jquery.fancybox.css" media="screen"/>
    <link rel="stylesheet" type="text/css" href="/bitrix/js/fancybox/helpers/jquery.fancybox-thumbs.css"
          media="screen"/>
    <script type="text/javascript">
        if (!window.jQuery) {
            document.write(unescape('<script type="text/javascript" src="http://mod.calltouch.ru/js/jquery-1.5.1.min.js">%3C/script%3E'));
        }

        var gaJsHost = (("https:" == document.location.protocol) ?
            "https://" : "http://");

        document.write(unescape("%3Cscript src='" +
        gaJsHost + "mod.calltouch.ru/d_client.js?param;ref")
        + escape(document.referrer) + ";url" + escape(document.URL)
        + ";cook" + escape(document.cookie) + unescape("'type='text/javascript'%3E%3C/script%3E"));
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('.fancybox').fancybox({
                nextEffect: 'fade',
                prevEffect: 'fade'
            });

            $(".gallery_photos .item").equalHeights();

        });
    </script>


    <script type="text/javascript">
        jQuery.fn.equalHeights = function () {
            var currentTallest = 0;
            jQuery(this).each(function () {
                if (jQuery(this).height() > currentTallest) {
                    currentTallest = jQuery(this).height();
                }
            });
            jQuery(this).css({'min-height': currentTallest});
            return this;
        };
    </script>

    <script src="<?= SITE_TEMPLATE_PATH ?>/js/jquery.maskedinput-1.3.min.js"></script>
    <? if ($APPLICATION->GetCurDir() == SITE_DIR): ?>
        <script src="<?= SITE_TEMPLATE_PATH ?>/js/jquery.slinkySlider.js"></script>
        <script type="text/javascript">
            $(document).ready(function () {
                $("#content").slinkySlider({
                    numberofpanels: 4, // number of panels to load
                    smallsize: 20, // size in pixels of collapsed panel
                    transition: 1000, // time in milliseconds for transition
                    doauto: true, // automatic timed animation? true/false
                    autotimer: 6000, // time between automatic animations
                    panelspacing: 1, // gap between collapsed panels
                    panelname: "panel" // sets the HTML filename (means filename will be "panel[x].html")
                });
            });
        </script>
        <style>
            #content {
                margin: 0 auto;
                width: 960px;
                height: 300px;
            }

            .panelwrappers {
                overflow: hidden;
            }
        </style>
        <script src="<?= SITE_TEMPLATE_PATH ?>/js/jquery.flexslider-min.js"></script>
        <script src="<?= SITE_TEMPLATE_PATH ?>/js/kwiks.js"></script>
        <script src="<?= SITE_TEMPLATE_PATH ?>/js/sliderflex.js"></script>
        <link rel="stylesheet" href="<?= SITE_TEMPLATE_PATH ?>/css/flexslider.css" type="text/css" media="screen"/>
    <? endif; ?>
    <link rel="stylesheet" href="/css/style.css" type="text/css"/>
    <link type="image/x-icon" href="/favicon.ico" rel="icon">
    <link type="image/x-icon" href="/favicon.ico" rel="shortcut icon">
    <script type="text/javascript">

        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-38617607-1']);
        _gaq.push(['_setCustomVar', 1, 'call_value', call_value, 2]);
        _gaq.push(['_trackPageview']);

        (function () {
            var ga = document.createElement('script');
            ga.type = 'text/javascript';
            ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0];
            s.parentNode.insertBefore(ga, s);
        })();

    </script>
    <!-- Yandex.Metrika counter -->

    <script type="text/javascript">

        (function (d, w, c) {

            (w[c] = w[c] || []).push(function () {

                try {
                    w.yaCounter20154625 = new Ya.Metrika({
                        id: 20154625,

                        clickmap: true,

                        trackLinks: true,

                        accurateTrackBounce: true
                    });

                } catch (e) {
                }

            });


            var n = d.getElementsByTagName("script")[0],

                s = d.createElement("script"),

                f = function () {
                    n.parentNode.insertBefore(s, n);
                };

            s.type = "text/javascript";

            s.async = true;

            s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";


            if (w.opera == "[object Opera]") {

                d.addEventListener("DOMContentLoaded", f, false);

            } else {
                f();
            }

        })(document, window, "yandex_metrika_callbacks");

    </script>

    <noscript>
        <div><img src="//mc.yandex.ru/watch/20154625" style="position:absolute; left:-9999px;" alt=""/></div>
    </noscript>

    <!-- /Yandex.Metrika counter -->
</head>
<body>


<? global $USER;
if ($USER->IsAdmin()): ?>
    <div id="panel"><? $APPLICATION->ShowPanel(); ?></div>
<? endif; ?>
<div class="strmain_top">
    <div class="block">
        <? if ($APPLICATION->GetCurUri() == SITE_DIR && ERROR_404 != "Y"): ?>
            <div class="logo"><img src="/images/logo.png"/></div>
        <? else: ?>
            <div class="logo"><a href="<?= SITE_DIR ?>"><img src="/images/logo.png"/></a></div>
        <? endif; ?>
        <div class="header_contacts">
            <div class="hc_phone" id="call_phone_1">+7 (495) 545-44-70</div>
            <div
                class="hc_time"><? $APPLICATION->IncludeFile($APPLICATION->GetTemplatePath("include_areas/header_time.php"), Array(), Array("MODE" => "text")) ?></div>
        </div>
        <div class="header_buttons">
            <div>
                <img src="/images/headerbut_01_top.png"/>
                <a href="#" id="orderCall" class="hbut1"></a>
            </div>
            <div>
                <img src="/images/headerbut_02_top.jpg"/>
                <a href="#" id="orderManager" class="hbut2"></a>
            </div>
            <div>
                <img src="/images/headerbut_03_top.jpg"/>
                <a href="/service/calculate/" class="hbut3"></a>
            </div>
        </div>
    </div>
    <? $APPLICATION->IncludeComponent("bitrix:menu", "top", array(
        "ROOT_MENU_TYPE" => "top",
        "MENU_CACHE_TYPE" => "A",
        "MENU_CACHE_TIME" => "3600",
        "MENU_CACHE_USE_GROUPS" => "Y",
        "MENU_CACHE_GET_VARS" => array(),
        "MAX_LEVEL" => "2",
        "CHILD_MENU_TYPE" => "left",
        "USE_EXT" => "N",
        "DELAY" => "N",
        "ALLOW_MULTI_SELECT" => "N"
    ),
        false
    ); ?>
</div>
<div class="strmain_center">
    <? if ($APPLICATION->GetCurDir() == SITE_DIR): ?>
        <div class="block">

                <? $APPLICATION->IncludeComponent("bitrix:news.list", "okna-main-slider", Array(
                        "DISPLAY_NAME" => "Y",
                        "DISPLAY_PICTURE" => "Y",
                        "DISPLAY_PREVIEW_TEXT" => "Y",
                        "AJAX_MODE" => "N",
                        "IBLOCK_TYPE" => "content",
                        "IBLOCK_ID" => "6",
                        "NEWS_COUNT" => "4",
                        "SORT_BY1" => "ACTIVE_FROM",
                        "SORT_ORDER1" => "DESC",
                        "SORT_BY2" => "SORT",
                        "SORT_ORDER2" => "ASC",
                        "CACHE_TYPE" => "A",
                        "CACHE_TIME" => "3600000",
                        "CACHE_FILTER" => "Y",
                        "CACHE_GROUPS" => "N",
                        "PROPERTY_CODE" => Array("URL")
                    )
                ); ?>
        </div>
        <? //$APPLICATION->IncludeComponent("sfera:slider", ".default", array());?>
        <div class="block">
            <div class="winds">
                <div class="win">
                    <div
                        class="winimg"><? $APPLICATION->IncludeFile($APPLICATION->GetTemplatePath("include_areas/index_img_1.php"), Array(), Array("MODE" => "text")) ?></div>
                    <div
                        class="wintext"><? $APPLICATION->IncludeFile($APPLICATION->GetTemplatePath("include_areas/index_text_1.php"), Array(), Array("MODE" => "text")) ?></div>
                </div>
                <div class="win">
                    <div
                        class="winimg"><? $APPLICATION->IncludeFile($APPLICATION->GetTemplatePath("include_areas/index_img_2.php"), Array(), Array("MODE" => "text")) ?></div>
                    <div
                        class="wintext"><? $APPLICATION->IncludeFile($APPLICATION->GetTemplatePath("include_areas/index_text_2.php"), Array(), Array("MODE" => "text")) ?></div>
                </div>
                <div class="win">
                    <div
                        class="winimg"><? $APPLICATION->IncludeFile($APPLICATION->GetTemplatePath("include_areas/index_img_3.php"), Array(), Array("MODE" => "text")) ?></div>
                    <div
                        class="wintext"><? $APPLICATION->IncludeFile($APPLICATION->GetTemplatePath("include_areas/index_text_3.php"), Array(), Array("MODE" => "text")) ?></div>
                </div>
                <div class="win">
                    <div
                        class="winimg"><? $APPLICATION->IncludeFile($APPLICATION->GetTemplatePath("include_areas/index_img_4.php"), Array(), Array("MODE" => "text")) ?></div>
                    <div
                        class="wintext"><? $APPLICATION->IncludeFile($APPLICATION->GetTemplatePath("include_areas/index_text_4.php"), Array(), Array("MODE" => "text")) ?></div>
                </div>
            </div>
        </div>

    <? endif; ?>
    <div class="block">
        <div class="leftside_index">
            <? if ($APPLICATION->GetCurDir() != SITE_DIR): ?>
                <? $APPLICATION->IncludeComponent("bitrix:menu", "left", array(
                    "ROOT_MENU_TYPE" => "left",
                    "MENU_CACHE_TYPE" => "A",
                    "MENU_CACHE_TIME" => "3600",
                    "MENU_CACHE_USE_GROUPS" => "Y",
                    "MENU_CACHE_GET_VARS" => "",
                    "MAX_LEVEL" => "5",
                    "CHILD_MENU_TYPE" => "left",
                    "USE_EXT" => "Y",
                    "DELAY" => "N",
                    "ALLOW_MULTI_SELECT" => "N"
                ),
                    false,
                    array(
                        "ACTIVE_COMPONENT" => "Y"
                    )
                ); ?>
            <? endif; ?>
            <div style="margin-bottom:8px"><a href="/service/coupon/"><img src="/images/bng_01.jpg"
                                                                           alt="Купон на скидку"
                                                                           title="Купон на скидку"/></a></div>
            <!--<div style="margin-bottom:8px"><a href="/"><img src="/images/bng_02.png" alt="Помощник в выборе окон" title="Помощник в выборе окон" /></a></div>-->
            <div style="margin-bottom:8px"><a href="/response/"><img src="/images/bng_03.png" alt="Отзывы"
                                                                     title="Отзывы"/></a></div>
            <!-- <div><a href="/help/"><img src="/images/bng_02.jpg" alt="" title="" /></a></div> -->
            <? if ($APPLICATION->GetCurDir() == SITE_DIR): ?>
                <div id="side_callback">
                    <input class="invis_field field_fio" type="text" name="fio" maxlength="250" value=""/>
                    <input class="invis_field field_phone" type="text" name="telephone" maxlength="250"
                           class="textInput"/>
                    <button type="button" class="side_callback_submit"></button>
                </div>
            <? endif; ?>
            <? if ($APPLICATION->GetCurDir() != SITE_DIR): ?>
                <? $APPLICATION->IncludeComponent("bitrix:news.list", "response", array(
                    "IBLOCK_TYPE" => "content",
                    "IBLOCK_ID" => "5",
                    "NEWS_COUNT" => "1",
                    "SORT_BY1" => "ACTIVE_FROM",
                    "SORT_ORDER1" => "DESC",
                    "SORT_BY2" => "SORT",
                    "SORT_ORDER2" => "ASC",
                    "FILTER_NAME" => "",
                    "FIELD_CODE" => array(
                        0 => "",
                        1 => "",
                    ),
                    "PROPERTY_CODE" => array(
                        0 => "NAME",
                        1 => "",
                    ),
                    "CHECK_DATES" => "Y",
                    "DETAIL_URL" => "",
                    "AJAX_MODE" => "N",
                    "AJAX_OPTION_JUMP" => "N",
                    "AJAX_OPTION_STYLE" => "Y",
                    "AJAX_OPTION_HISTORY" => "N",
                    "CACHE_TYPE" => "A",
                    "CACHE_TIME" => "36000000",
                    "CACHE_FILTER" => "N",
                    "CACHE_GROUPS" => "Y",
                    "PREVIEW_TRUNCATE_LEN" => "",
                    "ACTIVE_DATE_FORMAT" => "d/m/Y",
                    "SET_TITLE" => "N",
                    "SET_STATUS_404" => "N",
                    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                    "ADD_SECTIONS_CHAIN" => "N",
                    "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                    "PARENT_SECTION" => "",
                    "PARENT_SECTION_CODE" => "",
                    "DISPLAY_TOP_PAGER" => "N",
                    "DISPLAY_BOTTOM_PAGER" => "N",
                    "PAGER_TITLE" => "Отзывы",
                    "PAGER_SHOW_ALWAYS" => "N",
                    "PAGER_TEMPLATE" => "",
                    "PAGER_DESC_NUMBERING" => "N",
                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                    "PAGER_SHOW_ALL" => "N",
                    "DISPLAY_DATE" => "Y",
                    "DISPLAY_NAME" => "Y",
                    "DISPLAY_PICTURE" => "N",
                    "DISPLAY_PREVIEW_TEXT" => "Y",
                    "AJAX_OPTION_ADDITIONAL" => ""
                ),
                    false
                ); ?>
            <? endif; ?>
        </div>
        <div class="rightside_index">
            <div class="content">
                <? if (!defined("NOT_SHOW_BREADCRUMB")): ?><? $APPLICATION->IncludeComponent("bitrix:breadcrumb", "", array()); ?><? endif; ?>
                <? if ($APPLICATION->GetCurDir() != SITE_DIR): ?>
                <h1><?= $APPLICATION->ShowTitle(false); ?></h1>
<? endif; ?>