$(function(){
    $('.phonebut').click(function(){
        showPopup('popup-ordercall');
        return false;
    });

    // $(".hbut3").fancybox({
    //     centerOnScroll: true,
    //     padding: [0,0,0,0],
    //     fitToView : false,
    //     transitionIn		: 'none',
    //     transitionOut		: 'none',
    //     width		: 1060,
    //     height		: 663,
    //     type				: 'iframe'
    // });

    $('#call_offer .phone').mask("(999) 999-99-99")
    var phoneMessage = ""
    $('#call_offer input[type=text]').val(phoneMessage)
    $('#call_offer input[type=text]').blur(function(){
        setTimeout(function(){
            var phone = $('#call_offer input[type=text]')
            if (phone.val() == "") {
                $('#call_offer input[type=text]').val(phoneMessage)
            }
        },0)
    })
	$('#call_offer .btn').attr('onclick', "yaCounter20154625.reachGoal('middletel'); _gaq.push(['_trackPageview', '/middle']); return true;") 
    $('#call_offer .btn').click(function(){
        if ($('#call_offer input[type=text]').val() == phoneMessage) {
            alert("Укажите телефон с кодом!");
            return false    
        }
        $.getJSON("/ajax.php",
        {
            cmd: "zakaz_submit_center",
            fio: "-",
            telephone: $("#call_offer input[type=text]").val(),
        }, function(data){
            if (data['result'] == "error") {
                message = "";
                if (data['error']['telephone']) { message += "Укажите телефон с кодом!\r\n"; }
                alert(message);
            } else {
                yaCounter20154625.reachGoal('FeedbackMiddleSuccess')
                _gaq.push(['_trackEvent', 'Обратный звонок (средняя)', 'Успешная отправка']);
                alert("Спасибо! Ваша заявка принята!");
            }
        })
        return false
    })
    $('.zakaz_submit').attr('onclick', "yaCounter20154625.reachGoal('nizbtm'); _gaq.push(['_trackEvent', 'orderCallBack', 'orderCallBackDownSent']); return true;")
    $('.zakaz_submit').click(function(){
        $.getJSON("/ajax.php",
        {
            cmd: "zakaz_submit",
            fio: $('[name="zakaz_fio"]').val(),
            telephone: $('[name="zakaz_telephone"]').val(),
        }, function(data){
            if (data['result'] == "error") {
                message = "";
                if (data['error']['fio']) { message += "Укажите имя!\r\n"; }
                if (data['error']['telephone']) { message += "Укажите телефон с кодом!\r\n"; }
                alert(message);
            } else {
                $('[name="zakaz_fio"]').val('Имя')
                $('[name="zakaz_telephone"]').val('Телефон с кодом')
                yaCounter20154625.reachGoal('FeedbackBottomSuccess')
                _gaq.push(['_trackEvent', 'Обратный звонок (нижняя)', 'Успешная отправка']);
                alert("Спасибо! Ваша заявка принята!");
            }
        });
        return false;
    });
    $("[name=zakaz_telephone]").mask("+7 (999) 999-99-99");
    $('[name=zakaz_telephone]').val('Ваш телефон:')

    $('#orderCall1').bind('click',function(){
        
    })


    $('.side_callback_submit').click(function(){
        $.getJSON("/ajax.php",
        {
            cmd: "zakaz_submit_side",
            fio: $('#side_callback .field_fio').val(),
            telephone: '+7' + $('#side_callback .field_phone').val(),
        }, function(data){
            if (data['result'] == "error") {
                message = "";
                if (data['error']['fio']) { message += "Укажите имя!\r\n"; }
                if (data['error']['telephone']) { message += "Укажите телефон с кодом!\r\n"; }
                alert(message);
            } else {
                $('#side_callback .field_fio').val('')
                $('#side_callback .field_phone').val('')
                yaCounter20154625.reachGoal('FeedbackSideSuccess')
                _gaq.push(['_trackEvent', 'Обратный звонок (боковая)', 'Успешная отправка']);
                alert("Спасибо! Ваша заявка принята!");
            }
        });
        return false;
    });
});
function setCenter(item) {
	windowHeight = document.documentElement.clientHeight;
	currentOffset = document.documentElement.scrollTop || document.body.scrollTop;
	currentOffset = currentOffset + parseInt((windowHeight - $(item).outerHeight()) / 2);
	currentOffset = (currentOffset < 10) ? 10 : currentOffset;
	pLeft = parseInt(($(window).width() - item.outerWidth()) / 2);
	item.css({top:currentOffset,left:pLeft}).show();
}
function showPopup(popup) {
	popup = $('#'+popup);
	setCenter(popup);
	createBlind(popup);
	popup.find('.popup-close').click(function(){
		closePopup(popup);
		//return false;
	});
};
function createBlind(popup) {
	var blind = $('<div id="blind"></div>');
	blind.height($(document).height()).appendTo('body');
	blind.click(function(){
		closePopup(popup);
		//return false;
	});
};
function closePopup(popup) {
	$('#blind').remove();
	popup.hide();
	return false;
};