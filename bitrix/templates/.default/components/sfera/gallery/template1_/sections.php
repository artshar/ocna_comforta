<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
$arSort = array("SORT" => "ASC");
$arFilter = array("IBLOCK_ID" => 3, "GLOBAL_ACTIVE" => "Y");

$rsElement = CIBlockSection::GetList($arSort, $arFilter, false);
while($obElement = $rsElement->GetNextElement())
{
    $arItem = $obElement->GetFields();

    $arFile = "";
    if (is_numeric($arItem["PICTURE"]))
    {
        $arFileTmp = CFile::ResizeImageGet(
        $arItem["PICTURE"],
        array("width" => 100, "height" => 100),
        array(),
        BX_RESIZE_IMAGE_PROPORTIONAL,
        true, false);

        $arFile['WIDTH'] = $arFileTmp["width"];
        $arFile['HEIGHT'] = $arFileTmp["height"];
        $arFile['SRC'] = $arFileTmp['src'];
    }

    $arResult["SECTIONS"][] = array(
        "ID" => $arItem["ID"],
        "NAME" => $arItem["NAME"],
        "PICTURE" => $arFile,
        "DESCRIPTION" => $arItem["DESCRIPTION"],
        "SECTION_PAGE_URL" => $arItem["SECTION_PAGE_URL"]
    );
}
?>
<?if(count($arResult["SECTIONS"])>0):?>
    <div class="gallery">
<?    foreach($arResult["SECTIONS"] as $arItem):?>
      <div class="item">
<?        if(is_array($arItem["PICTURE"])):?>
        <div class="img"><a href="<?=$arItem["SECTION_PAGE_URL"]?>"><img src="<?=$arItem["PICTURE"]["SRC"]?>" alt="<?=$arItem["NAME"]?>" border="0"></a></div>
<?        endif;?>
        <div class="name"><a href="<?=$arItem["SECTION_PAGE_URL"]?>"><?=$arItem["NAME"]?></a></div>
<?        if($arItem["DESCRIPTION"] != ""):?>
        <div class="desc"><?=$arItem["DESCRIPTION"]?></div>
<?        endif;?>
      </div>
<?    endforeach;?>
    </div>
<?endif;?>