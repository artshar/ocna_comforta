<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
$ucResizeImg = new C_ucResizeImg;
$ucResizeImg->SetDefParams(array(
    'KEEP_SIZE' => true,
    'FILL_COLOR' => array('R'=>255,'G'=>255,'B'=>255), # Белый фон
));

$arSort = array("SORT" => "ASC");
$arFilter = array("IBLOCK_ID" => 3, "SECTION_CODE" => $arParams["SECTION_CODE"], "ACTIVE" => "Y");
$arSelect = array("ID", "NAME", "DETAIL_PICTURE", "DETAIL_TEXT");
$rsElement = CIBlockElement::GetList($arSort, $arFilter, false, false, $arSelect);
while($obElement = $rsElement->GetNextElement())
{
    $arItem = $obElement->GetFields();

    $arFile = "";
    if (is_numeric($arItem["DETAIL_PICTURE"]))
    {
        $arFileTmp = CFile::ResizeImageGet(
        $arItem["DETAIL_PICTURE"],
        array("width" => 100, "height" => 100),
        array(),
        BX_RESIZE_IMAGE_PROPORTIONAL,
        true, false);

        $arFile['WIDTH'] = $arFileTmp["width"];
        $arFile['HEIGHT'] = $arFileTmp["height"];
        $arFile['SRC'] = $arFileTmp['src'];
    }

    $arResult["ITEMS"][] = array(
        "ID" => $arItem["ID"],
        "NAME" => $arItem["NAME"],
        "DETAIL_TEXT" => $arItem["DETAIL_TEXT"],
        "DETAIL_PICTURE" => CFile::GetFileArray($arItem["DETAIL_PICTURE"]),
        "PICTURE" => $arFile,
    );
}

if (count($arResult["ITEMS"])>0)
{
    foreach($arResult["ITEMS"] as $key => $arElement)
    {
        if($arElement['DETAIL_PICTURE']['WIDTH'] > 800 || $arElement['DETAIL_PICTURE']['HEIGHT'] > 600) {
            $width = 800; $height = 600;
            if($arElement['DETAIL_PICTURE']['HEIGHT'] > $arElement['DETAIL_PICTURE']['WIDTH'])
            {
                $width = 600; $height = 800;
            }
            $arResult["ITEMS"][$key]['DETAIL_PICTURE']['SRC'] = $ucResizeImg->GetResized(array(
                'INPUT_FILE' => $arElement['DETAIL_PICTURE']['SRC'],
                'WIDTH' => $width,
                'HEIGHT' => $height,
            ));
        }
    }
}

$arFilter = array("IBLOCK_ID" => 3, "CODE" => $arParams["SECTION_CODE"], "ACTIVE" => "Y");
$db_list = CIBlockSection::GetList($arSort, $arFilter, true);
while($ar_result = $db_list->GetNext())
{
    $arResult["SECTION"] = $ar_result;
}
$APPLICATION->AddChainItem($arResult["SECTION"]["NAME"], $arResult["SECTION"]["~SECTION_PAGE_URL"]);
$APPLICATION->SetTitle($arResult["SECTION"]["NAME"]);
?>
<?if(count($arResult["ITEMS"])>0):?>
    <script type="text/javascript" src="/fancybox/js/jquery.fancybox-1.3.4.pack.js"></script>
    <script type="text/javascript" src="/fancybox/js/jquery.easing-1.4.pack.js"></script>
    <script type="text/javascript" src="/fancybox/js/jquery.mousewheel-3.0.4.pack.js"></script>
    <link rel="stylesheet" href="/fancybox/css/jquery.fancybox-1.3.4.css" type="text/css" media="screen" />
    <script type="text/javascript">
    $(document).ready(function() {
        $("a[rel=gallery_group]").fancybox({
            'transitionIn' : 'elastic',
            'transitionOut' : 'elastic',
            'titlePosition' : 'over',
            'titleFormat' : function(title, currentArray, currentIndex, currentOpts) {
                return '<span id="fancybox-title-over">Image ' + (currentIndex + 1) + ' / ' + currentArray.length + (title.length ? ' &nbsp; ' + title : '') + '</span>';
            }
        });
    });
    </script>
    <div class="gallery_photos">
<?    foreach($arResult["ITEMS"] as $arElement):?>
      <div class="item">
<?        if(is_array($arElement["PICTURE"])):?>
        <div class="img"><a rel="gallery_group" href="<?=$arElement["DETAIL_PICTURE"]["SRC"]?>"><img src="<?=$arElement["PICTURE"]["SRC"]?>" alt="<?=$arElement["NAME"]?>" border="0"></a></div>
<?        endif;?>
        <div class="name"><?=$arElement["NAME"]?></div>
<?        if($arElement["DETAIL_TEXT"] != ""):?>
        <div class="desc"><?=$arElement["DETAIL_TEXT"]?></div>
<?        endif;?>
      </div>
<?    endforeach;?>
    </div>
<?endif;?>
    <br />
<div>Посмотреть другие фотографии:
<a href="/gallery/the-windows-of-the-cottages/">Окна в коттеджах</a>,
<a href="/gallery/the-windows-in-the-apartment/">  Остекление балконов</a>,
<a href="/gallery/glazing-of-balconies/">Окна в квартирах</a></div>
<br /><br />