<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<table class="szd-shop-catalog-element">
	<?
	$this->AddEditAction($arResult['ID'], $arResult['EDIT_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arResult['ID'], $arResult['DELETE_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BCS_ELEMENT_DELETE_CONFIRM')));
	?>
	<tr class="szd-shop-product" id="<?=$this->GetEditAreaId($arResult['ID']);?>">
	<? if(is_array($arResult["DETAIL_PICTURE"])):?>
		<td class="szd-shop-pic">
			<img border="0" src="<?=$arResult["DETAIL_PICTURE"]["SRC"]?>" width="<?=$arResult["DETAIL_PICTURE"]["WIDTH"]?>" height="<?=$arResult["DETAIL_PICTURE"]["HEIGHT"]?>" alt="<?=$arResult["NAME"]?>" title="<?=$arResult["NAME"]?>" />
		</td>
		<td class="szd-shop-info" colspan="1">
	<? elseif(is_array($arResult["PREVIEW_PICTURE"])):?>
		<td class="szd-shop-pic">
			<img border="0" src="<?=$arResult["PREVIEW_PICTURE"]["SRC"]?>" width="<?=$arResult["PREVIEW_PICTURE"]["WIDTH"]?>" height="<?=$arResult["PREVIEW_PICTURE"]["HEIGHT"]?>" alt="<?=$arResult["NAME"]?>" title="<?=$arResult["NAME"]?>" />
		</td>
		<td class="szd-shop-info" colspan="1">
	<? else: ?>
		<td class="szd-shop-info" colspan="2">
	<? endif?>
			<b><?=$arResult["NAME"]?></b>

			<table cellpadding="0" cellpadding="0">
				<tr>
					<td>
						<b><?=$arResult["DISPLAY_PROPERTIES"][$arResult["SZD_SHOP_PRICE_CODE"]]["NAME"]?></b>:&nbsp;<?=$arResult["DISPLAY_PROPERTIES"][$arResult["SZD_SHOP_PRICE_CODE"]]["VALUE"]?>&nbsp;<?=$arResult["SHOP_CURRENCY"]?><br/>
					</td>
					<td class="szd-shop-buttons" align="right">
						<div class="szd-shop-buttons-basket">
							<noindex>
							<? $APPLICATION->IncludeComponent(
								"sozdavatel:shop.button", 
								"", 
								Array(
									"ELEMENT_ID" 			=> $arResult["ID"],
									"BUTTON_TYPE" 			=> $arParams["BUTTON_TYPE"],
									"TO_BASKET_TEXT" 		=> $arParams["TO_BASKET_TEXT"],
									"IN_BASKET_TEXT" 		=> $arParams["IN_BASKET_TEXT"],
									"BUTTON_COLOR" 			=> $arParams["BUTTON_COLOR"],
									"BUTTON_COLOR_DISABLED"	=> $arParams["BUTTON_COLOR_DISABLED"],
									"TEXT_COLOR" 			=> $arParams["TEXT_COLOR"],
								)
							);?>
							</noindex>
						</div>
					</td>
				</tr>
			</table>
			
			<? unset($arResult["DISPLAY_PROPERTIES"][$arResult["SZD_SHOP_PRICE_CODE"]]); ?>
			<? foreach($arResult["DISPLAY_PROPERTIES"] as $pid=>$arProperty):?>
				<?=$arProperty["NAME"]?>:&nbsp;<?
					if(is_array($arProperty["DISPLAY_VALUE"]))
						echo implode("&nbsp;/&nbsp;", $arProperty["DISPLAY_VALUE"]);
					else
						echo $arProperty["DISPLAY_VALUE"];?><br />
			<? endforeach?>
			<? if ($arResult["DETAIL_TEXT"]): ?>
				<p><?=$arResult["DETAIL_TEXT"]?></p>
			<? elseif ($arResult["PREVIEW_TEXT"]): ?>
				<p><?=$arResult["PREVIEW_TEXT"]?></p>
			<? endif; ?>
		</td>
	</tr>
</table>

<? if(is_array($arResult["SECTION"])):?>
	<br /><a href="<?=$arResult["SECTION"]["SECTION_PAGE_URL"]?>"><?=GetMessage("CATALOG_BACK")?></a>
<? endif?>

