<?
	$maxImgHeight = 10;
	$maxImgWidth = 10;
	if (intval($arParams["LINE_ELEMENT_COUNT"]) > 1)
	{
		$lineCount = intval($arParams["LINE_ELEMENT_COUNT"]);
	}
	else
	{
		$lineCount = 3;
	}
	$arResult["TD_WIDTH"] = round(100/$lineCount)."%";
	
	$arTableProps = array();
	$arResult["ROWS"] = array();
	$arResult["SZD_SHOP_PRICE_CODE"] = COption::GetOptionString("sozdavatel.shop", "PRICE_PROPERTY_ID");
	$i = 0;
	$row = 0;
	foreach ($arResult["ITEMS"] as $arItem)
	{
		if ($arItem["PREVIEW_PICTURE"])
		{
			if ($maxImgHeight < $arItem["PREVIEW_PICTURE"]["HEIGHT"])
			{	
				$maxImgHeight = $arItem["PREVIEW_PICTURE"]["HEIGHT"];
			}
			if ($maxImgWidth < $arItem["PREVIEW_PICTURE"]["WIDTH"])
			{	
				$maxImgWidth = $arItem["PREVIEW_PICTURE"]["WIDTH"];
			}
		}
		elseif ($arItem["DETAIL_PICTURE"])
		{
			if ($maxImgHeight < $arItem["DETAIL_PICTURE"]["HEIGHT"])
			{	
				$maxImgHeight = $arItem["DETAIL_PICTURE"]["HEIGHT"];
			}
			if ($maxImgWidth < $arItem["DETAIL_PICTURE"]["WIDTH"])
			{	
				$maxImgWidth = $arItem["DETAIL_PICTURE"]["WIDTH"];
			}
		}
		
		foreach ($arItem["DISPLAY_PROPERTIES"] as $code=>$arProp)
		{
			if ($code != $arResult["SZD_SHOP_PRICE_CODE"])
			{
				$arTableProps[$code] = $arProp["NAME"];
			}
		}
		if ($i >= $lineCount)
		{
			$i = 0;
			$row++;
		}
		$arResult["ROWS"][$row]["ITEMS"][] = $arItem;
		$i++;
	}
	$arYableProps[$arResult["SZD_SHOP_PRICE_CODE"]] = GetMessage("TABLE_PRICE");
	$arResult["TABLE_PROPERTIES"] = $arTableProps;
	$arResult["MAX_IMG_HEIGHT"] = $maxImgHeight-10;
	$arResult["MAX_IMG_WIDTH"] = $maxImgWidth-10;
	
	$arResult["SHOP_CURRENCY"] = COption::GetOptionString("sozdavatel.shop", "SHOP_CURRENCY");
?>