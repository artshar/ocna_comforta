<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if($arParams["DISPLAY_TOP_PAGER"]):?>
	<?=$arResult["NAV_STRING"]?><br />
<?endif;?>

<div class="szd-shop-mosaic-catalog-section">
<table cellpadding="8" cellspacing="20">
	<? foreach($arResult["ROWS"] as $arRow):?>
	<tr>
		<? foreach($arRow["ITEMS"] as $key=>$arElement): ?>

		<?
		$this->AddEditAction($arElement['ID'], $arElement['EDIT_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_EDIT"));
		$this->AddDeleteAction($arElement['ID'], $arElement['DELETE_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BCS_ELEMENT_DELETE_CONFIRM')));
		?>
		<td id="<?=$this->GetEditAreaId($arElement['ID']);?>" style="width: <?=$arResult["TD_WIDTH"]?>;">
			<div class="szd-name">
				<a title="<?=$arElement["NAME"]?>" href="<?=$arElement["DETAIL_PAGE_URL"]?>"><?=$arElement["NAME"]?></a>
			</div>
			<br/>
			<? if ($pic = $arElement["PREVIEW_PICTURE"]["SRC"]):?>
				<div class="szd-preview" style="width: <?=$arResult["MAX_IMG_WIDTH"]?>px; height: <?=$arResult["MAX_IMG_HEIGHT"]?>px;">
					<img class="szd-preview-picture" src="<?=$pic?>" alt="<?=$arElement["NAME"]?>" style="max-width: <?=$arResult["MAX_IMG_WIDTH"]?>px; max-height: <?=$arResult["MAX_IMG_HEIGHT"]?>px;"/>
				</div>
				<br/>
			<? elseif ($pic = $arElement["DETAIL_PICTURE"][
			"SRC"]):?>
				<div class="szd-preview" style="width: <?=$arResult["MAX_IMG_WIDTH"]?>px; height: <?=$arResult["MAX_IMG_HEIGHT"]?>px;">
					<img class="szd-preview-picture" src="<?=$pic?>" alt="<?=$arElement["NAME"]?>" style="max-width: <?=$arResult["MAX_IMG_WIDTH"]?>px; max-height: <?=$arResult["MAX_IMG_HEIGHT"]?>px;"/>
				</div>
				<br/>
			<? endif; ?>

			<? if ($price = $arElement["PROPERTIES"][$arResult["SZD_SHOP_PRICE_CODE"]]["VALUE"]): ?>
				<div class="szd-price">
					<b><?=$price?></b>&nbsp;<?=$arResult["SHOP_CURRENCY"]?>
				</div>
				<br/>
			<? endif; ?>
			<div class="szd-buy-button">
				<noindex>
				<? $APPLICATION->IncludeComponent(
				"sozdavatel:shop.button", 
					"", 
					Array(
						"ELEMENT_ID" 			=> $arElement["ID"],
						"BUTTON_TYPE" 			=> $arParams["BUTTON_TYPE"],
						"TO_BASKET_TEXT" 		=> $arParams["TO_BASKET_TEXT"],
						"IN_BASKET_TEXT" 		=> $arParams["IN_BASKET_TEXT"],
						"BUTTON_COLOR" 			=> $arParams["BUTTON_COLOR"],
						"BUTTON_COLOR_DISABLED"	=> $arParams["BUTTON_COLOR_DISABLED"],
						"TEXT_COLOR" 			=> $arParams["TEXT_COLOR"],
					)
				);?>
				</noindex>
			</div>
			<br/>
			<? if($arParams["DISPLAY_COMPARE"]):?>
				<div class="szd-compare-button">
					<noindex>
						<a href="<?=$arElement["COMPARE_URL"]?>" rel="nofollow"><?=GetMessage("CATALOG_COMPARE")?></a>
					</noindex>
				</div>
				<br/>
			<? endif?>
			<? unset($arElement["DISPLAY_PROPERTIES"][$arResult["SZD_SHOP_PRICE_CODE"]]); ?>
			<? if (count($arElement["DISPLAY_PROPERTIES"]) > 0 ): ?>
				<div class="szd-properties">
				<? foreach($arElement["DISPLAY_PROPERTIES"] as $pid=>$arProperty):?>	
					<?=$arProperty["NAME"]?>:&nbsp;<?
						if(is_array($arProperty["DISPLAY_VALUE"]))
							echo implode("&nbsp;/&nbsp;", $arProperty["DISPLAY_VALUE"]);
						else
							echo $arProperty["DISPLAY_VALUE"];?><br />
				<? endforeach?>
				</div>
			<? endif; ?>
			<? if (($arElement["PREVIEW_TEXT"]) && ($arParams["SECTION_SHOW_PREVIEW_TEXT"] == "Y")): ?>
				<div class="szd-preview-text"><?=$arElement["PREVIEW_TEXT"]?></div>
			<? endif; ?>
		</td>
		<? endforeach; ?>
	</tr>
	<? endforeach; ?>
</table>
</div>

<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<br /><?=$arResult["NAV_STRING"]?>
<?endif;?>
