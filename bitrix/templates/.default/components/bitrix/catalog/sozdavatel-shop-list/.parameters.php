<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arTemplateParameters = array(
	"SHOW_ORDERS_STATUSES" => array(
		"NAME" => GetMessage("SHOW_ORDERS_STATUSES"),
		"TYPE" => "LIST",
		"DEFAULT" => "Y",
		"VALUES" => array(
			"Y" => GetMessage("YES"),
			"N" => GetMessage("NO")
		),
	),
	
	"BUTTON_TYPE" => array(
		"NAME" => GetMessage("BUTTON_TYPE"),
		"TYPE" => "LIST",
		"DEFAULT" => "normal",
		"VALUES" => array(
			"rounded" => GetMessage("BUTTON_TYPE_OVAL"),
			"normal" => GetMessage("BUTTON_TYPE_RECTANGLE")
		),
	),
	"TO_BASKET_TEXT" => array(
		"NAME" => GetMessage("TO_BASKET_TEXT"),
		"TYPE" => "TEXT",
		"DEFAULT" => GetMessage("TO_BASKET_TEXT_DEFAULT"),
	),
	"IN_BASKET_TEXT" => array(
		"NAME" => GetMessage("IN_BASKET_TEXT"),
		"TYPE" => "TEXT",
		"DEFAULT" => GetMessage("IN_BASKET_TEXT_DEFAULT"),
	),
	"BUTTON_COLOR" => array(
		"NAME" => GetMessage("BUTTON_COLOR"),
		"TYPE" => "TEXT",
		"DEFAULT" => "f26522",
	),
	"BUTTON_COLOR_DISABLED" => array(
		"NAME" => GetMessage("BUTTON_COLOR_DISABLED"),
		"TYPE" => "TEXT",
		"DEFAULT" => "898989",
	),
	"TEXT_COLOR" => array(
		"NAME" => GetMessage("TEXT_COLOR"),
		"TYPE" => "TEXT",
		"DEFAULT" => "FFFFFF",
	),
);
?>