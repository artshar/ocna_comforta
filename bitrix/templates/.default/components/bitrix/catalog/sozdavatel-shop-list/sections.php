<? 
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$APPLICATION->IncludeComponent(
	"bitrix:catalog.section.list",
	"",
	Array(
		"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
		"IBLOCK_ID" => $arParams["IBLOCK_ID"],
		"DISPLAY_PANEL" => $arParams["DISPLAY_PANEL"],
		"CACHE_TYPE" => $arParams["CACHE_TYPE"],
		"CACHE_TIME" => $arParams["CACHE_TIME"],
		"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
		"SECTION_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["section"]
	),
	$component
);
?><br><?

if($arParams["USE_COMPARE"]=="Y"):
$APPLICATION->IncludeComponent(
	"bitrix:catalog.compare.list",
	"",
	Array(
		"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
		"IBLOCK_ID" => $arParams["IBLOCK_ID"],
		"NAME" => $arParams["COMPARE_NAME"],
		"DETAIL_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["element"],
		"COMPARE_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["compare"],
	),
	$component
);
?><br /><? 
endif;

if ($arParams["SHOW_ORDERS_STATUSES"] == "Y")
{
	$APPLICATION->IncludeComponent(
		"sozdavatel:shop.orders.statuses", 
		"", 
		Array(
			"BUTTON_TYPE" 			=> $arParams["BUTTON_TYPE"],
			"BUTTON_COLOR" 			=> $arParams["BUTTON_COLOR"],
			"TEXT_COLOR" 			=> $arParams["TEXT_COLOR"],
		),
		false
	);
}

$APPLICATION->IncludeComponent(
	"sozdavatel:shop.basket.full",
	"",
	Array(
		"BUTTON_TYPE" 			=> $arParams["BUTTON_TYPE"],
		"TO_BASKET_TEXT" 		=> $arParams["TO_BASKET_TEXT"],
		"IN_BASKET_TEXT" 		=> $arParams["IN_BASKET_TEXT"],
		"BUTTON_COLOR" 			=> $arParams["BUTTON_COLOR"],
		"BUTTON_COLOR_DISABLED"	=> $arParams["BUTTON_COLOR_DISABLED"],
		"TEXT_COLOR" 			=> $arParams["TEXT_COLOR"],
	),
	false
);
if($arParams["SHOW_TOP_ELEMENTS"]!="N"):
?><br/><?
$APPLICATION->IncludeComponent(
	"bitrix:catalog.top",
	"sozdavatel-shop-list",
	Array(
		"BUTTON_TYPE" 			=> $arParams["BUTTON_TYPE"],
		"TO_BASKET_TEXT" 		=> $arParams["TO_BASKET_TEXT"],
		"IN_BASKET_TEXT" 		=> $arParams["IN_BASKET_TEXT"],
		"BUTTON_COLOR" 			=> $arParams["BUTTON_COLOR"],
		"BUTTON_COLOR_DISABLED"	=> $arParams["BUTTON_COLOR_DISABLED"],
		"TEXT_COLOR" 			=> $arParams["TEXT_COLOR"],
		
		"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
		"IBLOCK_ID" => $arParams["IBLOCK_ID"],
		"ELEMENT_SORT_FIELD" => $arParams["TOP_ELEMENT_SORT_FIELD"],
		"ELEMENT_SORT_ORDER" => $arParams["TOP_ELEMENT_SORT_ORDER"],
		"SECTION_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["section"],
		"DETAIL_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["element"],
		"BASKET_URL" => $arParams["BASKET_URL"],
		"ACTION_VARIABLE" => $arParams["ACTION_VARIABLE"],
		"PRODUCT_ID_VARIABLE" => $arParams["PRODUCT_ID_VARIABLE"],
		"SECTION_ID_VARIABLE" => $arParams["SECTION_ID_VARIABLE"],
		"DISPLAY_COMPARE" => $arParams["USE_COMPARE"],
		"ELEMENT_COUNT" => $arParams["TOP_ELEMENT_COUNT"],
		"LINE_ELEMENT_COUNT" => $arParams["TOP_LINE_ELEMENT_COUNT"],
		"PROPERTY_CODE" => $arParams["TOP_PROPERTY_CODE"],
		"PRICE_CODE" => $arParams["PRICE_CODE"],
		"USE_PRICE_COUNT" => $arParams["USE_PRICE_COUNT"],
		"SHOW_PRICE_COUNT" => $arParams["SHOW_PRICE_COUNT"],
		"PRICE_VAT_INCLUDE" => $arParams["PRICE_VAT_INCLUDE"],
		"PRICE_VAT_SHOW_VALUE" => $arParams["PRICE_VAT_SHOW_VALUE"],
		"CACHE_TYPE" => $arParams["CACHE_TYPE"],
		"CACHE_TIME" => $arParams["CACHE_TIME"],
		"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
		"OFFERS_CART_PROPERTIES" => $arParams["OFFERS_CART_PROPERTIES"],
		"OFFERS_FIELD_CODE" => $arParams["TOP_OFFERS_FIELD_CODE"],
		"OFFERS_PROPERTY_CODE" => $arParams["TOP_OFFERS_PROPERTY_CODE"],
		"OFFERS_SORT_FIELD" => $arParams["OFFERS_SORT_FIELD"],
		"OFFERS_SORT_ORDER" => $arParams["OFFERS_SORT_ORDER"],
		"OFFERS_LIMIT" => $arParams["TOP_OFFERS_LIMIT"],
	),
$component
);
endif;
?>