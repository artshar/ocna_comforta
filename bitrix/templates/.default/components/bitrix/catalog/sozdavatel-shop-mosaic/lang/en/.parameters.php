<?
	$MESS ['SHOW_ORDERS_STATUSES'] = 'Показывать историю заказов клиента';
	$MESS ['YES'] = 'Да';
	$MESS ['NO'] = 'Нет';
	
	$MESS ['BUTTON_TYPE'] = 'Вид кнопки';
	$MESS ['BUTTON_TYPE_ARROW'] = 'Стрелка';
	$MESS ['BUTTON_TYPE_OVAL'] = 'Овал';
	$MESS ['BUTTON_TYPE_RECTANGLE'] = 'Прямоугольник';
	
	$MESS ['TO_BASKET_TEXT'] = 'Текст на активной кнопке';
	$MESS ['TO_BASKET_TEXT_DEFAULT'] = 'Купить';
	$MESS ['IN_BASKET_TEXT'] = 'Текст на неактивной кнопке';
	$MESS ['IN_BASKET_TEXT_DEFAULT'] = 'В&nbsp;заказе';
	$MESS ['BUTTON_COLOR'] = 'Цвет активной кнопки (RGB) #';
	$MESS ['BUTTON_COLOR_DISABLED'] = 'Цвет неактивной кнопки (RGB) #';
	$MESS ['TEXT_COLOR'] = 'Цвет текста (RGB) #';
	$MESS ['SECTION_SHOW_PREVIEW_TEXT'] = 'Показывать текст анонса в списке товаров';
	
	$MESS ['YES'] = 'Да';
	$MESS ['NO'] = 'Нет';
?>