<?php
if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true)die();?>

<?
$APPLICATION->IncludeComponent(
	'bitrix:crm.lead.menu', 
	'', 
	array(
		'PATH_TO_LEAD_LIST' => $arResult['PATH_TO_LEAD_LIST'],
		'PATH_TO_LEAD_SHOW' => $arResult['PATH_TO_LEAD_SHOW'],
		'PATH_TO_LEAD_EDIT' => $arResult['PATH_TO_LEAD_EDIT'],
		'PATH_TO_LEAD_CONVERT' => $arResult['PATH_TO_LEAD_CONVERT'],
		'PATH_TO_LEAD_IMPORT' => $arResult['PATH_TO_LEAD_IMPORT'],	
		'ELEMENT_ID' => $arResult['VARIABLES']['lead_id'],
		'TYPE' => 'edit'
	),
	$component
);?>

<?
$APPLICATION->IncludeComponent(
	'bitrix:crm.lead.edit', 
	'', 
	array(
		'PATH_TO_LEAD_SHOW' => $arResult['PATH_TO_LEAD_SHOW'],
		'PATH_TO_LEAD_LIST' => $arResult['PATH_TO_LEAD_LIST'],
		'PATH_TO_LEAD_EDIT' => $arResult['PATH_TO_LEAD_EDIT'],
		'PATH_TO_LEAD_CONVERT' => $arResult['PATH_TO_LEAD_CONVERT'],
		'PATH_TO_USER_PROFILE' => $arResult['PATH_TO_USER_PROFILE'],
		'ELEMENT_ID' => $arResult['VARIABLES']['lead_id']
	),
	$component
);?>