<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<? if($arParams["DISPLAY_TOP_PAGER"]):?>
	<?=$arResult["NAV_STRING"]?><br />
<? endif;?>

<table class="szd-shop-list-catalog-section">
<? foreach($arResult["ITEMS"] as $key=>$arElement):?>
	<?
	$this->AddEditAction($arElement['ID'], $arElement['EDIT_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arElement['ID'], $arElement['DELETE_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BCS_ELEMENT_DELETE_CONFIRM')));
	?>
	<tr class="szd-shop-product" id="<?=$this->GetEditAreaId($arElement['ID']);?>">
	<? if(is_array($arElement["PREVIEW_PICTURE"])):?>
		<td class="szd-shop-pic">
			<a href="<?=$arElement["DETAIL_PAGE_URL"]?>"><img border="0" src="<?=$arElement["PREVIEW_PICTURE"]["SRC"]?>" width="<?=$arElement["PREVIEW_PICTURE"]["WIDTH"]?>" height="<?=$arElement["PREVIEW_PICTURE"]["HEIGHT"]?>" alt="<?=$arElement["NAME"]?>" title="<?=$arElement["NAME"]?>" /></a>
		</td>
		<td class="szd-shop-info" colspan="1">
	<? elseif(is_array($arElement["DETAIL_PICTURE"])):?>
		<td class="szd-shop-pic">
			<a href="<?=$arElement["DETAIL_PAGE_URL"]?>"><img border="0" src="<?=$arElement["DETAIL_PICTURE"]["SRC"]?>" width="<?=$arElement["DETAIL_PICTURE"]["WIDTH"]?>" height="<?=$arElement["DETAIL_PICTURE"]["HEIGHT"]?>" alt="<?=$arElement["NAME"]?>" title="<?=$arElement["NAME"]?>" /></a>
		</td>
		<td class="szd-shop-info" colspan="1">
	<? else: ?>
		<td class="szd-shop-info" colspan="2">
	<? endif?>
			<a href="<?=$arElement["DETAIL_PAGE_URL"]?>"><b><?=$arElement["NAME"]?></b></a><br />
			
			<b><?=$arElement["DISPLAY_PROPERTIES"][$arResult["SZD_SHOP_PRICE_CODE"]]["NAME"]?></b>:&nbsp;<?=$arElement["DISPLAY_PROPERTIES"][$arResult["SZD_SHOP_PRICE_CODE"]]["VALUE"]?>&nbsp;<?=$arResult["SHOP_CURRENCY"]?><br/>
			
			<? unset($arElement["DISPLAY_PROPERTIES"][$arResult["SZD_SHOP_PRICE_CODE"]]); ?>
			<? foreach($arElement["DISPLAY_PROPERTIES"] as $pid=>$arProperty):?>
				<?=$arProperty["NAME"]?>:&nbsp;<?
					if(is_array($arProperty["DISPLAY_VALUE"]))
						echo implode("&nbsp;/&nbsp;", $arProperty["DISPLAY_VALUE"]);
					else
						echo $arProperty["DISPLAY_VALUE"];?><br />
			<? endforeach?>
			<? if ($arElement["PREVIEW_TEXT"]): ?>
				<p><?=$arElement["PREVIEW_TEXT"]?></p>
			<? endif; ?>
		</td>
		<td class="szd-shop-buttons" align="right">
			<div class="szd-shop-buttons-basket">
				<noindex>
				<? $APPLICATION->IncludeComponent(
					"sozdavatel:shop.button", 
					"", 
					Array(
						"ELEMENT_ID" 			=> $arElement["ID"],
						"BUTTON_TYPE" 			=> $arParams["BUTTON_TYPE"],
						"TO_BASKET_TEXT" 		=> $arParams["TO_BASKET_TEXT"],
						"IN_BASKET_TEXT" 		=> $arParams["IN_BASKET_TEXT"],
						"BUTTON_COLOR" 			=> $arParams["BUTTON_COLOR"],
						"BUTTON_COLOR_DISABLED"	=> $arParams["BUTTON_COLOR_DISABLED"],
						"TEXT_COLOR" 			=> $arParams["TEXT_COLOR"],
					)
				);?>
				</noindex>
			</div>
			<? if($arParams["DISPLAY_COMPARE"]):?>
			<div class="szd-shop-buttons-compare">
				<noindex>
				<a href="<?=$arElement["COMPARE_URL"]?>" rel="nofollow"><?=GetMessage("CATALOG_COMPARE")?></a>
				</noindex>
			</div>
			<? endif?>
		</td>
	</tr>
	<tr><td class="szd-shop-list-splitter" colspan="3"></td></tr>
<? endforeach; // foreach($arResult["ITEMS"] as $arElement):?>
</table>

<? if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<br /><?=$arResult["NAV_STRING"]?>
<? endif;?>

