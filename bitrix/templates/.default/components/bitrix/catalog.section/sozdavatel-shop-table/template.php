<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<? if($arParams["DISPLAY_TOP_PAGER"]):?>
	<?=$arResult["NAV_STRING"]?><br />
<? endif;?>

<div class="szd-shop-table-catalog-section">
<table cellpadding="0" cellspacing="0" border="0">
	<tr>
		<th></th>
		<th><?=GetMessage("TABLE_NAME")?></th>
		<? foreach($arResult["TABLE_PROPERTIES"] as $code=>$name): ?>
			<th><?=$name?></th>
		<? endforeach; ?>
		<th><?=GetMessage("TABLE_PRICE")?>, <?=$arResult["SZD_SHOP_CURRENCY"]?></th>
		<? if($arParams["DISPLAY_COMPARE"]):?>
			<th></th>
		<? endif; ?>
		<th></th>
	</tr>
	<? foreach($arResult["ITEMS"] as $cell=>$arElement):?>
		<?
		$this->AddEditAction($arElement['ID'], $arElement['EDIT_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_EDIT"));
		$this->AddDeleteAction($arElement['ID'], $arElement['DELETE_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BCS_ELEMENT_DELETE_CONFIRM')));
		?>
		<tr id="<?=$this->GetEditAreaId($arElement['ID']);?>">
			<td class="szd-shop-center">
			<? if ($pic = $arElement["PREVIEW_PICTURE"]["SRC"]):?>
				<a href="<?=$arElement["DETAIL_PAGE_URL"]?>"><img class="szd-shop-table-preview-pic" src="<?=$pic?>" alt="<?=$arElement["NAME"]?>"/></a>
			<? elseif ($pic = $arElement["DETAIL_PICTURE"]["SRC"]):?>
				<a href="<?=$arElement["DETAIL_PAGE_URL"]?>"><img class="szd-shop-table-preview-pic" src="<?=$pic?>" alt="<?=$arElement["NAME"]?>"/></a>
			<? endif; ?>
			</td>
			<td><a href="<?=$arElement["DETAIL_PAGE_URL"]?>"><?=$arElement["NAME"]?></a></td>
			<? foreach($arResult["TABLE_PROPERTIES"] as $code=>$name): ?>
				<td>
				<?
					if ($arElement["DISPLAY_PROPERTIES"][$code])
					{
						$arValue = $arElement["DISPLAY_PROPERTIES"][$code]["DISPLAY_VALUE"];
					}
					else
					{
						$arValue = $arElement["PROPERTIES"][$code]["VALUE"];
					}
					
					if(is_array($arValue))
					{
						echo implode("&nbsp;/&nbsp;", $arValue);
					}
					else
					{	
						echo $arValue;
					}
				?>
				</td>
			<? endforeach; ?>
			<td>
				<?=$arElement["PROPERTIES"][$arResult["SZD_SHOP_PRICE_CODE"]]["VALUE"]?>
			</td>
			<? if($arParams["DISPLAY_COMPARE"]):?>
			<td>
				<noindex><a href="<?=$arElement["COMPARE_URL"]?>" rel="nofollow"><?=GetMessage("CATALOG_COMPARE")?></a></noindex>
			</td>	
			<? endif?>
			<td>
				<? $APPLICATION->IncludeComponent(
					"sozdavatel:shop.button", 
					"simple", 
					Array(
						"ELEMENT_ID" 			=> $arElement["ID"],
						"BUTTON_TYPE" 			=> $arParams["BUTTON_TYPE"],
						"TO_BASKET_TEXT" 		=> $arParams["TO_BASKET_TEXT"],
						"IN_BASKET_TEXT" 		=> $arParams["IN_BASKET_TEXT"],
						"BUTTON_COLOR" 			=> $arParams["BUTTON_COLOR"],
						"BUTTON_COLOR_DISABLED"	=> $arParams["BUTTON_COLOR_DISABLED"],
						"TEXT_COLOR" 			=> $arParams["TEXT_COLOR"],
					)
				);?>
			</td>
		</tr>
	<? endforeach; ?>
</table>
</div>

<? if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<br /><?=$arResult["NAV_STRING"]?>
<? endif;?>
