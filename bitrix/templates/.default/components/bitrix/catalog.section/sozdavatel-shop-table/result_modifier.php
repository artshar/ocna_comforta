<?
	$arTableProps = array();
	$arResult["SZD_SHOP_PRICE_CODE"] = COption::GetOptionString("sozdavatel.shop", "PRICE_PROPERTY_ID");
	$arResult["SHOW_PIC_TD"] = false;
	foreach ($arResult["ITEMS"] as $arItem)
	{
		foreach ($arItem["DISPLAY_PROPERTIES"] as $code=>$arProp)
		{
			if ($code != $arResult["SZD_SHOP_PRICE_CODE"])
			{
				$arTableProps[$code] = $arProp["NAME"];
			}
		}
		if ($arItem["PREVIEW_PICTURE"]["SRC"] || $arItem["DETAIL_PICTURE"]["SRC"])
		{
			$arResult["SHOW_PIC_TD"] = true;
		}
	}
	$arResult["TABLE_PROPERTIES"] = array_unique($arTableProps);
	
	$arResult["SZD_SHOP_CURRENCY"] = COption::GetOptionString("sozdavatel.shop", "SHOP_CURRENCY");
?>