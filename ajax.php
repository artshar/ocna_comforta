<?
define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS", true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
?>
<?if($_REQUEST['cmd'] == "getcaptcha") {
    $capCode = $GLOBALS["APPLICATION"]->CaptchaGetCode();
    $json['captcha_img'] = "/bitrix/tools/captcha.php?captcha_sid=".htmlspecialchars($capCode);
    $json['captcha_sid'] = htmlspecialchars($capCode);
    echo json_encode($json);
}?>
<?
$from = "oknakomforta.com <noreply@oknakomforta.com>";
/* $to = "dennis@hacktor.ru, dina@oknakomforta.com, marketing@oknakomforta.com"; */
//$to = "boldireva@oknakomforta.com, marketing@oknakomforta.com, snezhana@oknakomforta.com, dina@oknakomforta.com, m.mikhailenko@nectarin.ru";
//$to = 'sergei.sheludchenko@gmail.com';
//$to = 'denis@e-produce.ru';
//$to = "boldireva@oknakomforta.com, m.mikhailenko@nectarin.ru, analitik@oknakomforta.com";

$to = "zamerok@oknakomforta.com, analitik@oknakomforta.com";

$subject = "";
$message = "";

// Форма заказа звонка

if ($_REQUEST['cmd'] == 'calculator')
{

              // cmd: "calculator",
              // telephone: $('#calc .phone').val(),
              // name: $('#calc .name').val(),
              // width: $('#calc .balconwidth').allval(),
              // height: $('#calc .balconheight').allval(),
              // yours: $('#calc .row2b .column.first .toggle').getSelected(),
              // glassing: $('#calc .row2b .column.second .toggle').getSelected(),
              // inner: $('#calc .row3b .column.first .toggle').getSelected(),
              // roof: $('#calc .row3b .column.second .toggle').getSelected(),
              // flapcount:$('#calc_flapcount').val(), 

    $json = array();

    if ($_REQUEST['telephone'] == "" || $_REQUEST['telephone'] == "Телефон с кодом") { $json['error']['telephone'] = true; }

    if(!isset($json['error'])) { $json['result'] = "ok"; } else { $json['result'] = "error"; }

    if ($json['result'] == "ok") {
        CModule::IncludeModule("iblock");
        $res = CIBlockElement::GetProperty(9,423);
        $request_id = $res->Fetch();
        $request_id = $request_id['VALUE'];
        $json['request_id'] = $request_id;
        $request_id++;
        $result = CIBlockElement::SetPropertyValuesEx(423,9,array("request_id"=>$request_id)); 
        $subject = "Окна комфорта - Онлайн-калькулятор";
        $message = "<b>".$subject."</b><br /><br />";
        $message .= "<b>№ заявки:</b> ".($request_id-1)."<br />";
        $message .= "<b>Имя:</b> ".strip_tags(trim($_REQUEST['name']))."<br />";
        $message .= "<b>Телефон:</b> ".strip_tags(trim($_REQUEST['telephone']))."<br />";
        $message .= "<b>Вариант:</b> ".strip_tags(trim($_REQUEST['type']))."<br />";

        if (strrpos($_REQUEST['type'],"окна")) {

            $width = strip_tags(trim($_REQUEST['width']));
            $width = explode('|',$width);

            $height = strip_tags(trim($_REQUEST['height']));
            $height = explode('|',$height);

            $flaps = strip_tags(trim($_REQUEST['flaps']));
            $flaps = explode('|',$flaps);

            $message .= "<b>Куда планируете устанавливать окна:</b> ".strip_tags(trim($_REQUEST['place']))."<br />";

            for ($i=0;$i<sizeof($width);$i++) {
                $message .= "<br>";
                $message .= "<b style='font-size:115%'>Окно " . ($i+1) . "</b><br>";
                $message .= "<b>Количество створок в окне:</b> ".$flaps[$i]."<br />";
                $message .= "<b>Ширина окна, см:</b> ".$width[$i]."<br />";
                $message .= "<b>Высота окна, см:</b> ".$height[$i]."<br />";
            }
            $message .= "<br>";
            $message .= "<b>Модель:</b> ".strip_tags(trim($_REQUEST['model']))."<br />";
            $message .= "<b>Дополнительные функции окна:</b> ".strip_tags(trim($_REQUEST['additional']))."<br />";
            $message .= "<b>Декорировать окна:</b> ".strip_tags(trim($_REQUEST['decoration']))."<br />";
            $message .= "<b>Требуются ли монтажные работы:</b> ".strip_tags(trim($_REQUEST['works']))."<br />";

        } else {

            $message .= "<b>У вас:</b> ".strip_tags(trim($_REQUEST['yours']))."<br />";
            $message .= "<b>Выберите тип остекления:</b> ".strip_tags(trim($_REQUEST['glassing']))."<br />";
            $message .= "<b>Внутренняя отделка:</b> ".strip_tags(trim($_REQUEST['inner']))."<br />";
            $message .= "<b>Крыша для балкона (если последний этаж):</b> ".strip_tags(trim($_REQUEST['roof']))."<br />";
            $message .= "<b>Необходимое количество створок:</b> ".strip_tags(trim($_REQUEST['flapcount']))."<br />";
            $message .= "<b>Высота балконного блока, см:</b> ".strip_tags(trim($_REQUEST['height']))."<br />";
            $message .= "<b>Длина балконного блока, см:</b> ".strip_tags(trim($_REQUEST['width']))."<br />";

        }
        //echo($message);
        sendEmail($from, $to, $subject, $message);
    }
    echo json_encode($json); exit();
}

if ($_REQUEST['cmd'] == "popup-ordercall")
{
    $json = array();

    if ($_REQUEST['fio'] == "" || $_REQUEST['fio'] == "Имя") { $json['error']['fio'] = true; }
    if ($_REQUEST['telephone'] == "" || $_REQUEST['telephone'] == "Телефон с кодом") { $json['error']['telephone'] = true; }

    if(!isset($json['error'])) { $json['result'] = "ok"; } else { $json['result'] = "error"; }

    if ($json['result'] == "ok")
    {
        // $time = "не указано";
        // if($_REQUEST['time'] == 1) { $time = "С 9.00 до 12.00"; }
        // if($_REQUEST['time'] == 2) { $time = "С 12.00 до 14.00"; }
        // if($_REQUEST['time'] == 3) { $time = "С 14.00 до 18.00"; }
        // if($_REQUEST['time'] == 4) { $time = "C 18.00 до 21.00"; }

        $time = $_REQUEST['time'];
        if (!$time) $time = 'не указано';

        $subject = "Окна комфорта - обратный звонок";

        $message = "<b>".$subject."</b><br /><br />";
        $message .= "<b>Контактное лицо:</b> ".$_REQUEST['fio']."<br />";
        $message .= "<b>Телефон:</b> ".$_REQUEST['telephone']."<br />";
        $message .= "<b>Удобное время для звонка:</b> ".$time."<br />";
        $message .= "<b>Форма:</b> Поп-ап";

        sendEmail($from, $to, $subject, $message);
    }

    echo json_encode($json); exit();
}

// Форма вызова замерщика
if ($_REQUEST['cmd'] == "popup-ordermanager")
{
    $json = array();

    if ($_REQUEST['fio'] == "" || $_REQUEST['fio'] == "Имя") { $json['error']['fio'] = true; }
    if ($_REQUEST['telephone'] == "" || $_REQUEST['telephone'] == "Телефон с кодом") { $json['error']['telephone'] = true; }

    if(!isset($json['error'])) { $json['result'] = "ok"; } else { $json['result'] = "error"; }

    if ($json['result'] == "ok")
    {

        $time = $_REQUEST['time'];
        if (!$time) $time = 'не указано';

        $subject = "Окна комфорта - вызов замерщика";

        $message = "<b>".$subject."</b><br /><br />";
        $message .= "<b>Контактное лицо:</b> ".$_REQUEST['fio']."<br />";
        $message .= "<b>Телефон:</b> ".$_REQUEST['telephone']."<br />";
        $message .= "<b>Время звонка:</b> ".$time."<br />";
        $message .= "<b>Сообщение:</b><br />";
        $message .= $_REQUEST['message'];

        sendEmail($from, $to, $subject, $message);
    }

    echo json_encode($json); exit();
}

if ($_REQUEST['cmd'] == "zakaz_submit")
{
    $json = array();

    if ($_REQUEST['fio'] == "" || $_REQUEST['fio'] == "Имя") { $json['error']['fio'] = true; }
    if ($_REQUEST['telephone'] == "") { $json['error']['telephone'] = true; }

    if(!isset($json['error'])) { $json['result'] = "ok"; } else { $json['result'] = "error"; }

    if ($json['result'] == "ok")
    {
        $subject = "Окна комфорта - обратный звонок";

        $message = "<b>".$subject."</b><br /><br />";
        if ($_REQUEST['fio'] && $_REQUEST['fio'] != '-') {
        $message .= "<b>Контактное лицо:</b> ".$_REQUEST['fio']."<br />";
        }
        $message .= "<b>Телефон:</b> ".$_REQUEST['telephone']."<br />";
        $message .= "<b>Форма:</b> Нижняя";

        sendEmail($from, $to, $subject, $message);
    }

    echo json_encode($json); exit();
}

if ($_REQUEST['cmd'] == "zakaz_submit_center")
{
    $json = array();

    if ($_REQUEST['fio'] == "" || $_REQUEST['fio'] == "Имя") { $json['error']['fio'] = true; }
    if ($_REQUEST['telephone'] == "") { $json['error']['telephone'] = true; }

    if(!isset($json['error'])) { $json['result'] = "ok"; } else { $json['result'] = "error"; }

    if ($json['result'] == "ok")
    {
        $subject = "Окна комфорта - обратный звонок";

        $message = "<b>".$subject."</b><br /><br />";
        if ($_REQUEST['fio'] && $_REQUEST['fio'] != '-') {
        $message .= "<b>Контактное лицо:</b> ".$_REQUEST['fio']."<br />";
        }
        $message .= "<b>Телефон:</b> ".$_REQUEST['telephone']."<br />";
        $message .= "<b>Форма:</b> Средняя";

        sendEmail($from, $to, $subject, $message);
    }

    echo json_encode($json); exit();
}

if ($_REQUEST['cmd'] == "zakaz_submit_side")
{
    $json = array();

    if ($_REQUEST['fio'] == "" || $_REQUEST['fio'] == "Имя") { $json['error']['fio'] = true; }
    if ($_REQUEST['telephone'] == "") { $json['error']['telephone'] = true; }

    if(!isset($json['error'])) { $json['result'] = "ok"; } else { $json['result'] = "error"; }

    if ($json['result'] == "ok")
    {
        $subject = "Окна комфорта - обратный звонок";

        $message = "<b>".$subject."</b><br /><br />";
        if ($_REQUEST['fio'] && $_REQUEST['fio'] != '-') {
        $message .= "<b>Контактное лицо:</b> ".$_REQUEST['fio']."<br />";
        }
        $message .= "<b>Телефон:</b> ".$_REQUEST['telephone']."<br />";
        $message .= "<b>Форма:</b> Боковая";

        sendEmail($from, $to, $subject, $message);
    }

    echo json_encode($json); exit();
}

function sendEmail($from, $to, $subject, $message) {
    $subject = "=?utf-8?B?" . base64_encode($subject) . "?=";
    $separator = md5(time());
    $eol = PHP_EOL;

    // main header (multipart mandatory)
    $headers = "From: ".$from.$eol;
    $headers .= "MIME-Version: 1.0".$eol;
    $headers .= "Content-Type: multipart/mixed; boundary=\"".$separator."\"".$eol.$eol;
    $headers .= "Content-Transfer-Encoding: 7bit".$eol;
    $headers .= "This is a MIME encoded message.".$eol.$eol;

    // message
    $headers .= "--".$separator.$eol;
    $headers .= "Content-Type: text/html; charset=\"utf-8\"".$eol;
    $headers .= "Content-Transfer-Encoding: 8bit".$eol.$eol;
    $headers .= $message.$eol.$eol;

    // send message
    mail($to, $subject, "", $headers);
    return $headers;
}
// Слайдер главной страницы
if ($_REQUEST['cmd'] == "slider")
{
    if(!CModule::IncludeModule("iblock")) { die(); }

    $_REQUEST['NUMBER']--;

    $arSort = array("SORT" => "ASC");
    $arFilter = array("IBLOCK_ID" => 6, "ACTIVE" => "Y");
    $arSelect = array("ID", "NAME", "PREVIEW_PICTURE", "PROPERTY_URL","DETAIL_TEXT");
    $rsElement = CIBlockElement::GetList($arSort, $arFilter, false, false, $arSelect);
    while($obElement = $rsElement->GetNextElement())
    {
        $arItem = $obElement->GetFields();
        $arResult[] = array(
            "ID" => $arItem["ID"],
            "NAME" => $arItem["NAME"],
            "PREVIEW_PICTURE" => CFile::GetFileArray($arItem["PREVIEW_PICTURE"]),
            "DETAIL_TEXT" => $arItem["DETAIL_TEXT"],
            "URL" => $arItem["PROPERTY_URL_VALUE"],
        );
    }

    if($arResult[$_REQUEST['NUMBER']]["URL"] != "")
    {
        //$arResult[$_REQUEST['NUMBER']]['DETAIL_TEXT']
        echo '<a href="'.$arResult[$_REQUEST['NUMBER']]["URL"].'"><img src="'.$arResult[$_REQUEST['NUMBER']]["PREVIEW_PICTURE"]["SRC"].'" width="907" height="300" alt="'.$arResult[$_REQUEST['NUMBER']]["NAME"].'" /></a>';
    } else {
        echo '<span class="htmlslide" style="background-image:url(\''.$arResult[$_REQUEST['NUMBER']]["PREVIEW_PICTURE"]["SRC"].'\');" alt="'.$arResult[$_REQUEST['NUMBER']]["NAME"].'">' . $arResult[$_REQUEST['NUMBER']]["DETAIL_TEXT"] . '</span>';
    }
}
?>