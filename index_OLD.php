<?
define("NOT_SHOW_BREADCRUMB", 1);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Окна комфорта");




?> 
<table border="0" cellpadding="0" cellspacing="0"> 
  <tbody> 
    <tr> <td colspan="3"> <img style="border: none;" src="/images/main_banner/tabe1.jpg"  /> </td> </tr>
   
    <tr> <td> <a style="outline: none;" href="/catalog/veka-plastic-windows/" ><img style="border: none;" src="/images/main_banner/tabe2.jpg"  /></a> </td> <td> <a href="#" id="orderCall1" ><img id="orderCall1" style="border: none;" src="/images/main_banner/tabe3.jpg"  /></a> </td> <td> <img style="border: none;" src="/images/main_banner/tabe4.jpg"  /> </td> </tr>
   
    <tr> <td colspan="3"> 
        <table border="0" cellpadding="0" cellspacing="0"> 
          <tbody> 
            <tr> <td> <a style="outline: none;" href="/service/consultation/" ><img style="border: none;" src="/images/main_banner/tabe5.jpg"  /></a> </td> <td> <a style="outline: none;" href="/service/contract/" ><img style="border: none;" src="/images/main_banner/tabe6.jpg"  /></a> </td> </tr>
           </tbody>
         </table>
       </td> </tr>
   </tbody>
 </table>
 
<p class="MsoNormal" style="margin-bottom: 0.0001pt; line-height: normal;"><b> 
    <br />
   Как заказать окна?</b></p>
 
<p align="justify" class="MsoNormal" style="margin-bottom: 0.0001pt; line-height: normal;">1.Зайдите в раздел <a href="/catalog/veka-plastic-windows/" >Пластиковые окна</a> и выберите подходящий  вариант остекления: <a href="/catalog/euroline/" > 
    <br />
   </a></p>
 
<ol> 
  <li><a href="/catalog/euroline/" ><b>Euroline</b></a> - бюджетное остекление 
    <br />
   </li>
 
  <li><b><a href="/catalog/proline/" >Proline</a></b> - наиболее популярная модель 
    <br />
   </li>
 
  <li><b><a href="/catalog/softline/" >Softline</a></b> - предложение для квартир и коттеджей, где небходимы нестандартные формы или ламинация окон 
    <br />
   </li>
 
  <li><a href="/catalog/softline/" ><b>Softline 82</b></a> - новинка! 
    <br />
   </li>
 
  <li><b><a href="/catalog/alphaline/" >Alphaline</a></b> - премиум-предложение<o:p></o:p></li>
 </ol>
 
<p align="justify" class="MsoNormal" style="margin-bottom: 0.0001pt; line-height: normal;">2. Отправьте заявку, и наш консультант перезвонит вам. Консультант рассчитает предварительную стоимость вашего заказа и ответит на все вопросы. Стоит отметить, что вы также можете узнать предварительную стоимость заказа, воспользовавшись on-line &ndash; калькулятором. Однако чтобы точно рассчитать заказ потребуется выполнить замер окон.<o:p></o:p></p>
 
<p align="justify" class="MsoNormal" style="margin-bottom: 0.0001pt; line-height: normal;">3. Воспользуйтесь услугой <a href="/service/consultation/" >замер окон</a>. Наш специалист бесплатно произведет все необходимые замеры, которые будут использованы для определения точной стоимости<span>  </span>вашего заказа. Оформить заявку на замер можно на сайте или, позвонив менеджерам-консультантам.<o:p></o:p></p>
 
<p align="justify" class="MsoNormal" style="margin-bottom: 0.0001pt; line-height: normal;">4. После проведения замеров, и расчета стоимости, осуществляется <a href="/service/contract/" >подписание договора</a>. Заключить договор вы можете у себя дома сразу после произведения замеров окон или же, подъехав к нам в офис.<o:p></o:p></p>
 
<p align="justify" class="MsoNormal" style="margin-bottom: 0.0001pt; line-height: normal;">5. Ваш заказ будет доставлен по указанному адресу в согласованное время. <a href="/service/installation-of-windows/" >Монтаж окон</a>, как правило, выполняется на следующий день после доставки. Наши специалисты установят окна в соответствии со всеми нормами и требованиями, обеспечив, таким образом, долговечность и функциональность окон. 
  <br />
 </p>
 
<p align="center" class="MsoNormal" style="margin-bottom: 0.0001pt; line-height: normal;"><b>Телефон многоканальной линии <span id="call_phone_2">(495) 545-44-70</span>!</b> 
  <br />
 </p>
 <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>