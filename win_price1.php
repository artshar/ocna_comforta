<?

// Подключаем основные классы
require_once('main_classes.php');

$kernel = & singleton('kernel');

// Загрузка модулей
$kernel->LoadModules();

function obj_to_str($val){
    return (string)$val;
}
error_reporting(E_ALL & ~E_WARNING);
$calculator = & singleton('calculator');

$price = 0;

$config_xml = to_str($_POST['config']);

//$config_xml = file_get_contents(CMS_FOLDER .'calc_input_demo.xml');

function isXML($xml){
    libxml_use_internal_errors(true);

    $doc = new DOMDocument('1.0', 'utf-8');
    $doc->loadXML($xml);

    $errors = libxml_get_errors();

    if(empty($errors)){
        return true;
    }

    $error = $errors[0];
    if($error->level < 3){
        return true;
    }

    $explodedxml = explode("r", $xml);
    $badxml = $explodedxml[($error->line)-1];

    $message = $error->message . ' at line ' . $error->line . '. Bad XML: ' . htmlentities($badxml);
    return $message;
}

if($config_xml && isXML($config_xml) === true)
{
    
    $xml = simplexml_load_string($config_xml);
    //$xml = simplexml_load_file(CMS_FOLDER .'calc_input_demo.xml');
    
    $nodes = $xml->xpath("//configuration");
    
    foreach ($nodes as $i => $node) 
    {
        $configuration_price = $calculator->getConfigurationPrice($node);
        
        list( , $amount) = each($node->xpath("./@amount"));
        $amount = (string)$amount;
        
        $price += $configuration_price * $amount;
    }


}


echo '<?xml version="1.0" encoding="UTF-8"?>';
?>
<document>
    <price><?=($price * calculator::RATE_USD)?></price>
</document>