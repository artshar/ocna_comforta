﻿<?php


// Подключаем основные классы
require_once('main_classes.php');

$kernel = & singleton('kernel');

// Загрузка модулей
$kernel->LoadModules();

function obj_to_str($val){
    return (string)$val;
}

if(to_str($_POST['name']) != '' && to_str($_POST['config']) != '' && (to_str($_POST['phone']) != '' || to_str($_POST['email']) != ''))
{
    
	$fio = htmlspecialchars(strip_tags(obj_to_str(to_str($_POST['name']))));
	$phone = htmlspecialchars(strip_tags(obj_to_str(to_str($_POST['phone']))));
	$email = htmlspecialchars(strip_tags(obj_to_str(to_str($_POST['email']))));
	$text_item = '';
	$text_message = '';
	$config_xml = to_str($_POST['config']);
    
    
    
	$order_date = date('Y-m-d H:i:s');
	$order_time = date('H.i');
    
    $DataBase = & singleton('DataBase');
    
    if(to_int($_SESSION['last_calculation_items_id']) == 0) {
        $param = array();
        $param['calculation_items_date'] = $order_date;
        $param['calculation_items_user_name'] = $fio;
        $param['calculation_items_user_phone'] = $phone;
        $param['calculation_items_user_email'] = $email;
        $param['calculation_items_xml'] = obj_to_str($config_xml);
        $param['calculation_items_accepted_date'] = '0000-00-00 00:00:00';
        $param['calculation_items_accepted_user'] = 0;
        $DataBase->Insert('calculation_items', $param);	
        
        $_SESSION['last_calculation_items_id'] = $DataBase->GetLastInsertId();
    }
    
    $order_id = to_int($_SESSION['last_calculation_items_id']);
    
    //$config_xml = iconv('windows-1251', 'utf-8', $config_xml);
    $xml = simplexml_load_string($config_xml);
    $nodes = $xml->xpath("//configuration");
	foreach ($nodes as $i => $node) {
	   list( , $configuration) = each($node->xpath("./@id"));
       $configuration = (string)$configuration;
       $text_item .= '<p><strong><i style="font-size: 14px;color: #ff0000;">Конструкция №'. ($i + 1) .'</i></strong><br />';
       
       
       if($configuration == 'window') {
        
            $prm = array();
            $prm[] = 'окно';
            //Типы домов, серии, материалы - ШАГ 1
    	    list( , $build_type) = each($node->xpath("./build_type/title"));
            $prm[] = obj_to_str($build_type);
       
    	    list( , $build_sub_type) = each($node->xpath("./build_sub_type/title"));
            $build_sub_type = obj_to_str($build_sub_type);
            if(!empty($build_sub_type)) {
                $prm[] = 'постройка - '. $build_sub_type;
            }else {
                //$prm[] = 'постройка - не указана';
            }
            
            $text_item .= ''. join(', ', $prm) .';<br />';
            
                $prm = array();
            // Конфигурация
    	    list( , $door_position) = each($node->xpath("./frame_type/door_position/title"));
            $door_position = obj_to_str($door_position);
            if($door_position) {
        	    list( , $frame_type) = each($node->xpath("./frame_type/title"));
                $prm[] = obj_to_str($frame_type);
                $prm[] = 'дверь - '. $door_position;
    	        list( , $door_opening) = each($node->xpath("./frame_type/door_opening/title"));
                $prm[] = obj_to_str($door_opening);
                
                $leaf_prm = array();   
                $leafs = $node->xpath("./frame_type/leafs/leaf");
                foreach ($leafs as $i => $leaf) {
                    list( , $leaf_title) = each($leaf->xpath("./title"));
                    $leaf_prm[] = obj_to_str($leaf_title);
            	}       
                $leaf_frame = '';  
                if(count($leaf_prm)) {
                    $leaf_frame = ' ('. join(', ', $leaf_prm) .')';
                }
    	        list( , $door_type) = each($node->xpath("./frame_type/door_type/title"));
                $prm[] = obj_to_str($door_type) . $leaf_frame;
            }else {
                $leaf_prm = array();   
                $leafs = $node->xpath("./frame_type/leafs/leaf");
                foreach ($leafs as $i => $leaf) {
                    list( , $leaf_title) = each($leaf->xpath("./title"));
                    $leaf_prm[] = obj_to_str($leaf_title);
            	}       
                $leaf_frame = '';  
                if(count($leaf_prm)) {
                    $leaf_frame = ' ('. join(', ', $leaf_prm) .')';
                }
        	    list( , $frame_type) = each($node->xpath("./frame_type/title"));
                $prm[] = obj_to_str($frame_type) . $leaf_frame;
                
        	    list( , $framuga_type) = each($node->xpath("./frame_type/framuga/title"));
                $framuga_type = obj_to_str($framuga_type);
                if($framuga_type && $framuga_type != 'Нет') {
                    $prm[] = 'с фрамугой, тип открывания - '. $framuga_type;
                }
                
            }
            
            
            
            $text_item .= '<strong>Конфигурация:</strong> '. join(', ', $prm) .';<br />';
            
            // Размеры
            $prm = array();
    	    list( , $size) = each($node->xpath("./size/title"));
            $prm[] = obj_to_str($size);
    	    list( , $oriel) = each($node->xpath("./oriel/title"));
            $prm[] = 'изгибы - '. obj_to_str($oriel);
            $text_item .= '<strong>Размеры:</strong> '. join(', ', $prm) .';<br />';
            
            // Решение
            $prm = array();
    	    list( , $base) = each($node->xpath("./base/title"));
            $base = obj_to_str($base);
            if(!empty($base)) {
                $prm[] = $base;
                    
                if($base != 'Эконом' && $base != 'Классик') {
            	    list( , $base_laminated) = each($node->xpath("./base/laminated/title"));
                    $base_laminated = obj_to_str($base_laminated);
                    if($base_laminated) {
                        $prm[] = 'ламинированные '. $base_laminated;
                    }
            	    list( , $base_color) = each($node->xpath("./base/color/title"));
                    $base_color = obj_to_str($base_color);
                    if($base_color) {
                        $prm[] = 'цвет - '. $base_color;
                    }
                }
                $text_item .= '<strong>Решение:</strong> '. join(', ', $prm) .';<br />';
            }else {
                $text_item .= '<strong>Решение:</strong> не указано;<br />';
            }
            
            // Подоконник
            $prm = array();
    	    list( , $sill) = each($node->xpath("./sill/title"));
            $sill = obj_to_str($sill);
            if($sill) {
                $prm[] = $sill;
        	    list( , $sill_size) = each($node->xpath("./sill/size"));
                $sill_size = obj_to_str($sill_size);
                if($sill_size) {
                    $prm[] = $sill_size;
                }
        	    list( , $sill_color) = each($node->xpath("./sill/color"));
                $sill_color = obj_to_str($sill_color);
                if($sill_color) {
                    $prm[] = $sill_color;
                }       
            }
            if(count($prm)) {
                $text_item .= '<strong>Подоконник:</strong> '. join(', ', $prm) .';<br />';
            }
            
            // Откосы
            $prm = array();
    	    list( , $slopes) = each($node->xpath("./slopes/title"));
            $slopes = obj_to_str($slopes);
            if($slopes) {
                $prm[] = $slopes;
        	    list( , $slopes_size) = each($node->xpath("./slopes/size"));
                $slopes_size = obj_to_str($slopes_size);
                if($slopes_size) {
                    $prm[] = $slopes_size;
                }
        	    list( , $slopes_color) = each($node->xpath("./slopes/color"));
                $slopes_color = obj_to_str($slopes_color);
                if($slopes_color) {
                    $prm[] = $slopes_color;
                }
            }   
            if(count($prm)) {
                $text_item .= '<strong>Откосы:</strong> '. join(', ', $prm) .';<br />';
            }
            
            // Отлив
            $prm = array();
    	    list( , $ebb) = each($node->xpath("./ebb/title"));
                $ebb = obj_to_str($ebb);
                if($ebb) {
                    $prm[] = $ebb;
                }
    	    list( , $ebb_size) = each($node->xpath("./ebb/size"));
                $ebb_size = obj_to_str($ebb_size);
                if($ebb_size) {
                    $prm[] = $ebb_size;
                }
    	    list( , $ebb_color) = each($node->xpath("./ebb/color"));
                $ebb_color = obj_to_str($ebb_color);
                if($ebb_color) {
                    $prm[] = $ebb_color;
                }             
            if(count($prm)) {
                $text_item .= '<strong>Отлив:</strong> '. join(', ', $prm) .';<br />';
            }     
            
            // Опции
            $prm = array();
            $options = $node->xpath("./options/option");
            foreach ($options as $i => $option) {
                list( , $option_title) = each($option->xpath("./title"));
                $prm[] = obj_to_str($option_title);
        	}    
            if(count($prm)) {
                $text_item .= '<strong>Опции:</strong> '. join(', ', $prm) .';<br />';
            }else {
                $text_item .= '<strong>Опции:</strong> нет;<br />';
            }
            
            
            // Доставка и монтаж
            $prm = array();
    	    list( , $delivery) = each($node->xpath("./delivery/title"));
            $delivery = obj_to_str($delivery);
            if($delivery) {
                $prm[] = $delivery;
        	    list( , $rise) = each($node->xpath("./rise/title"));
                $rise = obj_to_str($rise);
                if(!empty($rise)) {
                    $prm[] = $rise;
                }
            }else {
                $prm[] = 'самовывоз';
            }
        	list( , $assembly) = each($node->xpath("./assembly/title"));
            $assembly = obj_to_str($assembly);
            if(!empty($assembly)) {
                $prm[] = $assembly;
            }else {
                $prm[] = 'монтаж не требуется';
            }
            
            $text_item .= '<strong>Доставка и монтаж:</strong> '. join(', ', $prm) .';<br />';
            
       }else {
            $prm = array();
            $prm[] = 'балкон или лоджия';
    	    list( , $material) = each($node->xpath("./material/title"));
            $prm[] = obj_to_str($material);
            
            //Типы домов, серии, материалы - ШАГ 1
    	    list( , $build_type) = each($node->xpath("./build_type/title"));
            $prm[] = obj_to_str($build_type);
       
    	    list( , $build_sub_type) = each($node->xpath("./build_sub_type/title"));
            $build_sub_type = obj_to_str($build_sub_type);
            if(!empty($build_sub_type)) {
                $prm[] = 'постройка - '. $build_sub_type;
            }else {
                //$prm[] = 'постройка - не указана';
            }
            
            $text_item .= ''. join(', ', $prm) .';<br />';
            
            // Размеры и парапет
            $prm = array();
    	    list( , $size) = each($node->xpath("./size/title"));
            $prm[] = obj_to_str($size);
    	    list( , $oriel) = each($node->xpath("./oriel/title"));
            $prm[] = 'изгибы - '. obj_to_str($oriel);
    	    list( , $parapet) = each($node->xpath("./parapet/title"));
            $prm[] = 'материал парапета - '. obj_to_str($parapet);
                   
    	    list( , $about) = each($node->xpath("./about/title"));
            $about = obj_to_str($about);
            if($about) {
                $prm[] = $about;
            }
            $text_item .= '<strong>Размеры и тип парапета:</strong> '. join(', ', $prm) .';<br />';
            
            // Тип створки
            $prm = array();
                $leaf_prm = array();   
                $leafs = $node->xpath("./frame_type/leafs/leaf");
                foreach ($leafs as $i => $leaf) {
                    list( , $leaf_title) = each($leaf->xpath("./title"));
                    $leaf_prm[] = obj_to_str($leaf_title);
            	}       
                $leaf_frame = '';  
                if(count($leaf_prm)) {
                    $leaf_frame = ' ('. join(', ', $leaf_prm) .')';
                }
        	    list( , $frame_type) = each($node->xpath("./frame_type/title"));
                $prm[] = obj_to_str($frame_type) . $leaf_frame;
                   
            $text_item .= '<strong>Конфигурация:</strong> '. join(', ', $prm) .';<br />';
            
            // Решение
            $prm = array();
    	    list( , $base) = each($node->xpath("./base/title"));
            $base = obj_to_str($base);
            if(!empty($base)) {
                $prm[] = $base;
                    
                if($base != 'Эконом' && $base != 'Классик') {
            	    list( , $base_laminated) = each($node->xpath("./base/laminated/title"));
                    $base_laminated = obj_to_str($base_laminated);
                    if($base_laminated) {
                        $prm[] = 'ламинированные '. $base_laminated;
                    }
            	    list( , $base_color) = each($node->xpath("./base/color/title"));
                    $base_color = obj_to_str($base_color);
                    if($base_color) {
                        $prm[] = 'цвет - '. $base_color;
                    }
                }
                $text_item .= '<strong>Решение:</strong> '. join(', ', $prm) .';<br />';
            }else {
                $text_item .= '<strong>Решение:</strong> не указано;<br />';
            }
            // Подоконник
            $prm = array();
    	    list( , $sill) = each($node->xpath("./sill/title"));
            $sill = obj_to_str($sill);
            if($sill) {
                $prm[] = $sill;
        	    list( , $sill_size) = each($node->xpath("./sill/size"));
                $sill_size = obj_to_str($sill_size);
                if($sill_size) {
                    $prm[] = $sill_size;
                }
        	    list( , $sill_color) = each($node->xpath("./sill/color"));
                $sill_color = obj_to_str($sill_color);
                if($sill_color) {
                    $prm[] = $sill_color;
                }       
            }
            if(count($prm)) {
                $text_item .= '<strong>Подоконник:</strong> '. join(', ', $prm) .';<br />';
            }
            
            // Опции
            $prm = array();
            $options = $node->xpath("./options/option");
            foreach ($options as $i => $option) {
                list( , $option_title) = each($option->xpath("./title"));
                $prm[] = obj_to_str($option_title);
        	}    
            if(count($prm)) {
                $text_item .= '<strong>Опции:</strong> '. join(', ', $prm) .';<br />';
            }else {
                $text_item .= '<strong>Опции:</strong> нет;<br />';
            }
            
            // Дополнительно
            $prm = array();
            // Москитная сетка
    	    list( , $mosquito_net) = each($node->xpath("./mosquito_net/title"));
            $mosquito_net = obj_to_str($mosquito_net);
            if($mosquito_net) {
                $prm[] = $mosquito_net;
            }
            
                   
            if(count($prm)) {
                $text_item .= '<strong>Дополнительно:</strong> '. join(', ', $prm) .';<br />';
            }
            
            
            // Доставка и монтаж
            $prm = array();
    	    list( , $delivery) = each($node->xpath("./delivery/title"));
            $delivery = obj_to_str($delivery);
            if($delivery) {
                $prm[] = $delivery;
        	    list( , $rise) = each($node->xpath("./rise/title"));
                $rise = obj_to_str($rise);
                if(!empty($rise)) {
                    $prm[] = $rise;
                }
            }else {
                $prm[] = 'самовывоз';
            }
        	list( , $assembly) = each($node->xpath("./assembly/title"));
            $assembly = obj_to_str($assembly);
            if(!empty($assembly)) {
                $prm[] = $assembly;
            }else {
                $prm[] = 'монтаж не требуется';
            }
            $text_item .= '<strong>Доставка и монтаж:</strong> '. join(', ', $prm) .';<br />';
            
       }
       
       
       
       
       $text_item .= '</p>';
	   $text_message .= '</p>';
    }
    
			// Текст сообщения
	$param = array();
			
    ?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>ON-Line калькулятор - Окна Комфорта</title>
    <meta name="description" content=""/>
    <meta name="keywords" content=""/>
    
    <meta http-equiv="Content-Language" content="ru"/>
    <meta content="text/html; charset=UTF-8" http-equiv="Content-Type"/>
    
    <link rel="stylesheet" type="text/css" href="/css/order.css" />

    <!-- Google Analytics -->
    <script type="text/javascript">
        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-17683530-3']);
        _gaq.push(['_trackPageview']);
        (function() {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();
    </script>
    <!-- /Google Analytics -->

</head>
<body>
<!-- Yandex.Metrika counter -->
<script src="//mc.yandex.ru/metrika/watch.js" type="text/javascript"></script>
<div style="display:none;"><script type="text/javascript">
try { var yaCounter1390649 = new Ya.Metrika({id:1390649,
          clickmap:true,
          trackLinks:true});}
catch(e) { }
</script></div>
<noscript><div><img src="//mc.yandex.ru/watch/1390649" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
<div class="content">
<?
ob_start();
?>
<table style="width:650px;">
<tr>
    <td>
    
        <img src="http://www.oknakomforta.com/img/order_head.jpg" alt="" width="700" height="104" border="0"/>
        
        <p>Уважаемый(ая) <strong><?=$fio?></strong>!</p>
        <p>Спасибо,  что воспользовались нашей услугой «ON-Line  калькулятор».<br />
        Ваша  заявка под номером <strong>№  <strong class="red"><?=$order_id?></strong></strong>  принята на расчёт специалистом компании <strong class="red">в <?=$order_time?></strong>.</p>
        <p>Ваш заказ будет рассчитан в течении дня.<sup style="color:#ff0000;">*</sup><br />
        <? if($phone != ''): ?>
        Чтобы сообщить стоимость заказа с Вами свяжется наш менеджер-консультант по  указанному телефону:  <strong class="red"><?=$phone?></strong>.<br />
        <? elseif($email != ''): ?>
        Чтобы сообщить стоимость заказа с Вами свяжется наш менеджер-консультант по  указанному адресу электронной почты:  <strong class="red"><?=$email?></strong>.<br />
        <? endif; ?>
        Самостоятельно узнать стоимость заказа Вы можете по телефону (495) 38-006-38.<br />
        При обращении по телефону назовите менеджеру номер вашей заявки:: <strong>№  <strong class="red"><?=$order_id?></strong></strong>.</p>
        <p><strong>Ваш  заказ:</strong></p>
        <?=$text_item?>
        <p><sup style="color:#ff0000;">*</sup> Если Вы отправили заявку после 19-00 текущего дня, стоимость заказа будет  сообщена на следующий день.</i></p>
        <p><strong>Благодарим за ожидание.</strong></p>
<?
$mail_message_1 = ob_get_clean();
echo $mail_message_1;
?>
        <div style="margin: 10px 0 20px 0;text-align: center;" class="no_print">
            <a href="#" onclick="window.print(); return false;"><img src="http://www.oknakomforta.com/img/print_order_button.png" alt="" border="0"/></a>
        </div>
<?
ob_start();
?>
        <div style="border-bottom: 1px solid #cccccc;margin: 10px 0 20px 0;"></div>
        <h4 style="text-align: center;font-size: 15px;font-weight: bold;">Сегодня действуют акции:</h4>
        <table width="100%">
<tr>
	<td width="50%" style="vertical-align: middle;text-align: center;font-size: 14px;font-weight: bold;">Теплые окна со скидкой 45%!</td>
	<td width="50%" style="vertical-align: middle;text-align: center;font-size: 14px;font-weight: bold;border-left: 1px solid #cccccc;">
    Даже если у вас нет достающей суммы, Вы уже можете купить окна ПВХ в рассрочку!
    </td>
</tr>
<tr>
	<td style="vertical-align: middle;text-align: center;"><img src="http://www.oknakomforta.com/img3/actsii/osennyaya_akciya.jpg" alt="" border="0"/></td>
	<td style="vertical-align: middle;text-align: center;border-left: 1px solid #cccccc;"><img src="http://www.oknakomforta.com/img3/actsii/rassrochka.jpg" alt="" border="0"/></td>
</tr>
</table>
        <p style="margin-top: 20px;">
        Узнайте подробности у Вашего менеджера-консультанта.<br />
        (495) 38-006-38<br />
        С уважением, <br />
        команда «Окна Комфорта».
        </p>
    </td>
</tr>
</table>
<?
$mail_message_2 = ob_get_clean();
echo $mail_message_2;
$mail_message = $mail_message_1 . $mail_message_2;
?>
</div>
<!-- Google Code for Калькулятор Conversion Page -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 1013687388;
var google_conversion_language = "en";
var google_conversion_format = "3";
var google_conversion_color = "ffffff";
var google_conversion_label = "bthFCKTKjAMQ3Miu4wM";
var google_conversion_value = 0;
/* ]]> */
</script>
<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="http://www.googleadservices.com/pagead/conversion/1013687388/?label=bthFCKTKjAMQ3Miu4wM&guid=ON&script=0"/>
</div>
</noscript>
</body>
</html>
    <?
    
    // Отправка письма администратору
	$message = "<p><strong>На сайте, была заполнена заявка на расчет стоимости:</strong></p>";
	$message .= "<p>Имя: ". $fio ."<br />";
	$message .= "Телефон: ". $phone ."<br />";
	$message .= "E-mail: ". $email ."<br />";
	$DateClass = & singleton('DateClass');
	$message .= "Дата: ". $DateClass->datetime_format($order_date) ."<br />";
	$message .= "IP-адрес: ". to_str($_SERVER['REMOTE_ADDR']) ."<br />";
	$message .= "Для просмотра заявки перейдите в систему управления <a href=\"http://www.oknakomforta.com/admin/order_calculation/order_calculation.php\">http://www.oknakomforta.com/admin/order_calculation/order_calculation.php</a>";
	$subject = 'Заявка на расчет стоимости';

    $param = array();
    $param['sender_name'] = DEFAULT_SENDER_NAME;
    // EMAIL_TO_ZAMER, EMAIL_TO
	$kernel->SendMailWithFile(EMAIL_TO_CALKULATION, 'marketing@oknakomforta.com', $subject, $message, array(), 'text/html', $param);
    if($email != '') {
        // Отправка письма пользователю
    	$kernel->SendMailWithFile($email, 'marketing@oknakomforta.com', $subject, $mail_message, array(), 'text/html', $param);
    }
    
    // отправка SMS
    
    require_once "unisender_api.php";
    
    $my_api_key = "2UnzJXytvXqCygFBhkp0w4FACYvfLi3tT";
    $uni_api = new UniSenderApi($my_api_key);
    
    $phone = preg_replace('/[^0-9]/u', '', $phone);    
    if($phone{0} != 7) {
        $phone = '7'. substr($phone, 1);
    }
    
    if($phone != '') 
    {
        $sms_param = array();
        $sms_param['phone'] = $phone;
        $sms_param['sender'] = 'OknaKomfort';
        $sms_param['text'] = 'Окна Комфорта: заявка №'.$order_id.' принята к расчету! тел.: 8(495)38-006-38';
        $r = $uni_api->sendSms($sms_param);        
    }
    
    
    
}else {
    echo '<h1>Не корректные парамеры запроса</h1>';
}




?>