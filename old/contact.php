<?php

session_name("fancyform");
session_start();


$_SESSION['n1'] = rand(1,20);
$_SESSION['n2'] = rand(1,20);
$_SESSION['expect'] = $_SESSION['n1']+$_SESSION['n2'];


$str='';
if($_SESSION['errStr'])
{
	$str='<div class="error">'.$_SESSION['errStr'].'</div>';
	unset($_SESSION['errStr']);
}

$success='';
if($_SESSION['sent'])
{
	$success='<h1>Thank you!</h1>';

	$css='<style type="text/css">#contact-form{display:none;}</style>';

	unset($_SESSION['sent']);
}
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>��������</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" type="text/css" href="style.css" />
<link rel="stylesheet" type="text/css" href="css/style.nav.css" />
<link href="docs/assets/css/bootstrap.css" rel="stylesheet">
<link href="docs/assets/css/bootstrap-responsive.css" rel="stylesheet">
<link href="docs/assets/css/docs.css" rel="stylesheet">
<link href="docs/assets/js/google-code-prettify/prettify.css" rel="stylesheet">
<script type="text/javascript" src="docs/assets/js/jquery.js"></script>
<script src="docs/assets/js/bootstrap.min.js"></script>
<script src="docs/assets/js/bootstrap-transition.js"></script>
<script type="text/javascript" src="js/jquery.easing.1.3.js"></script>
<script type="text/javascript" src="js/sprite.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1251" />
<link rel="stylesheet" type="text/css" href="jqtransformplugin/jqtransform.css" />
<link rel="stylesheet" type="text/css" href="formValidator/validationEngine.jquery.css" />
<link rel="stylesheet" type="text/css" href="css/form.css" />
<?=$css?>
<!--<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"></script>-->
<script type="text/javascript" src="jqtransformplugin/jquery.jqtransform.js"></script>
<script type="text/javascript" src="formValidator/jquery.validationEngine.js"></script>

<script type="text/javascript" src="js/script.form.js"></script>
</head>

<body data-spy="scroll" data-target=".bs-docs-sidebar" data-twttr-rendered="true">

	<header>
   	    <div class="container" style="margin-bottom: 40px; margin-top:40px">
			<div class="row">
				<div class="span3" id="logo">
					<div><a href="/"><img src="images/okna_klassik.jpg" alt="������� &bdquo;���� �������&ldquo;" title="���� �������"></a></div>
				</div>
				<div class="span9">
					<ul id="navigation">
						<li><a class="about" href="about.html"></a></li>
						<li><a class="production" href="production.html"></a></li>
						<li><a class="services" href="services.html"></a></li>
						<li><a class="portfolio" href="portfolio.html"></a></li>
					    <li><a class="contact" href="contact.html"></a></li>
					</ul>
				</div>
			</div>
		</div>
	</header>
	<div class="container">
		<div class="row">
			<div class="span3 bs-docs-sidebar">
			</div>
			<div class="span7" style="margin-left:55px; width:630px">
				<h1>��������</h1>
				<div>���� ��� �������������� ��������� ��� ������ �������� ����� ������� �������� ������ � ��������� ��� �����. �� �������� � ���� � ���������� �����.</div>
				<br />

				<!-- FORM START -->
				<div id="main-container">
					<div id="form-container">
				    <form id="contact-form" name="contact-form" method="post" action="submit.php">
				      <table width="100%" border="0" cellspacing="0" cellpadding="5">
				        <tr>
				          <td width="15%"><label for="name">���</label></td>
				          <td width="70%"><input type="text" class="validate[required,custom[onlyLetter]]" name="name" id="name" value="<?=$_SESSION['post']['name']?>" /></td>
				          <td width="15%" id="errOffset">&nbsp;</td>
				        </tr>
				        <tr>
				          <td><label for="email">Email</label></td>
				          <td><input type="text" class="validate[required,custom[email]]" name="email" id="email" value="<?=$_SESSION['post']['email']?>" /></td>
				          <td>&nbsp;</td>
				        </tr>
				        <tr>
				          <td><label for="subject">����</label></td>
				          <td><select name="subject" id="subject">
				            <option value="" selected="selected"> - �������� -</option>
				            <option value="Question">������</option>
				            <option value="Business proposal">������� �����������</option>
				            <option value="Advertisement">�������</option>
				            <option value="Complaint">������</option>
				          </select>          </td>
				          <td>&nbsp;</td>
				        </tr>
				        <tr>
				          <td valign="top"><label for="message">���������</label></td>
				          <td><textarea name="message" id="message" class="validate[required]" cols="35" rows="5"><?=$_SESSION['post']['message']?></textarea></td>
				          <td valign="top">&nbsp;</td>
				        </tr>
				        <tr>
				          <td><label for="captcha"><?=$_SESSION['n1']?> + <?=$_SESSION['n2']?> =</label></td>
				          <td><input type="text" class="validate[required,custom[onlyNumber]]" name="captcha" id="captcha" /></td>
				          <td valign="top">&nbsp;</td>
				        </tr>
				        <tr>
				          <td valign="top">&nbsp;</td>
				          <td colspan="2"><input type="submit" name="button" id="button" value="���������" />
				          <input type="reset" name="button2" id="button2" value="�����" />

				          <?=$str?>          <img id="loading" src="img/ajax-load.gif" width="16" height="16" alt="loading" /></td>
				        </tr>
				      </table>
				      </form>
				      <?=$success?>
				    </div>
					<div class="tutorial-info">

				</div>

				<!-- FORM END -->
			</div>
		</div>
	</div>
	<footer class="footer">
		<div class="container">
			<p>2012 &copy; ��� ����� ��������. <a href="/">���� �������</a></p>
		</div>
	</footer>
	<script type="text/javascript" src="http://platform.twitter.com/widgets.js"></script>
    <script src="docs/assets/js/google-code-prettify/prettify.js"></script>
    <script src="docs/assets/js/bootstrap-transition.js"></script>
    <script src="docs/assets/js/bootstrap-alert.js"></script>
    <script src="docs/assets/js/bootstrap-modal.js"></script>
    <script src="docs/assets/js/bootstrap-dropdown.js"></script>
    <script src="docs/assets/js/bootstrap-scrollspy.js"></script>
    <script src="docs/assets/js/bootstrap-tab.js"></script>
    <script src="docs/assets/js/bootstrap-tooltip.js"></script>
    <script src="docs/assets/js/bootstrap-popover.js"></script>
    <script src="docs/assets/js/bootstrap-button.js"></script>
    <script src="docs/assets/js/bootstrap-collapse.js"></script>
    <script src="docs/assets/js/bootstrap-carousel.js"></script>
    <script src="docs/assets/js/bootstrap-typeahead.js"></script>
    <script src="docs/assets/js/bootstrap-affix.js"></script>
    <script src="docs/assets/js/application.js"></script>

</body></html>