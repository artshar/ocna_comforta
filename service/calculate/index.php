<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Расчет стоимости");
?>
<h1>Онлайн-калькулятор</h1>
<div id="calc">
  <div class="row row1">
      
      <div class="toggle type">
        <div class="center">
          <div class="option" data-alias="plastic">Пластиковые окна</div>
          <div class="option" data-alias="balcony">Лоджия/балкон</div>
          <div class="clear"></div>
        </div>
      </div>
  </div>
  <div class="plastic variants">
    <div class="row row2">
      <h2 class="header">Куда планируете устанавливать окна</h2>
      <div class="toggle icon">
        <div class="center">
			<div class="option" data-alias="panel"><span class="icon icon2"></span><span class="text">В квартиру в панельном доме</span></div>
          <div class="option" data-alias="brick"><span class="icon icon1"></span><span class="text">В квартиру в кирпичном доме</span></div>
          <div class="option" data-alias="stalin"><span class="icon icon3"></span><span class="text">В квартиру в доме-сталинке</span></div>
          <div class="option" data-alias="private"><span class="icon icon4"></span><span class="text">В частный<br>дом</span></div>
          <div class="option" data-alias="office"><span class="icon icon5"></span><span class="text">В офис</span></div>
          <div class="option" data-alias="cottage"><span class="icon icon6"></span><span class="text">На дачу</span></div>
          <div class="option" data-alias="misc"><span class="icon icon7"></span><span class="text">Другое</span></div>
          <div class="clear"></div>
        </div>
      </div>
      <div class="clear"></div>
    </div>
    <div class="repeat">
    <div class="row row4">
      <h2 class="header">Количество створок в окне:</h2>
      <div class="toggle icon">
          <div class="center">
            <div class="option" data-alias="one"><span class="icon icon8"></span><span class="text">Одна</span></div>
            <div class="option" data-alias="two"><span class="icon icon9"></span><span class="text">Две</span></div>
            <div class="option" data-alias="three"><span class="icon icon10"></span><span class="text">Три</span></div>
            <div class="option" data-alias="block"><span class="icon icon11"></span><span class="text">Балконный блок</span></div>
            <div class="clear"></div>
          </div>
        </div>
    </div>
    <div class="row row3">
      <h2 class="header">Укажите размеры необходимого окна, если вы его знаете:</h2>
      <div class="column first">
        <label for="calc_width">Ширина,<br>см</label>
        <input type="text" class="field width" />
      </div>
      <div class="column second">
        <label for="calc_height">Высота,<br>см</label>
        <input type="text" class="field height" />
      </div>  
      <div class="clear"></div>
    </div>
    </div>
    <div class="row row_add">
      <a class="add" href="./">+ <span>Еще одно окно</span></a>
      <a class="remove" href="./">- <span>Удалить последнее</span></a>
    </div>
    <div class="row row3-5">
      <h2 class="header">Модель окон:</h2>
      <div class="models">
        <input type="radio" name="window_model" id="model_euroline" value="Euroline"><label for="model_euroline">Euroline</label>
		<div class="clear"></div>
		<span><input type="radio" name="window_model" id="model_proline" value="Proline"><label for="model_proline">Proline</label>
		<img src="/images/hit.png" style="margin: 3px 0px 0px -10px;"></span>
        <div class="clear"></div>
        <input type="radio" name="window_model" id="model_softline" value="Softline"><label for="model_softline">Softline</label>
        <div class="clear"></div>
        <input type="radio" name="window_model" id="model_softline82" value="Softline82"><label for="model_softline82">Softline82</label>
        <div class="clear"></div>
        <input type="radio" name="window_model" id="model_alphaline" value="Alphaline"><label for="model_alphaline">Alphaline</label>
               
      </div>
      <div class="image">
        <img class="proline" src="/images/forms/calc/okna/proline.png" border="0" />
        <img class="softline" src="/images/forms/calc/okna/softline.png" border="0" />
        <img class="softline82" src="/images/forms/calc/okna/softline82.png" border="0" />
        <img class="alphaline" src="/images/forms/calc/okna/alphaline.png" border="0" />
        <img class="euroline" src="/images/forms/calc/okna/euroline.png" border="0" />
      </div>
      <div class="description">
          <h3>Рекомендуем для остекления:</h3>
          <p class="proline">
          Квартир любого типа и серии домов; балконов и лоджий (с бетонным или кирпичным парапетом); загородных домов.
          </p>
          <p class="softline">
          Квартир как цветными так и классическими белыми окнами; загородных домов и коттеджей с оригинальным дизайном и окнами нестандартной формы и цвета; утеплённых террас и веранд в частных домах; лоджий и балконов с бетонным или кирпичным парапетом с возможностью дальнейшего утепления.
          </p>
          <p class="softline82">
          Эта профильная система имеет оптимальную ширину 82 мм, позволяющую использовать энергоэффективные окна как в новых постройках, так и при замене старых окон в существующих жилых зданиях.
          </p>
          <p class="alphaline">
          Квартир с большими оконными проёмами (в частности квартир сталинской постройки); многоуровневых квартир, коттеджей и загородных домов; архитектурных памятников, требующих установки окон, соответствующих стилю постройки. В целях подчёркивания изысканного необычного дизайна и достижения технического совершенства стоит установить окна класса «Люкс».
          </p>
          <p class="euroline">
          Небольших дачных домиков, балконов и лоджий с узким парапетом; для остекления квартир с хорошим отоплением; торговых павильонов производственных помещений.
          </p>
      </div>
    </div>
    <div class="row row5">
  <!--     <div class="column first">
        <h2 class="header">Тип стеклопакета:</h2>
        <div class="toggle icon">
          <div class="center">
            <div class="option" data-alias="one"><span class="icon"></span><span class="text">Однокамерный</span></div>
            <div class="option" data-alias="two"><span class="icon"></span><span class="text">Двухкамерный</span></div>
            <div class="clear"></div>
          </div>
        </div>
      </div>
      <div class="column second"> -->
        <h2 class="header">Дополнительные функции окна:</h2>
        <div class="toggle icon" data-multiple="true">
          <div class="center">
            <div class="option" data-alias="energo"><span class="icon icon12"></span><span class="text">Энергосберегающий стеклопакет</span></div>
            <div class="option" data-alias="sun"><span class="icon icon13"></span><span class="text">Стеклопакет с солнцезащитой</span></div>
            <div class="option" data-alias="noise"><span class="icon icon14"></span><span class="text">Дополнительная шумоизоляция</span></div>
            <div class="option"><span class="icon icon15"></span><span class="text">Москитная сетка</span></div>
            <div class="clear"></div>
          </div>
        </div>
  <!--     </div> -->
  <!--     <div class="clear"></div> -->
    </div>
    <div class="row row7">
      <h2 class="header">Декорировать окна:</h2>
      <div class="toggle icon" data-multiple="true">
          <div class="center">
            <div class="option" data-alias="stained"><span class="icon icon16"></span><span class="text">Декоративный витраж</span></div>
            <div class="option" data-alias="colorgrip"><span class="icon icon17"></span><span class="text">Цветные ручки</span></div>
            <div class="option" data-alias="specialform"><span class="icon icon18"></span><span class="text">Нестандартная форма</span></div>
            <div class="option" data-alias="layout"><span class="icon icon19"></span><span class="text">Раскладки в стеклопакете</span></div>
            <div class="option" data-alias="colorframe"><span class="icon icon20"></span><span class="text">Цветная рама</span></div>
            <div class="clear"></div>
          </div>
        </div>
    </div>
    <div class="row row6">
      <h2 class="header">Требуются ли монтажные работы:</h2>
      <div class="toggle icon">
          <div class="center">
            <div class="option" data-alias="full"><span class="icon icon21"></span><span class="text">Монтаж и демонтаж</span></div>
            <div class="option" data-alias="half"><span class="icon icon22"></span><span class="text">Только монтаж</span></div>
            <div class="option" data-alias="zero"><span class="icon icon23"></span><span class="text">Без монтажа</span></div>
            <div class="clear"></div>
          </div>
        </div>
    </div>
  </div>
  <div class="balcony variants">
    <div class="row row2b">
      <div class="column first">
                <h2 class="header">У вас:</h2>
                <div class="toggle icon">
                    <div class="center">
                      <div class="option" data-alias="full"><span class="icon iconb1"></span><span class="text">Лоджия</span></div>
                      <div class="option" data-alias="half"><span class="icon iconb2"></span><span class="text">Балкон</span></div>
                      <div class="clear"></div>
                    </div>
                  </div>
      </div>
      <div class="column second">
                <h2 class="header">Выберите тип остекления:</h2>
                <div class="toggle icon">
                    <div class="center">
                      <div class="option" data-alias="full"><span class="icon iconb3"></span><span class="text">Холодное остекление</span></div>
                      <div class="option" data-alias="half"><span class="icon iconb4"></span><span class="text">Теплое остекление</span></div>
                      <div class="clear"></div>
                    </div>
                  </div>
      </div>
      <div class="clear"></div>
    </div>
    <div class="row row3b">
      <div class="column first">
            <h2 class="header">Внутренняя отделка:</h2>
            <div class="toggle icon">
                <div class="center">
                  <div class="option" data-alias="full"><span class="icon iconb5"></span><span class="text">Нужна</span></div>
                  <div class="option" data-alias="half"><span class="icon iconb6"></span><span class="text">Не нужна</span></div>
                  <div class="clear"></div>
                </div>
            </div>
      </div>
      <div class="column second">
        <h2 class="header">Крыша для балкона<br>(если последний этаж):</h2>
        <div class="toggle icon">
            <div class="center">
              <div class="option" data-alias="full"><span class="icon iconb7"></span><span class="text">Нужна</span></div>
              <div class="option" data-alias="half"><span class="icon iconb8"></span><span class="text">Не нужна</span></div>
              <div class="clear"></div>
            </div>
        </div>
      </div>
      <div class="clear"></div>
   </div>
   <div class="row row4b">
      <h2 class="header">Укажите размеры оконного блока, если вы их знаете:</h2>
      <label for="calc_flapcount">Необходимое<br>количество<br>створок</label>
      <input type="number" value="1" id="calc_flapcount" class="field flapcount">
      <div class="clear" style="height:20px"></div>
      <label for="calc_balconheight">Высота<br>балконнного<br>блока, см</label>
      <input type="text" id="calc_balconheight" class="field balconheight">
      <label for="calc_balconwidth">Длина<br>балконнного<br>блока, см</label>
      <input type="text" id="calc_balconwidth" class="field balconwidth">
      <div class="clear"></div>
    </div> 
  </div>
  <div class="row row8">
    <div class="column first">
      <h2 class="header">Телефон,<br>на который вам перезвонить*:</h2>
      <input type="text" class="field phone" />
    </div>
    <div class="column second">
      <h2 class="header">Имя:</h2>
      <input type="text" class="field name" />
    </div>
    <div class="clear"></div>
    <button type="button" class="submit"></button>
    <div class="clear"></div>
  </div>
</div>
<div class="clear"></div>
<p align="center">Если вы хотите заказать окна другой конфигурации, вы можете получить бесплатную консультацию, заказав обратный звонок или по телефону +7 (495) 545-44-70.</p>
<script>
$.fn.getSelected = function(){
  if (!$(this).hasClass('toggle')) return false
  var result = ''
  $('.option',this).each(function(){
    if ($(this).hasClass('active')) {
      if ($(this).find('.text').length > 0) {
          result += $(this).find('.text').html() + ', '
      } else {
          result += $(this).html() + ', '
      }
    }
  })
  result = result.substr(0,result.length-2)
  return result
}

$.fn.getSelectedAll = function(){
  if (!$(this).hasClass('toggle')) return false
  var elements = $(this)
  result = ''
  for(var i=0;i<elements.length;i++) {
     result += $(elements[i]).getSelected() + "|"
  }
  result = result.substr(0,result.length-1)
  return result
}

$.fn.allval = function(){
  var elements = $(this)
  var val = ""
  for(var i=0;i<elements.length;i++) {
    val += $(elements[i]).val() + "|"
  }
  val = val.substr(0,val.length-1)
  return val
}
$(document).ready(function(){

  $('#calc_flapcount').bind('change',function(e){
    if (isNaN($(this).val())) {
      $(this).val('')
    } else {
      var val = parseInt($(this).val())
      if ( $(this).val() < 1 ) {
        $(this).val('1')
      } 
    }
  })

  if ($.browser.opera) {
    $('.toggle .option').each(function(){$(this).addClass('opera')})
  }

  $('.breadcrumbs').css('display','none')
  $('.breadcrumbs').next().css('display','none')

  $.fn.initControls = function(){
    var elements = $(this)

    $('.center',elements).each(function(){
      var width = 0
      for (var i=0;i<$('.option',this).length;i++) {
          width += $($('.option',this)[i]).outerWidth()
      }
      $(this).css('width',width+4)
    })

    $('.toggle',elements).each(function(){
        $('.option',this).first().addClass('first')
        $('.option',this).last().addClass('last')
    })

    $('.toggle .option',elements).click(function(){
        if ($(this).hasClass('active')) {
          $(this).removeClass('active')
          return false
        }
        if ($(this).closest('.toggle').data('multiple') != true) {
          $(this).siblings('.active').removeClass('active')
        }
        $(this).addClass('active')
        if ($(this).closest('.toggle').hasClass('type')) {
          var value = $(this).data('alias')
          window.variant = value
          $('.variants').css('display','none')
          $('.' + value).css('display','block')
        }
    })

    $('.width, .height, .balconwidth, .balconheight',elements).mask('9?999',{placeholder:" "})
  }

  $('#calc').initControls()

  var repeat = $('.repeat').html()

  $('.row_add .add').click(function(){
    $('.repeat').last().after("<div class=repeat>" + repeat + "</div>")
    $('.repeat').last().initControls()
    if ($('.repeat').length >= 2) {
      $('.row_add .remove').show()
    } else {
      $('.row_add .remove').hide()
    }
    return false
  })

  $('.row_add .remove').click(function(){
    $('.repeat').last().detach()
    if ($('.repeat').length >= 2) {
      $('.row_add .remove').show()
    } else {
      $('.row_add .remove').hide()
    }
    return false
  })

  $('#calc .phone').mask('+7 (999) 999-99-99')

  $('.toggle.type .option.first').click()

  $.fn.getRadioValue = function(){
    var radios = $('.models input')
    var prev = null
    for(var i = 0; i < radios.length; i++) {
      radios[i].onclick = function() {
        if(this !== prev) {
          prev = this
        }
        window.model = $('label[for=' + this.id + ']').html()
        $('.row3-5 .image img, .row3-5 .description p').css('display','none')
        $('.row3-5 .image .'+model.toLowerCase()).css('display','block')
        $('.row3-5 .description .'+model.toLowerCase()).css('display','block')
      }}   
  }

  $(document).getRadioValue()
  $('#model_euroline').click()

  $('#calc .submit').bind('click',function(){
    $('#calc .submit').attr('disabled','disabled')
    $('body').css('cursor','progress')
    if (window.variant != 'plastic') {
      var data = {
              cmd: "calculator",
              telephone: $('#calc .phone').val(),
              name: $('#calc .name').val(),
              type: $('.row1 .toggle').getSelected(),
              width: $('#calc .balconwidth').val(),
              height: $('#calc .balconheight').val(),
              yours: $('#calc .row2b .column.first .toggle').getSelected(),
              glassing: $('#calc .row2b .column.second .toggle').getSelected(),
              inner: $('#calc .row3b .column.first .toggle').getSelected(),
              roof: $('#calc .row3b .column.second .toggle').getSelected(),
              flapcount:$('#calc_flapcount').val(), 
      }
    } else {
      var data = {
              cmd: "calculator",
              telephone: $('#calc .phone').val(),
              name: $('#calc .name').val(),
              model: window.model,
              width: $('#calc .width').allval(),
              height: $('#calc .height').allval(),
              type: $('.row1 .toggle').getSelected(),
              place: $('.row2 .toggle').getSelected(),
              flaps: $('.row4 .toggle').getSelectedAll(),
              additional: $('.row5 .toggle').getSelected(),
              works: $('.row6 .toggle').getSelected(),
              decoration: $('.row7 .toggle').getSelected()    
      }
    }
    for(var value in data) {
      if (data.hasOwnProperty(value) && value.length == 0) {
        data[value] = 'не установлено'
      }
    }
    $.getJSON("/ajax.php",
        data, function(data){
            if (data['result'] == "error") {
                message = "";
                if (data['error']['telephone']) { message += "Укажите телефон с кодом!\r\n"; }
                $('#calc .submit').removeAttr('disabled')
                $('body').css('cursor','auto')
                alert(message);
            } else {
                _gaq.push(['_trackEvent', 'Калькулятор', 'Успешная отправка']);
                var request = data['request_id']
                yaCounter20154625.reachGoal('CalculatorSuccess')
                $('#calc .submit').removeAttr('disabled')
                $('body').css('cursor','auto')
                $('#calc').html('<div class="success row"><p class=first>Ваша заявка <strong>№ ' + request + '</strong></p><p class=second>Спасибо за обращение в нашу компанию!</p></div>')
                $(window).scrollTop(0)
                $('#calc input').each(function(){$(this).val('')})
                $('#calc .option.active').each(function(){$(this).removeClass('active')})
            }
        });
    return false
  })
})
</script>
<style>
#calc {
  background: #c3dcec;
  border-radius: 10px;
  padding:1px 0 0 0;
  margin-top: 10px;
  height:auto;

    -webkit-transition: height 1s ease-in-out;
    -moz-transition: height 1s ease-in-out;
    -o-transition: height 1s ease-in-out;
    -ms-transition: height 1s ease-in-out;
    transition: height 1s ease-in-out;
}

#calc h1 {
  font-size:30px;
  font-weight: normal;
}

#calc .toggle {

}

#calc .toggle.icon {

}

#calc .row {
  padding:32px 26px 36px 26px;
}

#calc .row2, #calc .row3-5, #calc .row7, #calc .row2b, #calc .row4b {
  background: url('/images/forms/calc/bg1.png') left bottom repeat-x;
}

#calc .toggle.icon .option {
  padding:47px 0 7px 0;
  line-height: 1.2;
  display: table;
}

#calc .toggle.icon .option.opera {
  display: block;
}

#calc .toggle.icon .icon {
  background-position:center center;
  background-repeat:no-repeat;
  position:absolute;
  height:50px;
  margin-top:-42px;
  padding:7px 7px 0 7px;
}

#calc .toggle .row1

#calc .toggle.icon .option.active .icon {

}

#calc .toggle.type {
  margin-top:30px;
}

#calc .toggle.type .option {
  color:#2d80ae!important;
}

#calc .toggle.type .option.active {
  color:#333!important;
}

#calc .center {
  margin:0 auto;
  border-radius: 7px;
  overflow: hidden;
}

#calc .toggle .center {
  box-shadow: 0 5px 15px 0 #88b3d1;
  border-radius:7px;
  background: transparent;
}

#calc .option {
  float:left;
  padding:13px 28px;
  background: #dfe9f2;
  color:#333;
  cursor: pointer;
  box-shadow: inset 0 1px 2px 0 #fff;
  border-right:1px solid #d6e0e9;
  border-left:1px solid #b9cbdd;
  -webkit-user-select: none; 
  -moz-user-select: none;
  -ms-user-select: none;
  -o-user-select: none;
  user-select: none;
}

#calc .row2 .option, #calc .row4 .option, #calc .row6 .option {
  box-shadow: inset 0 1 2px 0 #73a4c4;
}

#calc .option.active {
  border-right-color:#e0d7be!important;
  border-left-color: #e0d7be!important;
  box-shadow: none;
}

#calc .option.first {
  border-left:1px solid transparent!important;
}

#calc .option.last {
  border-right:1px solid transparent!important;
} 

#calc .option.active {
  background: #f7edcd;
  color:#000;
}

#calc .option.first {
  border-top-left-radius: 7px;
  border-bottom-left-radius: 7px;
}

#calc .option.last {
  border-top-right-radius: 7px;
  border-bottom-right-radius: 7px;
}

#calc .clear {
  clear:both;
}

#calc .header {
  font-size:18px;
  font-weight: bold;
  margin-bottom:12px;
  margin-top:0;
  color:#2d80ae;
}

#calc .column {
  float:left;
}

#calc .column.second {
  margin-left:50px;
}

#calc .icon .text {
  display: table-cell;
  height: 31px;
  font-size: 11.5px;
  text-align: center;
  padding: 0 7px;
  vertical-align: middle;
}

#calc .submit {
  display: block; 
  background: url('/images/forms/calc/submit.png') no-repeat;
  background-position: -5px -85px; 
  width: 388px; 
  height: 80px;
  border:0;
  margin: 35px auto 0 auto;
  cursor: pointer;
}

#calc .submit:hover {
  background-position: -5px 0;
}

#calc label {
  float:left;
  color:#333;
  font-size: 12px;
  padding: 10px 16px 0 0;
}

#calc input {
  border:0;
  border-radius: 6px;
  height:20px;
  padding-top:15px;
  padding-bottom:17px;
  padding-left:10px;
  padding-right:10px;
  width:212px;
  font-size:20px;
  background: #d3e2ee;
  box-shadow:inset 0 2px 1px 1px #8fb1cc, inset 0 -1px 1px 1px #eff5f9;
  outline:none;
  float:left;
}

#calc .butordercall2 {
  display:none;
}

#calc .models {
  float:left;
}

#calc .models input {
  width:auto;
  display:block;
  float:left;
  margin-bottom:10px;
  cursor: pointer;
  height:auto!important;
  padding:0!important;
}

#calc .models label {
  float:left;
  position: relative;
  top:-8px;
  margin-left:7px;
  font-size:15px;
  font-weight: bold;
  cursor: pointer;
}

#calc .row_add .add, #calc .row_add .remove {
  font-size:120%;
  color:#2d80ae;
  text-decoration: none;
}

#calc .row_add .add span,
#calc .row_add .remove span {
  text-decoration: underline;
}

#calc .row_add .add:hover span, #calc .row_add .remove:hover span {
  text-decoration: none;
}

#calc .row_add .remove {
  display: none;
  margin-left:30px;
}

.bottom_form {
  display:none;
}
.row1 {
  padding-top:0!important;
}
.row2 .toggle .option {
  width:90px;
  height:59px;
}
.row2 .toggle .option .text, .row2 .toggle .option .icon {
  width:78px;
}
.row4 .toggle .option {
  width:159px;
}
.row4 .toggle .option .text, .row4 .toggle .option .icon {
  width:147px;
}
.row5 .toggle .option {
  width:159px;
}
.row5 .toggle .option .text, .row5 .toggle .option .icon {
  width:147px;
}
.row6 .toggle .option {
  width:213px;
}
.row6 .toggle .option .text, .row6 .toggle .option .icon {
  width:199px;
}
.row7 .toggle .option {
  width:127px;
}
.row7 .toggle .option .text, .row7 .toggle .option .icon {
  width:113px;
}
.row8 {
  padding-top:25px!important;
}
.row8 .column {
  width:299px;
}
.row8 .column.first h2 {

}
.row8 .column.second h2 {
  margin-top:21px!important;
}
.row8 .column.second input {
  width:281px;
}
.row8 .header {
  float: left;
  width: 377px;
}
.row8 input {
  float:left!important;
  width:250px!important;
}
.row3-5 {
  height:180px;
}
.row3-5 .image {
  float: left;
  width: 158px;
  margin-left: 50px;
}
.row3-5 .image img {
  display:none;
  width:115px;
  height:auto;
}
.row3-5 .description {
	margin-top: -18px;
}
.row3-5 .description p {
  display:none;
  font-size:13px;
}
.row3-5 .description {

}
.row3-5 .description h3 {
  font-size:16px;
  margin-top:0;
}
.plastic {
  display:block;
}
.balcony {
  display:none;
}
.row2b .column, .row3b .column {
  width:298px;
}
.row2b .toggle, .row3b .toggle {
  float:left;
}
.row2b .toggle .option, .row3b .toggle .option {
  width:109px;
}
.row2b .toggle .option .icon, .row2b .toggle .option .text,
.rob3b .toggle .option .icon, .row3b .toggle .option .icon {
  width:94px;
}
.row3b .column.second {
  position: relative;
  top: -1.4em;
}
.row3b .toggle .text {
  width: 91px;
}
.row4b label {
  width:70px;
}
.row4b input {
  width:180px!important;
  margin-left:0;
}
#calc_balconheight {
  margin-right:50px;
}
#calc_flapcount {
  width:110px!important;
}
.success {
    padding-top: 0!important;
    font-size: 110%;
}
.icon1 {
  background-image:url('/images/forms/calc/icons/1.png');
}
.icon2 {
  background-image:url('/images/forms/calc/icons/2.png');
}
.icon3 {
  background-image:url('/images/forms/calc/icons/3.png');
}
.icon4 {
  background-image:url('/images/forms/calc/icons/4.png');
}
.icon5 {
  background-image:url('/images/forms/calc/icons/5.png');
}
.icon6 {
  background-image:url('/images/forms/calc/icons/6.png');
}
.icon7 {
  background-image:url('/images/forms/calc/icons/7.png');
  margin-left: -1px!important;
  margin-top: -38px!important;
}
.icon8 {
  background-image:url('/images/forms/calc/icons/8.png');
}
.icon9 {
  background-image:url('/images/forms/calc/icons/9.png');
}
.icon10 {
  background-image:url('/images/forms/calc/icons/10.png');
}
.icon11 {
  background-image:url('/images/forms/calc/icons/11.png');
  margin-top: -44px!important;
  margin-left: -2px!important;
}
.icon12 {
  background-image:url('/images/forms/calc/icons/12.png');
  margin-top: -50px!important;
}
.icon13 {
  background-image:url('/images/forms/calc/icons/13.png');
  margin-top: -50px!important;
}
.icon14 {
  background-image:url('/images/forms/calc/icons/14.png');
  margin-top: -50px!important;
}
.icon15 {
  background-image:url('/images/forms/calc/icons/15.png');
  margin-top: -50px!important;
}
.icon16 {
  background-image:url('/images/forms/calc/icons/16.png');

}
.icon17 {
  background-image:url('/images/forms/calc/icons/17.png');
}
.icon18 {
  background-image:url('/images/forms/calc/icons/18.png');
}
.icon19 {
  background-image:url('/images/forms/calc/icons/19.png');
}
.icon20 {
  background-image:url('/images/forms/calc/icons/20.png');
}
.icon21 {
  background-image:url('/images/forms/calc/icons/21.png');
}
.icon22 {
  background-image:url('/images/forms/calc/icons/22.png');
}
.icon23 {
  background-image:url('/images/forms/calc/icons/23.png');
}
.iconb1 {
  background-image:url('/images/forms/calc/icons/2_1.png');
  margin-top:-44px!important;
}
.iconb2 {
  background-image:url('/images/forms/calc/icons/2_2.png');
  margin-top:-44px!important;
}
.iconb3 {
  background-image:url('/images/forms/calc/icons/2_3.png');
  margin-top:-44px!important;
}
.iconb4 {
  background-image:url('/images/forms/calc/icons/2_4.png');
  margin-top:-44px!important;
}
.iconb5 {
  background-image:url('/images/forms/calc/icons/2_5.png');
  margin-top:-44px!important;
}
.iconb6 {
  background-image:url('/images/forms/calc/icons/2_6.png');
  margin-top:-44px!important;
}
.iconb7 {
  background-image:url('/images/forms/calc/icons/2_7.png');
  margin-top:-44px!important;
}
.iconb8 {
  background-image:url('/images/forms/calc/icons/2_8.png');
  margin-top:-44px!important;
}
</style>

 <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>