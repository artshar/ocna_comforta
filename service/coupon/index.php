<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Купон на дополнительную скидку");
?> 
<script type="text/javascript">
$(function(){
if($('#print_coupon').length){
    $("#print_coupon").submit(function(){
        $('#error').remove();
        if($('#print_coupon_lastname').val() != '' && $('#print_coupon_name').val() != '' && $('#print_coupon_phone').val() != ''){
            var sLocation = $(this).attr('action') +'?' + $(this).serialize();
            obj = window.open(sLocation, "", "toolbar=0,menubar=1,scrollbars=1,dialog=0,minimizable=1,modal=1,width=730,height=750,resizable=0");
        }else{
            $('.foma_zakaz').prepend('<div id="error" style="color: #E12A0F;font-weight:bold;font-size:12px;">Заполните обязательные поля</div>');
        }
        return false;
    });
}
});
</script>
 
<p align="center"> </p>
 
<p style="line-height: normal;" class="MsoNormal"><b><span style="font-size: 12pt; font-family: 'Times New Roman';">Здесь и сейчас Вы можете воспользоваться дополнительной бонусной программой 
      <br />
     &laquo;Скидка с сайта 2%&raquo;.</span></b><span style="font-size: 12pt; font-family: 'Times New Roman';"></span></p>
 
<p style="text-align: center;"><strong>Для получения бонуса:</strong> 
  <br />
 <img src="/images/2_procenta2.jpg" border="0" width="700" height="200"  /> 
  <br />
 </p>
 
<div class="foma_zakaz" style="padding: 0px; background-image: none; background-attachment: scroll; background-color: transparent; background-position: 0% 0%; background-repeat: repeat repeat;"> <form action="/service/coupon/print/" id="print_coupon" method="get"> 
    <p> <span>Фамилия <sup>*</sup></span><input type="text" name="lastname" id="print_coupon_lastname" value="" /> 
      <br />
     
      <br />
     <span>Имя <sup>*</sup></span><input type="text" name="name" id="print_coupon_name" value="" /> 
      <br />
     
      <br />
     <span>Отчество</span><input type="text" name="surname" value="" /> 
      <br />
     
      <br />
     <span>Телефон <sup>*</sup></span><input type="text" name="phone" id="print_coupon_phone" value="" /> 
      <br />
     
      <br />
     <span>E-mail</span><input type="text" name="email" value="" /> 
      <br />
     
      <br />
     <span></span> 
      <br />
     
      <br />
     <input type="submit" class="button8" value="Распечатать купон" onclick="yaCounter20154625.reachGoal('KUPON'); _gaq.push(['_trackPageview', '/kupon_btm']); return true;" style="margin: 0px 0px 15px 10px;" /> </p>
   </form> </div>
 <span>* - звёздочкой отмечены поля, обязательные для заполнения</span> 
<ul> 
  <li>Распечатайте купон самостоятельно с сайта или попросите менеджера при визите в офис распечатать купон с Вашими данными  </li>
 
  <li>Предъявите купон менеджеру замера при заключении договора у Вас дома или менеджеру-консультанту при заключении договора в офисе.</li>
 
  <li>Этот купон действителен в течение 10 дней со дня получения.</li>
 
  <li>Скидки по другим акциям не суммируются с действующим предложением.</li>
 </ul>
 <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>