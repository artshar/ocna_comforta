<?
define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS", true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
?>
<?
// Записываем номер купона
$filenameNum = $_SERVER["DOCUMENT_ROOT"]."/service/coupon/print/number.txt";
$handleNum = fopen($filenameNum, "r");
$num = fread($handleNum, filesize($filenameNum)); $num++;
fclose($handleNum);

$fp = fopen($filenameNum, "w+");
fwrite($fp, $num);
fclose($fp);
if(strlen($num) < 8) { $num = str_repeat("0", 8-strlen($num)).$num; }

CModule::IncludeModule("iblock");

$el = new CIBlockElement;

$message .= "Купон №".$num."\r\n\r\n";
$message .= "Фамилия: ".$_REQUEST['lastname']."\r\n";
$message .= "Имя: ".$_REQUEST['name']."\r\n";
$message .= "Отчество: ".$_REQUEST['surname']."\r\n";
$message .= "Телефон: ".$_REQUEST['phone']."\r\n";
$message .= "E-mail: ".$_REQUEST['email']."\r\n";
$message .= "Дата: ".date("d.m.Y", mktime(0, 0, 0, date("m"), date("d"), date("Y")))." - ".date("d.m.Y", mktime(0, 0, 0, date("m"), date("d")+10, date("Y")))."\r\n";

$arLoadProductArray = Array(
    "IBLOCK_ID" => 7,
    "NAME" => "Купон №".$num,
    "PREVIEW_TEXT" => $message,
);
$el->Add($arLoadProductArray);
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<title>Печать купона на дополнительную скидку</title>
<meta name="description" content="Печать купона на дополнительную скидку">
<meta name="keywords" content="Печать купона на дополнительную скидку">
<meta http-equiv="Content-Language" content="ru">
<meta content="text/html; charset=utf-8" http-equiv=Content-Type>
<link href="print.css" type="text/css" rel="stylesheet" media="print"/>
</head>
<body bgcolor="#FFFFFF" topmargin="0" leftmargin="0" marginwidth="0" marginheight="0">
<div style="width:712px;margin:0 auto;position:relative;">
<div style="width:417px; height: 22px; top: 185px; left: 193px;  position: absolute;font-size:21px;"><?=$num?></div>
<div style="width:417px; height: 46px; top: 233px; left: 193px;  position: absolute;font-size:21px;">
<table border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td width="410" height="46" valign="middle"><?=$_REQUEST['lastname']?> <?=$_REQUEST['name']?> <?=$_REQUEST['surname']?></td>
  </tr>
</table>
</div>
<div style="width:125px; height: 30px; top: 299px; left: 193px; position: absolute;font-size:22px;"><?=date("d.m.Y", mktime(0, 0, 0, date("m"), date("d"), date("Y")));?>г.</div>
<div style="width:133px; height: 30px; top: 299px; left: 490px; position: absolute;font-size:22px;"><?=date("d.m.Y", mktime(0, 0, 0, date("m"), date("d")+10, date("Y")));?>г.</div>

<!-- <p align="center"><img src="/images/kupon_skidka2-2.jpg" alt="Купон на дополнительную скидку" border="0"/></p> -->
<p align="center"><img src="/images/kupon2.jpg" alt="Купон на дополнительную скидку" border="0"/></p>  
<p align="center" class="no_print"><a onclick="window.print(); return false;" href="#">Печать</a></p>
<p align="center"><strong>Предъявите купон менеджеру замера при  заключении договора у Вас дома или менеджеру-консультанту при заключении  договора в одном из центральных офисов.</strong></p>
<p align="center">Покупайте&nbsp; окна выгодно!</p>
</div>
</body>
</html>