<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Услуги");
?>Наша компания заботится о вашем комфорте, поэтому все работы по остеклению берет на себя. 
<div id="call_offer" style="margin-left: 133px;"> 	 
  <p>Мы Вам перезвоним и поможем!</p>
 	 
  <div> 	<input type="text" class="phone" /> 	<button type="button" value="Заказать" class="btn"></button> 	</div>
 </div>
 
<br />
 Мы предоставляем полный комплекс услуг, в частности: 
<br />
 
<br />
 
<table cellspacing="1" cellpadding="1" border="0"> 
  <tbody> 
    <tr><td><img src="/images/ikonki/konsult1].png" border="0" width="50" height="50"  /></td><td><a href="/service/consultation/" ><b>Квалифицированная консультация</b></a> и замер оконных проемов. Менеджеры-консультанты ознакомят с акциями, расскажут о преимуществах разных вариантов остекления, а также помогут подобрать вам наиболее подходящий тип остекления.</td></tr>
   
    <tr><td><img src="/images/ikonki/ras4et.png" border="0" width="50" height="50"  /></td><td><b><a href="/service/calculate/" >Расчет стоимости</a>.</b> Специалисты осуществят предварительный расчет стоимости вашего заказа.</td></tr>
   
    <tr><td><img src="/images/ikonki/rassro4.png" border="0" width="50" height="50"  /></td><td><b><a href="/service/deferred/" >Рассрочка</a></b> &ndash; удобный и выгодный способ оплаты.</td></tr>
   
    <tr><td><img src="/images/ikonki/dogovor.png" border="0" width="50" height="50"  /></td><td><a href="/service/contract/" ><b>Подписание договора</b></a> на взаимовыгодных условиях.</td></tr>
   
    <tr><td><img src="/images/ikonki/garant.png" border="0" width="50" height="50"  /></td><td><a href="/service/warranty/" ><b>Гарантия</b></a> на пластиковые окна 5 лет и 3 года на монтаж.</td></tr>
   
    <tr><td><img src="/images/ikonki/dostav.png" border="0" width="50" height="50"  /></td><td><b><a href="/service/delivery/" >Доставка</a> </b>оконных конструкций на специально оборудованных автомобилях.</td></tr>
   
    <tr><td><img src="/images/ikonki/ystanov.png" border="0" width="50" height="50"  /></td><td><a href="/service/installation-of-windows/" ><b>Установка окон</b></a>. Выполняем профессиональный демонтаж и монтаж окон.</td></tr>
   </tbody>
 </table>
 
<br />
 Кроме того, наша компания: 
<br />
 
<ul> 
  <li>дарит подарки и скидки</li>
 
  <li>заключает с каждым клиентом договор</li>
 
  <li>предлагает любые способы оплаты, в том числе рассрочку</li>
 
  <li>выдает гарантию на продукцию и монтаж. </li>
 </ul>
 
<br />
 <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>