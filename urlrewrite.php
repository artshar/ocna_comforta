<?
$arUrlRewrite = array(
	array(
		"CONDITION"	=>	"#^/panel([0-9]).html#",
		"RULE"	=>	"cmd=slider&NUMBER=$1",
		"ID"	=>	"",
		"PATH"	=>	"/ajax.php",
	),
	array(
		"CONDITION"	=>	"#^/gallery/(.*)/#",
		"RULE"	=>	"SECTION_CODE=$1",
		"ID"	=>	"",
		"PATH"	=>	"/gallery/index.php",
	),
	array(
		"CONDITION"	=>	"#^/about/shops/#",
		"RULE"	=>	"",
		"ID"	=>	"bitrix:news",
		"PATH"	=>	"/about/shops/index.php",
	),
	array(
		"CONDITION"	=>	"#^/response/#",
		"RULE"	=>	"",
		"ID"	=>	"bitrix:news",
		"PATH"	=>	"/response/index.php",
	),
	array(
		"CONDITION"	=>	"#^/catalog/#",
		"RULE"	=>	"",
		"ID"	=>	"bitrix:catalog",
		"PATH"	=>	"/catalog/index.php",
	),
	array(
		"CONDITION"	=>	"#^/actions/#",
		"RULE"	=>	"",
		"ID"	=>	"bitrix:news",
		"PATH"	=>	"/actions/index.php",
	),
	array(
		"CONDITION"	=>	"#^/news/#",
		"RULE"	=>	"",
		"ID"	=>	"bitrix:news",
		"PATH"	=>	"/news/index.php",
	),
);

?>