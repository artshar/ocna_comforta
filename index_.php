<?
define("NOT_SHOW_BREADCRUMB", 1);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Окна комфорта");




?> 
<script>
$(document).ready(function(){
    $('.linkmap .arrow').each(function(){$(this).addClass('init')})
})
</script>
 
<h1 class="largetitle">Заказать окна просто</h1>
 
<div id="passthrough" class="linkmap"> <a class="step step1" href="/catalog/veka-plastic-windows/" > <img src="/images/mainpage/1_1.png" border="0" alt="Зайдите в раздел &amp;laquo;пластиковые окна&amp;raquo; и выберите на свой вкус..."  /> <span class="title"><strong>Зайдите</strong> в раздел &laquo;пластиковые окна&raquo; и <strong>выберите</strong> на свой вкус...</span> </a> 
  <div class="arrow arrow1"></div>
 <a class="step step2" href="#" onclick="$('#orderCall').click();return false;" > <img src="/images/mainpage/1_2.png" border="0" alt="Отправьте заявку или позвоните нам"  /> <span class="title">Отправьте заявку или <strong>позвоните нам</strong>.</span> </a> 
  <div class="arrow arrow2"></div>
 <a class="step step3" href="/service/consultation/" > <img src="/images/mainpage/1_3.png" border="0" alt="Получите подробную консультацию менеджера"  /> <span class="title">Получите подробную <strong>консультацию</strong> менеджера</span> </a> <a class="step step4" href="#" id="orderManager1" onclick="$('#orderManager').click();return false;" > <img src="/images/mainpage/1_4.png" border="0" alt="Замер окон - бесплатно!"  /> <span class="title"><strong>Замер окон - бесплатно!</strong></span> </a> 
  <div class="arrow arrow4"></div>
 <a class="step step5" href="/service/contract/" > <img src="/images/mainpage/1_5.png" border="0" alt="Подписание договора - это легко!"  /> <span class="title">Подписание договора - это <strong>легко</strong>!</span> </a> 
  <div class="clear"></div>
 </div>
 
<div class="clear"></div>
 
<p class="MsoNormal" style="margin-bottom: 0.0001pt; line-height: normal;"><b> 
    <br />
   Как заказать окна?</b></p>
 
<p align="justify" class="MsoNormal" style="margin-bottom: 0.0001pt; line-height: normal;">1.Зайдите в раздел <a href="/catalog/veka-plastic-windows/" >Пластиковые окна</a> и выберите подходящий  вариант остекления: <a href="/catalog/euroline/" > 
    <br />
   </a></p>
 
<ol> 
  <li><a href="/catalog/euroline/" ><b>Euroline</b></a> - бюджетное остекление 
    <br />
   </li>
 
  <li><a href="/catalog/proline/" ><b>Proline</b></a> - наиболее популярная модель <a href="/catalog/proline/" ><img src="/images/hit.png" border="0" align="rigth" width="43" height="20" style="margin: -5px 2px;"  /> </a> 
    <br />
   </li>
 
  <li><b><a href="/catalog/softline/" >Softline</a></b> - предложение для квартир и коттеджей, где небходимы нестандартные формы или ламинация окон 
    <br />
   </li>
 
  <li><a href="/catalog/softline/" ><b>Softline 82</b></a> - новинка! 
    <br />
   </li>
 
  <li><b><a href="/catalog/alphaline/" >Alphaline</a></b> - премиум-предложение<o:p></o:p></li>
 </ol>
 
<p align="justify" class="MsoNormal" style="margin-bottom: 0.0001pt; line-height: normal;">2. Отправьте заявку, и наш консультант перезвонит вам. Консультант рассчитает предварительную стоимость вашего заказа и ответит на все вопросы. Стоит отметить, что вы также можете узнать предварительную стоимость заказа, воспользовавшись on-line &ndash; калькулятором. Однако чтобы точно рассчитать заказ потребуется выполнить замер окон.<o:p></o:p></p>
 
<p align="justify" class="MsoNormal" style="margin-bottom: 0.0001pt; line-height: normal;">3. Воспользуйтесь услугой <a href="/service/consultation/" >замер окон</a>. Наш специалист бесплатно произведет все необходимые замеры, которые будут использованы для определения точной стоимости<span>  </span>вашего заказа. Оформить заявку на замер можно на сайте или, позвонив менеджерам-консультантам.<o:p></o:p></p>
 
<p align="justify" class="MsoNormal" style="margin-bottom: 0.0001pt; line-height: normal;">4. После проведения замеров, и расчета стоимости, осуществляется <a href="/service/contract/" >подписание договора</a>. Заключить договор вы можете у себя дома сразу после произведения замеров окон или же, подъехав к нам в офис.<o:p></o:p></p>
 
<p align="justify" class="MsoNormal" style="margin-bottom: 0.0001pt; line-height: normal;">5. Ваш заказ будет доставлен по указанному адресу в согласованное время. <a href="/service/installation-of-windows/" >Монтаж окон</a>, как правило, выполняется на следующий день после доставки. Наши специалисты установят окна в соответствии со всеми нормами и требованиями, обеспечив, таким образом, долговечность и функциональность окон. 
  <br />
 </p>
 
<p align="center" class="MsoNormal" style="margin-bottom: 0.0001pt; line-height: normal;"><b>Телефон многоканальной линии <span id="call_phone_2">(495) 545-44-70</span>!</b> 
  <br />
 </p>
 
<h1 class="largetitle" style="margin-left: 6px; margin-top: 40px;">Преимущества</h1>
 
<div id="profits" class="linkmap"> <a class="step step1" href="/about/manufacture/" > <img src="/images/mainpage/2_1.png" border="0" alt="Собственное производство"  /> <span class="title">Собственное производство</span> </a> <a class="step step2" href="/service/deferred/" > <img src="/images/mainpage/2_2.png" border="0" alt="Рассрочка"  /> <span class="title">Рассрочка</span> </a> <a class="step step3" href="/service/warranty/" > <img src="/images/mainpage/2_3.png" border="0" alt="Гарантия"  /> <span class="title">Гарантия</span> </a> <a class="step step4" href="/actions/" > <img src="/images/mainpage/2_4.png" border="0" alt="Скидки и подарки"  /> <span class="title">Скидки и подарки</span> </a> <a class="step step5" href="/about/partners/" > <img src="/images/mainpage/2_5.png" border="0" alt="Качество продукции"  /> <span class="title">Качество продукции</span> </a> <a class="step step6" href="/service/consultation/" > <img src="/images/mainpage/2_6.png" border="0" alt="Бесплатный выезд и консультация специалиста"  /> <span class="title">Бесплатный выезд и консультация специалиста </span> </a> <a class="step step7" href="/service/delivery/" > <img src="/images/mainpage/2_7.png" border="0" alt="Бесплатная доставка"  /> <span class="title">Бесплатная доставка</span> </a> <a class="step step8" href="/response/" > <img src="/images/mainpage/2_8.png" border="0" alt="Довольные клиенты"  /> <span class="title">Довольные клиенты</span> </a> 
  <div class="clear"></div>
 </div>
 
<div class="clear"></div>
 <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>