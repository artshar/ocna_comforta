-- phpMyAdmin SQL Dump
-- version 3.3.2deb1
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Дек 17 2012 г., 13:58
-- Версия сервера: 5.1.41
-- Версия PHP: 5.2.10-2ubuntu6.5

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `oknakomforta`
--

-- --------------------------------------------------------

--
-- Структура таблицы `calculation_items`
--

CREATE TABLE IF NOT EXISTS `calculation_items` (
  `calculation_items_id` int(11) NOT NULL AUTO_INCREMENT,
  `calculation_items_date` datetime NOT NULL,
  `calculation_items_user_name` varchar(255) NOT NULL,
  `calculation_items_user_phone` varchar(255) NOT NULL,
  `calculation_items_user_email` varchar(255) NOT NULL,
  `calculation_items_xml` mediumtext NOT NULL,
  `calculation_items_accepted_date` datetime NOT NULL,
  `calculation_items_accepted_user` int(11) NOT NULL,
  PRIMARY KEY (`calculation_items_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13663 ;

-- --------------------------------------------------------

--
-- Структура таблицы `calculator_items`
--

CREATE TABLE IF NOT EXISTS `calculator_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `size_from_w` int(11) NOT NULL,
  `size_from_h` int(11) NOT NULL,
  `size_to_w` int(11) NOT NULL,
  `size_to_h` int(11) NOT NULL,
  `size_step` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

-- --------------------------------------------------------

--
-- Структура таблицы `calculator_item_sizes`
--

CREATE TABLE IF NOT EXISTS `calculator_item_sizes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `calculator_item_id` int(11) NOT NULL,
  `size_w` int(11) NOT NULL,
  `size_h` int(11) NOT NULL,
  `price` int(11) NOT NULL DEFAULT '0',
  `saved` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `calculator_item_id` (`calculator_item_id`),
  KEY `size_w` (`size_w`),
  KEY `size_h` (`size_h`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4253 ;
